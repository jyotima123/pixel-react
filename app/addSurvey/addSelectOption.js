import React from 'react'

const AddSelectOption = ({onClick,rand,opt}) => {

  //This is child Component to add Option, when You click on Add option after selection Multiple Or Single Choice
    const handleDelete = (e) =>{
        onClick(rand);
    }
      return (
       <div className="row" id={rand}>
        <div className="col-md-6">
            <div className="mb-3">
                <label for="defaultFormControlInput" className="form-label">Option {opt}</label>
                <div className="input-group">
                    <input type="text" className="form-control input-type" id="defaultFormControlInput" placeholder="option" aria-describedby="defaultFormControlHelp"/>
                </div>
            </div>
        </div>
        <div className="col-md-6">
            <div className="mb-3">
                <label for="defaultFormControlInput" className="form-label">Score {opt}</label>
                <div className="input-group">
                    <input type="text" className="form-control score-type" id="defaultFormControlInput" placeholder="10" aria-describedby="defaultFormControlHelp"/><span className="input-group-text" onClick={handleDelete}><i className="bx bx-trash cursor-pointer"></i></span>
                </div>
            </div>
        </div>
    </div>
        
      )

}

export default AddSelectOption