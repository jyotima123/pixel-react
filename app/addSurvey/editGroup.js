import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";


const EditGroup = ({ onClose, children, title,group_id }) => {
    const [isLoading, setLoading] = useState(false);
    const [group, setGroup] = useState({
        id:"",
        groupName: ""
      });
      const [errors, setError] = useState({
        errors: "",
      });

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    //This module is use to update the group
    useEffect(() => {
        
        async function fetchData()
        {
            const resschool = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getGroupById?id="+group_id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store' 
                }).then(res => res.json())
                .then(res => {
                    setGroup(res.output)
                });
        }
        fetchData();    
    },[]);

    const onInputChange = e => {

        setGroup({ ...group, [e.target.name]: e.target.value });
        
    };

    
    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        
        //username
        if(!group["groupName"]){
        formIsValid = false;
        errors["groupName"] = "Group name Cannot be empty";
        }
        
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/group/update", {
            method: "PUT",
            body: JSON.stringify(group),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success");
                }
                else{
                    onClose("error");
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };

         

      const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Add Group</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    
                    <div className="modal-body">
                        <form>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="mb-3">
                                        <label for="defaultFormControlInput" className="form-label">Group Name</label>
                                        <input type="text" placeholder="Group Name" className="form-control" name="groupName" value={group.groupName} onChange={e => onInputChange(e)}/>
                                            <span className="error" style={{ color: "red" }}>{errors['groupName']}</span>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
                        {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
};

export default EditGroup