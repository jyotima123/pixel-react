import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import AddSelectOption from './addSelectOption';
import AddImgOption from './addImgOption';

//This module is use to update question

const EditQuestion = ({ onClose, children, title,question_id,group_id }) => {
    const [isLoading, setLoading] = useState(false);
    const[likert,setLikert] = useState(false);
    const[multiSelect,setMultiSelect] = useState(false);
    const[multiImg,setMultiImg] = useState(false);
    const[qst,setQst] = useState(false);
    const[msopt,setMsopt] = useState(3);
    const[selectopt,setSelectOpt] = useState([]);
    const[imgopt,setImgOpt] = useState([]);
    const[qstFile,setQstFile] = useState(0);
    const[msmgopt,setMsmgopt] = useState(3);
    const[file,setFile] = useState("assets/img/question-img.png");
    const [question, setQuestion] = useState({
        id:"",
        groupId: group_id,
        questionType: "",
        questionCategory:"",
        bracket:"",
        question:"",
        questionImage:"",
        optionNumber:"",
        options:[],
        scaleMin:"",
        scaleMax:"",
        scaleMinScore:"",
        scaleMaxScore:"",
        scaleMinPointer:"",
        scaleMaxPointer:""
      });
      const [errors, setError] = useState({
        errors: "",
      });

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    useEffect(() => {
        
        async function fetchData()
        {
            const resschool = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getQuestionById?id="+question_id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store' 
                }).then(res => res.json())
                .then(res => {
                    
                setQuestion(res.output)
                
                if(res.output.questionType == 'Likert Scale')
                {
                    setLikert(true)
                    setMultiSelect(false)
                    setMultiImg(false)        
                }
                else if(res.output.questionType == 'Multiple Image Choice' || res.output.questionType == 'Single Image Choice')
                {
                    setMultiSelect(false)
                    setLikert(false)
                    setMultiImg(true)
                    setMsmgopt((res.output.options).length+1)
                    
                }
                else if(res.output.questionType == 'Free Text')
                {
                    setMultiSelect(false)
                    setLikert(false)
                    setMultiImg(false)
                    
                }
                else{
                    setMultiSelect(true)
                    setLikert(false)
                    setMultiImg(false)
                    setMsopt((res.output.options).length+1)
                }

                if(res.output.questionCategory == 'TextWithImage')
                {
                    setQst((prev) => !prev)
                    setFile('/var/www/html/node-api/PixelHealth/uploads/'+res.output.questionImage);
                }  
            
        });
        }
        fetchData();    
    },[]);


    const onInputChange = e => {
        setQuestion({ ...question, [e.target.name]: e.target.value });
    };

    const onResponseChange = async(e) => {

        setSelectOpt([])
        setImgOpt([])

        setQuestion({ ...question, [e.target.name]: e.target.value });
        

        if(e.target.value == 'Likert Scale')
        {
            setLikert(true)
            setMultiSelect(false)
            setMultiImg(false)
            setMsopt(3)
            setMsmgopt(3)

        }
        else if(e.target.value == 'Multiple Image Choice' || e.target.value == 'Single Image Choice')
        {
            setMultiSelect(false)
            setLikert(false)
            setMultiImg(true)
            setMsopt(3)
        }
        else if(e.target.value == 'Free Text')
        {
            setMultiSelect(false)
            setLikert(false)
            setMultiImg(false)
            setMsopt(3)
            setMsmgopt(3)
        }
        else{
            setMultiSelect(true)
            setLikert(false)
            setMultiImg(false)
            setMsmgopt(3)
            
        }
        
    };

    const addOption = () => {

        setMsopt((msopt) => (msopt+1))

        const min = 1;
        const max = 100;
        const rand = min + Math.random() * (max - min);

        setSelectOpt([ ...selectopt, <AddSelectOption rand={rand} opt={msopt} onClick={removeBox}/> ]);
       
    }
   
    const removeBox = (rand)=>{
       
        const list = document.getElementById(rand);        
        if(document.getElementById(rand))        
        {            
            document.getElementById(rand).removeChild(list.firstElementChild); 
            document.getElementById(rand).removeChild(list.lastElementChild);        
        }
    }

    const removesBox = (s,rand)=>{
        
        const list = document.getElementById(rand);        
        if(document.getElementById(rand))        
        {            
            document.getElementById(rand).removeChild(list.firstElementChild);   
            document.getElementById(rand).removeChild(list.lastElementChild);     
        }
    }

    const addMgOption = () => {

        setMsmgopt((msmgopt) => (msmgopt+1))

        const min = 1;
        const max = 100;
        const rand = min + Math.random() * (max - min);

        setImgOpt([ ...imgopt, <AddImgOption rand={rand} opt={msmgopt} onClick={removeBox}/> ]);      

    }
    const removemgBox = (e,rand)=>{
        const list = document.getElementById(rand);        
        if(document.getElementById(rand))        
        {            
            document.getElementById(rand).removeChild(list.firstElementChild);    
            document.getElementById(rand).removeChild(list.lastElementChild);        
        }
    }

    const onQstChange = (e) => {
        setQuestion({ ...question, [e.target.name]: e.target.value });
        setQst((prev) => !prev)
    }
    const onFileChange = async(e,type) => {
        
        if(type == 'qst')
        {
            
            var ext = checkFileExtension(e.target.files[0].name)
            if(ext == 'png' || ext == 'jpeg' || ext == 'jpg')
            {
                setQuestion({ ...question, 'questionImage': e.target.files[0] });
                var img = URL.createObjectURL(e.target.files[0]);
                setFile(img);
                setQstFile(2)
            }
            else{
                let errors = {};
                errors["qstFile"] = "Please upload only PNG,JPEG,JPG Files";
                setError(errors);
            }
        }
         
    }

    const checkFileExtension = (fileName) => {
        // fileName = document.querySelector('#file1').value;
        var extension = fileName.substring(fileName.lastIndexOf('.') + 1);
        return extension
    };

    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        
        //username
        if(!question["question"]){
        formIsValid = false;
        errors["question"] = "Question Text Cannot be empty";
        }
        if(!question["questionType"]){
        formIsValid = false;
        errors["questionType"] = "Please select any type";
        }
        if(question["questionType"] == 'Multiple Choice' || question["questionType"] == 'Single Choice')
        {
            var els = document.getElementsByClassName("score-type");
           
            Array.prototype.forEach.call(els, function(el) {
                if(!el.value)
                {
                    formIsValid = false;
                    errors["selectOption"] = "Don't leave any Score blank";
                }
                
            });

            var els = document.getElementsByClassName("input-type");
           
            Array.prototype.forEach.call(els, function(el) {
                if(!el.value)
                {
                    formIsValid = false;
                    errors["selectOption"] = "Don't leave any Option blank";
                }
                
            });

        }
        if(question["questionType"] == 'Likert Scale')
        {
            if(!question["scaleMin"])
            {
                formIsValid = false;
                errors["scaleMin"] = "Please enter minimum value";
            }
            if(!question["scaleMax"])
            {
                formIsValid = false;
                errors["scaleMax"] = "Please enter maximum value";
            }
            if(!question["scaleMinScore"])
            {
                formIsValid = false;
                errors["scaleMinScore"] = "Please enter score for minimum value";
            }
            if(!question["scaleMaxScore"])
            {
                formIsValid = false;
                errors["scaleMaxScore"] = "Please enter score for maximum value";
            }
            if(!question["scaleMinPointer"])
            {
                formIsValid = false;
                errors["scaleMinPointer"] = "Please enter pointer";
            }
            if(!question["scaleMaxPointer"])
            {
                formIsValid = false;
                errors["scaleMaxPointer"] = "Please enter pointer";
            }

        }
        if(question["questionType"]== 'Multiple Image Choice' || question["questionType"]== 'Single Image Choice')
        {
            var els = document.getElementsByClassName("score-file");
            
            Array.prototype.forEach.call(els, function(el) {
                
                if(!el.value)
                {
                    formIsValid = false;
                    errors["imgOption"] = "Don't leave any Score blank";
                }
                
            });

            
            
        }
        
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){

            const data = new FormData();
            data.append("id",question.id);
            data.append('questionType',question.questionType);
            data.append("questionCategory",question.questionCategory);  
            data.append("bracket",question.bracket);
            data.append("question",question.question);
            if(question.questionCategory == 'TextWithImage')
            {
                if(qstFile == 2)
                {
                    var time = new Date().getTime();
                    var ext = checkFileExtension(question.questionImage.name)
                    var qstpic = 'qst_'+time+'.'+ext;
                    data.append("questionImage",question.questionImage,qstpic); 
                    data.append("qstImg","new Image");
                }
                
            }
            if(question.questionType == 'Likert Scale')
            {
                data.append("scaleMin",question.scaleMin);
                data.append("scaleMax",question.scaleMax);
                data.append("scaleMinScore",question.scaleMinScore);
                data.append("scaleMaxScore",question.scaleMaxScore);
                data.append("scaleMinPointer",question.scaleMinPointer);
                data.append("scaleMaxPointer",question.scaleMaxPointer);
            }
            if(question.questionType == 'Multiple Choice' || question.questionType == 'Single Choice')
            {
                var els = document.getElementsByClassName("input-typeID");
                let opt = []
                Array.prototype.forEach.call(els, function(el) {
                    opt.push(el.value);
                });


                var els = document.getElementsByClassName("input-type");
                let types = []
                Array.prototype.forEach.call(els, function(el) {
                    types.push(el.value);
                });

                if(types.length>opt.length)
                {
                    for(var i=opt.length;i<types.length;i++)
                    {
                        opt.push('a'+i)
                    }
                }
              
                const optionId = Object.fromEntries(
                    opt.map((typesvalue, index) => [typesvalue, types[index]])
                );
                   
                var els = document.getElementsByClassName("score-type");
                let score = []
                Array.prototype.forEach.call(els, function(el) {
                    score.push(el.value);
                });

                const selectedScope = Object.fromEntries(
                    types.map((typesvalue, index) => [typesvalue, score[index]])
                );


                data.append("optionNames",JSON.stringify(optionId));
                data.append("optionScore",JSON.stringify(selectedScope));
            }
            if(question.questionType == 'Multiple Image Choice' || question.questionType == 'Single Image Choice')
            {
                var els = document.getElementsByClassName("input-file");
                let type = [] 
                let i = 0
                Array.prototype.forEach.call(els, function(el) {
                    if(el.files[0])
                    {
                        var time = new Date().getTime();
                        var ext = checkFileExtension(el.files[0].name)
                        var optpic = 'opt_'+time+i+'.'+ext;
                        data.append("optionImages",el.files[0],optpic);
                        i = i+1 
                    type.push(optpic);
                    } 
                    else{
                        type.push(''); 
                    } 
                                
                    
                });
                
                var els = document.getElementsByClassName("score-file");
                let score = []
                Array.prototype.forEach.call(els, function(el) {
                    score.push(el.value);
                });

                var els = document.getElementsByClassName("input-files");
                let types = []
                Array.prototype.forEach.call(els, function(el) {
                    types.push(el.value);
                });
                

                let selectedScope = {}
                for(let index=0;index<type.length;index++)
                {
                        if(type[index] == '')
                        {
                            selectedScope[types[index]] = score[index]
                        }
                        else{
                            selectedScope[type[index]] = score[index]
                        }
                }
                

                data.append("optionScore",JSON.stringify(selectedScope));
                data.append("editImages",JSON.stringify(types));
            }
            

            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/question/update", {
                        method: "PUT",
                        body: data,
                    }).then(res => res.json())
                    .then(res => {
                        
                        setLoading(false);
                        
                        if(res.success)
                        {
                            onClose("success");
                        }
                        else{
                            onClose("error");
                        }
                    });
          }
          else{
            setLoading(false);
          }
       
      };


      const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Edit Question</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                    </button>
                    </div>
                    
                    <div className="modal-body">
                        <form>
                            <div className="row">
                            <div className="col-md-12">
                                    <div className="mb-3">
                                        <label for="defaultSelect" className="form-label">Select Category</label>
                                            <select id="defaultSelect" name="bracket" className="form-select" value={question.bracket} onChange={e => onInputChange(e)}>
                                                <option value="Wellness">Wellness</option>
                                                <option value="Knowledge">Knowledge</option>
                                            </select>
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <div className="mb-3">
                                        <label for="defaultSelect" className="form-label">Select Response Type</label>
                                            <select id="defaultSelect" name="questionType" className="form-select" value={question.questionType} onChange={e => onResponseChange(e)}>
                                                <option value="">Select Response Type</option>
                                                <option value="Likert Scale">Likert Scale</option>
                                                <option value="Multiple Choice">Multiple Choice</option>
                                                <option value="Multiple Image Choice">Multiple Image Choice</option>
                                                <option value="Single Choice">Single Choice</option>
                                                <option value="Single Image Choice">Single Image Choice</option>
                                                <option value="Free Text">Free Text</option>
                                            </select>
                                            <span className="error" style={{ color: "red" }}>{errors['questionType']}</span>
                                    </div>
                                </div>
                            </div>
                            {(likert) ?
                            <>
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Scale Minimum Pointer</label>
                                            <input type="text" className="form-control" id="defaultFormControlInput" name="scaleMinPointer" value={question.scaleMinPointer} onChange={e => onInputChange(e)} placeholder="Poor" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMinPointer']}</span>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Scale Minimum</label>
                                            <input type="number" className="form-control" id="defaultFormControlInput" name="scaleMin" value={question.scaleMin} onChange={e => onInputChange(e)} placeholder="0" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMin']}</span>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score</label>
                                            <input type="number" className="form-control" id="defaultFormControlInput" name="scaleMinScore" value={question.scaleMinScore} onChange={e => onInputChange(e)} placeholder="10" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMinScore']}</span>
                                        </div>
                                    </div>
                                </div> 
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Scale Maximum Pointer</label>
                                            <input type="text" className="form-control" id="defaultFormControlInput" name="scaleMaxPointer" value={question.scaleMaxPointer} onChange={e => onInputChange(e)} placeholder="Best" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMaxPointer']}</span>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Scale Maximum</label>
                                            <input type="text" className="form-control" id="defaultFormControlInput" name="scaleMax" value={question.scaleMax} onChange={e => onInputChange(e)} placeholder="0" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMax']}</span>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score</label>
                                            <input type="text" className="form-control" id="defaultFormControlInput" name="scaleMaxScore" value={question.scaleMaxScore} onChange={e => onInputChange(e)} placeholder="10" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMaxScore']}</span>
                                        </div>
                                    </div>
                                </div> 
                            </>
                            :
                            (multiSelect) ?
                            <>
                                <div className="row">
                                    <div className="col-12 text-end">
                                        <a type="button" className="btn btn-primary" onClick={addOption}>
                                            <span className="tf-icons bx bx-plus"></span>&nbsp; Add Option
                                        </a>
                                    </div>
                                    <span className="error" style={{ color: "red" }}>{errors['selectOption']}</span>
                                </div>
                                <div className="row ">
                                {(question.options.length>0) ?
                                    (question.options).map((res,key,row) => {
                                        
                                return (
                                        <div className='row' id={key}>
                                            {(key > 1) ? 
                                                <>
                                                    <div className="col-md-6">
                                                        <div className="mb-3">
                                                            <label for="defaultFormControlInput" className="form-label">Option {key+1}</label>
                                                             <input type="text"  defaultValue={res.optionName} name="option3" className="form-control input-type" placeholder="option"/>
                                                             
                                                             <input type="hidden"  value={res.id}  className="form-control input-typeID"/>
                                                        </div>
                                                    </div>
                                                   
                                                    <div className="col-md-6">
                                                        <div className="mb-3">
                                                            <label for="defaultFormControlInput" className="form-label">Score {key+1}</label>
                                                            <div className="input-group">
                                                            <input type="number" className="form-control score-type" placeholder="10" defaultValue={res.optionScore}/>
                                                            <span className="input-group-text" onClick={(e) => removesBox(e,key)}><i className="bx bx-trash cursor-pointer"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                                :
                                                <>
                                                    <div className="col-md-6">
                                                        <div className="mb-3">
                                                            <label for="defaultFormControlInput" className="form-label">Option {key+1}</label>
                                                            <div className="input-group"> <input type="text"  defaultValue={res.optionName} name="option3" className="form-control input-type" placeholder="option"/>
                                                            
                                                            </div>
                                                        </div>
                                                        <input type="hidden"  value={res.id}  className="form-control input-typeID"/>
                                                    </div>
                                                    
                                                    <div className="col-md-6">
                                                        <div className="mb-3">
                                                            <label for="defaultFormControlInput" className="form-label">Score {key+1}</label>
                                                            <input type="number" className="form-control score-type" placeholder="10" defaultValue={res.optionScore}/>
                                                            
                                                        </div>
                                                    </div>
                                                </>
                                                }
                                                    
                                        </div>
                                     )})
                                     :
                                     <>
                                     <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Option 1</label>
                                            <input type="text" className="form-control input-type" placeholder="Mobile-phone"/>
                                        </div>
                                    </div>
                                    
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score 1</label>
                                            <input type="number" className="form-control score-type" placeholder="10"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Option 2</label>
                                            <input type="text" className="form-control input-type" id="defaultFormControlInput" placeholder="10" aria-describedby="defaultFormControlHelp"/>
                                        </div>
                                    </div>
                                    
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score 2</label>
                                            <input type="number" className="form-control score-type" placeholder="10"/>
                                        </div>
                                    </div>
                                     </>
                                    }
                                    
                                    {selectopt.map((result,index) => {
                                        return (
                                            result)})}
                                </div>
                                
                                
                            </>
                            :
                            (multiImg) ?
                            <>
                                <div className="row">
                                    <div className="col-12 text-end">
                                        <a type="button" className="btn btn-primary" onClick={addMgOption}>
                                            <span className="tf-icons bx bx-plus"></span>&nbsp; Add Option
                                        </a>
                                    </div>
                                    <span className="error" style={{ color: "red" }}>{errors['imgOption']}</span>
                                </div>
                                <div className="row">
                                {(question.options.length>0) ?
                                    (question.options).map((res,key,row) => {
                                    
                                    return (
                                        <div className='row' id={key}>
                                            {(key > 1) ? 
                                            <>
                                                <div className="col-md-6">
                                                    <div className="mb-3">
                                                        <label for="defaultFormControlInput" className="form-label">Option {key+1}</label>
                                                        
                                                        <input type="file" className="form-control input-file" id="inputGroupFile02"/>
                                                        
                                                        <input type="hidden" className="input-files" value={res.optionImage}/>
                                                    </div>
                                                    <a href={'http://44.230.62.131/PixelHealth/uploads/'+res.optionImage} target="_blank"><span>{res.optionImage}</span></a>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="mb-3">
                                                        <label for="defaultFormControlInput" className="form-label">Score {key+1}</label>
                                                        <div className="input-group"><input type="number" className="form-control score-file" placeholder="10" defaultValue={res.optionScore}/>
                                                        <span className="input-group-text" onClick={(e) => removemgBox(e,key)}><i className="bx bx-trash cursor-pointer"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </>
                                            :
                                            <>
                                                <div className="col-md-6">
                                                    <div className="mb-3">
                                                        <label for="defaultFormControlInput" className="form-label">Option {key+1}</label>
                                                        
                                                       
                                                        <input type="file" className="form-control input-file" id="inputGroupFile02"/>
                                                        
                                                        
                                                        <input type="hidden" className="input-files" value={res.optionImage}/>
                                                    </div>
                                                    <a href={'http://44.230.62.131/PixelHealth/uploads/'+res.optionImage} target="_blank"><span>{res.optionImage}</span></a>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="mb-3">
                                                        <label for="defaultFormControlInput" className="form-label">Score {key+1}</label>
                                                        <input type="number" className="form-control score-file" placeholder="10" defaultValue={res.optionScore}/>
                                                    </div>
                                                </div>
                                            </>
                                            }
                                        </div>
                                    )})
                                :
                                <>
                                <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Option 1</label>
                                            <input type="file" className="form-control input-file" id="inputGroupFile02"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score 1</label>
                                            <input type="number" className="form-control score-file" placeholder="10"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Option 2</label>
                                            <input type="file" className="form-control input-file" id="inputGroupFile02"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score 2</label>
                                            <input type="number" className="form-control score-file" placeholder="10"/>
                                        </div>
                                    </div>
                                </>
                                }
                                    
                                    {imgopt.map((result,index) => {
                                        return (
                                            result)})}
                                </div>
                                <div className='multimg'></div>
                                
                            </>
                            :
                            <>
                            <div className='multis'></div>
                                <div className='multimg'></div>
                            </>
                            }
                            <div className="row">
                                
                                <div className="col-md-12">
                                        <div className="form-check form-check-inline mt-2">
                                            <input className="form-check-input" type="radio" name="questionCategory" id="inlineRadio1" value="Text" checked={(question.questionCategory=='Text')} onChange={e => onQstChange(e)}/>
                                            <label className="form-check-label" for="inlineRadio1">Only Text</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="questionCategory" id="inlineRadio2" checked={(question.questionCategory=='TextWithImage')} value="TextWithImage" onChange={e => onQstChange(e)}/>
                                            <label className="form-check-label" for="inlineRadio2">Text With Image</label>
                                        </div>
                                </div>
                                {(qst) ? 
                                <>
                                <div className="col-md-7">
                                        <div className="mb-3">
                                            <label for="exampleFormControlTextarea1" className="form-label">Question</label>
                                            <textarea className="form-control" id="exampleFormControlTextarea1" name="question" rows="4" placeholder="How do you feel about your digital content usage?" spellCheck="false" value={question.question} onChange={e => onInputChange(e)}></textarea>
                                            <span className="error" style={{ color: "red" }}>{errors['question']}</span>
                                        </div>
                                    </div>
                                    <div className="col-md-5">
                                        <div className="mb-3">
                                            <div className="d-flex align-items-start align-items-sm-center gap-4">
                                                <img src={file} alt="user-avatar" className="d-block rounded" height="100" width="100" id="uploadedAvatar"/>
                                                <div className="button-wrapper">
                                                    <label for="upload" className="btn btn-sm btn-primary me-2 mb-4" tabIndex="0">
                                                        <span className="d-none d-sm-block">Upload</span>
                                                        <i className="bx bx-upload d-block d-sm-none"></i>
                                                        <input type="file" id="upload" className="account-file-input" name="qstFile" hidden accept="image/png, image/jpeg" onChange={e => onFileChange(e,'qst')}/>
                                                    </label>
                                                    
                                                    <span className="error" style={{ color: "red" }}>{errors['qstFile']}</span>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </>
                                :
                                <>
                                    <div className="col-md-12">
                                        <div className="mb-3">
                                            <label for="exampleFormControlTextarea1" className="form-label">Question</label>
                                            <textarea className="form-control" id="exampleFormControlTextarea1" name="question" rows="4" placeholder="How do you feel about your digital content usage?" spellCheck="false" value={question.question} onChange={e => onInputChange(e)}></textarea>
                                            <span className="error" style={{ color: "red" }}>{errors['question']}</span>
                                        </div>
                                    </div>
                                </>
                                }
                            </div>  
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
                        {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
};

export default EditQuestion