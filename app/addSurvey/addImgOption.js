import React from 'react'

const AddImgOption = ({onClick, rand,opt}) => {

    //This is child Component to add Image Option, when You click on Add option after selection Multiple Or Single Image Choice
    const handleDelete = (e) =>{
        onClick(rand);
    }
      return (
        <div className="row" id={rand}>
            <div className="col-md-6">
                <div className="mb-3">
                    <label for="defaultFormControlInput" className="form-label">Option {opt}</label>
                    <div className="input-group">
                        <input type="file" class="form-control input-file" id="inputGroupFile02"/>
                    </div>
                </div>
            </div>
            <div className="col-md-6">
                <div className="mb-3">
                    <label for="defaultFormControlInput" className="form-label">Score {opt}</label>
                    <div className="input-group">
                        <input type="number" className="form-control score-file" placeholder="10"/>
                        <span className="input-group-text" onClick={handleDelete}><i className="bx bx-trash cursor-pointer"></i></span>
                    </div>
                </div>
            </div>
        </div>
        
      )
}

export default AddImgOption