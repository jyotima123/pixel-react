import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import AddSelectOption from './addSelectOption';
import AddImgOption from './addImgOption';

const AddQuestion = ({onClose,group_id}) => {

    const [isLoading, setLoading] = useState(false);
    const[likert,setLikert] = useState(false);
    const[multiSelect,setMultiSelect] = useState(false);
    const[multiImg,setMultiImg] = useState(false);
    const[qst,setQst] = useState(false);
    const[msopt,setMsopt] = useState(3);
    const[selectopt,setSelectOpt] = useState([]);
    const[imgopt,setImgOpt] = useState([]);
    const[msmgopt,setMsmgopt] = useState(3);
    const[file,setFile] = useState("assets/img/question-img.png");
    const [question, setQuestion] = useState({
        groupId: group_id,
        questionType: "",
        questionCategory:"Text",
        bracket:"wellness",
        question:"",
        questionImage:"",
        optionNumber:"",
        optionNames:[],
        optionImages:[],
        scaleMin:"",
        scaleMax:"",
        scaleMinScore:"",
        scaleMaxScore:"",
        scaleMinPointer:"",
        scaleMaxPointer:""

      });
      const [errors, setError] = useState({
        errors: "",
      });

      //return to Parent Node
    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    //save input data into state variable
    const onInputChange = e => {

        setQuestion({ ...question, [e.target.name]: e.target.value });
        
    };

    //It defines the dynamic design based on response selection change.
    const onResponseChange = async(e) => {

        setSelectOpt([])
        setImgOpt([])

        setQuestion({ ...question, [e.target.name]: e.target.value });
        

        if(e.target.value == 'Likert Scale')
        {
            setLikert(true)
            setMultiSelect(false)
            setMultiImg(false)
            setMsopt(3)
            setMsmgopt(3)

        }
        else if(e.target.value == 'Multiple Image Choice' || e.target.value == 'Single Image Choice')
        {
            setMultiSelect(false)
            setLikert(false)
            setMultiImg(true)
            setMsopt(3)
        }
        else if(e.target.value == 'Free Text')
        {
            setMultiSelect(false)
            setLikert(false)
            setMultiImg(false)
            setMsopt(3)
            setMsmgopt(3)
        }
        else{
            setMultiSelect(true)
            setLikert(false)
            setMultiImg(false)
            setMsmgopt(3)
            
        }
        
    };

    //When you select Mulitple or Single Choice Response type , Then this function will work to call its child node to add extra options.
    const addOption = () => {

        setMsopt((msopt) => (msopt+1))

        const min = 1;
        const max = 100;
        const rand = min + Math.random() * (max - min);

        setSelectOpt([ ...selectopt, <AddSelectOption rand={rand} opt={msopt} onClick={removeBox}/> ]);
       
    }
   //remove extra options, which is generated by above function
    const removeBox = (rand)=>{
        
        const list = document.getElementById(rand);        
        if(document.getElementById(rand))        
        {            
            document.getElementById(rand).removeChild(list.firstElementChild); 
            document.getElementById(rand).removeChild(list.lastElementChild);        
        }
        
    }

    const addMgOption = () => {

        setMsmgopt((msmgopt) => (msmgopt+1))

        const min = 1;
        const max = 100;
        const rand = min + Math.random() * (max - min);

        setImgOpt([ ...imgopt, <AddImgOption rand={rand} opt={msmgopt} onClick={removeBox}/> ]);      

    }
    

    const onQstChange = (e) => {
        setQuestion({ ...question, [e.target.name]: e.target.value });
        setQst((prev) => !prev)
    }
    const onFileChange = async(e,type) => {
        
        if(type == 'qst')
        {
            
            var ext = checkFileExtension(e.target.files[0].name)
            if(ext == 'png' || ext == 'jpeg' || ext == 'jpg')
            {
                setQuestion({ ...question, 'questionImage': e.target.files[0] });
                var img = URL.createObjectURL(e.target.files[0]);
                setFile(img);
            }
            else{
                let errors = {};
                errors["qstFile"] = "Please upload only PNG,JPEG,JPG Files";
                setError(errors);
            }
        }
         
    }

    const checkFileExtension = (fileName) => {
        // fileName = document.querySelector('#file1').value;
        var extension = fileName.substring(fileName.lastIndexOf('.') + 1);
        return extension
    };

    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        
        //username
        if(!question["question"]){
        formIsValid = false;
        errors["question"] = "Question Text Cannot be empty";
        }
        if(!question["questionType"]){
        formIsValid = false;
        errors["questionType"] = "Please select any type";
        }
        if(question["questionType"] == 'Multiple Choice' || question["questionType"] == 'Single Choice')
        {

            var els = document.getElementsByClassName("score-type");
           
            Array.prototype.forEach.call(els, function(el) {
                if(!el.value)
                {
                    formIsValid = false;
                    errors["selectOption"] = "Don't leave any Score blank";
                }
                
            });

            var els = document.getElementsByClassName("input-type");
           
            Array.prototype.forEach.call(els, function(el) {
                if(!el.value)
                {
                    formIsValid = false;
                    errors["selectOption"] = "Don't leave any Option blank";
                }
                
            });

        }
        if(question["questionType"] == 'Likert Scale')
        {
            if(!question["scaleMin"])
            {
                formIsValid = false;
                errors["scaleMin"] = "Please enter minimum value";
            }
            if(!question["scaleMax"])
            {
                formIsValid = false;
                errors["scaleMax"] = "Please enter maximum value";
            }
            if(!question["scaleMinScore"])
            {
                formIsValid = false;
                errors["scaleMinScore"] = "Please enter score for minimum value";
            }
            if(!question["scaleMaxScore"])
            {
                formIsValid = false;
                errors["scaleMaxScore"] = "Please enter score for maximum value";
            }
            if(!question["scaleMinPointer"])
            {
                formIsValid = false;
                errors["scaleMinPointer"] = "Please enter pointer";
            }
            if(!question["scaleMaxPointer"])
            {
                formIsValid = false;
                errors["scaleMaxPointer"] = "Please enter pointer";
            }

        }
        if(question["questionType"]== 'Multiple Image Choice' || question["questionType"]== 'Single Image Choice')
        {

            var els = document.getElementsByClassName("score-file");
            
            Array.prototype.forEach.call(els, function(el) {
                
                if(!el.value)
                {
                    formIsValid = false;
                    errors["imgOption"] = "Don't leave any Score blank";
                }
                
            });
            
            var els = document.getElementsByClassName("input-file");
            
            Array.prototype.forEach.call(els, function(el) {
                
                if(!el.files[0])
                {
                    formIsValid = false;
                    errors["imgOption"] = "Don't leave any Option blank";
                }
                
            });

            
            
        }
        
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        
        if(handleValidation()){

            const data = new FormData();
            
            
            data.append("groupId",question.groupId);
            data.append('questionType',question.questionType);
            data.append("questionCategory",question.questionCategory);  
            data.append("bracket",question.bracket);
            data.append("question",question.question);
            if(question.questionCategory == 'TextWithImage')
            {
                var time = new Date().getTime();
                var ext = checkFileExtension(question.questionImage.name)
                var qstpic = 'qst_'+time+'.'+ext;

                data.append("questionImage",question.questionImage,qstpic); 
            }
            if(question.questionType == 'Likert Scale')
            {
                data.append("scaleMin",question.scaleMin);
                data.append("scaleMax",question.scaleMax);
                data.append("scaleMinScore",question.scaleMinScore);
                data.append("scaleMaxScore",question.scaleMaxScore);
                data.append("scaleMinPointer",question.scaleMinPointer);
                data.append("scaleMaxPointer",question.scaleMaxPointer);
            }
            if(question.questionType == 'Multiple Choice' || question.questionType == 'Single Choice')
            {
                var els = document.getElementsByClassName("input-type");
                let types = []
                Array.prototype.forEach.call(els, function(el) {
                    types.push(el.value);
                });

                var els = document.getElementsByClassName("score-type");
                let score = []
                Array.prototype.forEach.call(els, function(el) {
                    score.push(el.value);
                });

                const selectedScope = Object.fromEntries(
                    types.map((typesvalue, index) => [typesvalue, score[index]])
                );


                data.append("optionNames",JSON.stringify(types));
                data.append("optionScore",JSON.stringify(selectedScope));
            }
            if(question.questionType == 'Multiple Image Choice' || question.questionType == 'Single Image Choice')
            {
                var els = document.getElementsByClassName("input-file");
                let types = [] 
                let i = 0
                Array.prototype.forEach.call(els, function(el) {

                    var time = new Date().getTime();
                    var ext = checkFileExtension(el.files[0].name)
                    var optpic = 'opt_'+time+i+'.'+ext;
                    
                    data.append("optionImages",el.files[0],optpic);
                     i = i+1
                     types.push(optpic);
                });
               
                var els = document.getElementsByClassName("score-file");
                let score = []
                Array.prototype.forEach.call(els, function(el) {
                    score.push(el.value);
                });
               
                const selectedScope = Object.fromEntries(
                    types.map((typesvalue, index) => [typesvalue, score[index]])
                );
                

                data.append("optionScore",JSON.stringify(selectedScope));
            }

            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/question/add", {
                        method: "POST",
                        body: data,
                    }).then(res => res.json())
                    .then(res => {
                        
                        setLoading(false);
                        
                        if(res.success)
                        {
                            onClose("success");
                        }
                        else{
                            onClose("error");
                        }
                    });
          }
          else{
            setLoading(false);
          }
       
      };


      const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Add Question</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                    </button>
                    </div>
                    
                    <div className="modal-body">
                        <form>
                            <div className="row">
                            <div className="col-md-12">
                                    <div className="mb-3">
                                        <label for="defaultSelect" className="form-label">Select Category</label>
                                            <select id="defaultSelect" name="bracket" className="form-select" onChange={e => onInputChange(e)}>
                                                <option value="Wellness">Wellness</option>
                                                <option value="Knowledge">Knowledge</option>
                                            </select>
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <div className="mb-3">
                                        <label for="defaultSelect" className="form-label">Select Response Type</label>
                                            <select id="defaultSelect" name="questionType" className="form-select" onChange={e => onResponseChange(e)}>
                                                <option value="">Select Response Type</option>
                                                <option value="Likert Scale">Likert Scale</option>
                                                <option value="Multiple Choice">Multiple Choice</option>
                                                <option value="Multiple Image Choice">Multiple Image Choice</option>
                                                <option value="Single Choice">Single Choice</option>
                                                <option value="Single Image Choice">Single Image Choice</option>
                                                <option value="Free Text">Free Text</option>
                                            </select>
                                            <span className="error" style={{ color: "red" }}>{errors['questionType']}</span>
                                    </div>
                                    
                                </div>
                            </div>
                            
                            {(likert) ?
                            <>
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Scale Minimum Pointer</label>
                                            <input type="text" className="form-control" id="defaultFormControlInput" name="scaleMinPointer" onChange={e => onInputChange(e)} placeholder="Poor" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMinPointer']}</span>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Scale Minimum</label>
                                            <input type="text" className="form-control" id="defaultFormControlInput" name="scaleMin" onChange={e => onInputChange(e)} placeholder="0" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMin']}</span>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score</label>
                                            <input type="text" className="form-control" id="defaultFormControlInput" name="scaleMinScore" onChange={e => onInputChange(e)} placeholder="10" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMinScore']}</span>
                                        </div>
                                    </div>
                                </div> 
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Scale Maximum Pointer</label>
                                            <input type="text" className="form-control" id="defaultFormControlInput" name="scaleMaxPointer" onChange={e => onInputChange(e)} placeholder="best" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMaxPointer']}</span>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Scale Maximum</label>
                                            <input type="text" className="form-control" id="defaultFormControlInput" name="scaleMax" onChange={e => onInputChange(e)} placeholder="0" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMax']}</span>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score</label>
                                            <input type="text" className="form-control" id="defaultFormControlInput" name="scaleMaxScore" onChange={e => onInputChange(e)} placeholder="10" aria-describedby="defaultFormControlHelp"/>
                                            <span className="error" style={{ color: "red" }}>{errors['scaleMaxScore']}</span>
                                        </div>
                                    </div>
                                </div> 
                            </>
                            :
                            (multiSelect) ?
                            <>
                                <div className="row">
                                    <div className="col-12 text-end">
                                        <a type="button" className="btn btn-primary" onClick={addOption}>
                                            <span className="tf-icons bx bx-plus"></span>&nbsp; Add Option
                                        </a>
                                    </div>
                                    <span className="error" style={{ color: "red" }}>{errors['selectOption']}</span>
                                </div>
                                <div className="row ">
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Option 1</label>
                                            <input type="text" className="form-control input-type" placeholder="Mobile-phone"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score 1</label>
                                            <input type="number" className="form-control score-type" placeholder="10"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Option 2</label>
                                            <input type="text" className="form-control input-type" id="defaultFormControlInput" placeholder="10" aria-describedby="defaultFormControlHelp"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score 2</label>
                                            <input type="number" className="form-control score-type" placeholder="10"/>
                                        </div>
                                    </div>
                                    {selectopt.map((result,index) => {
                                        return (
                                            result)})}
                                </div>
                                
                                
                            </>
                            :
                            (multiImg) ?
                            <>
                                <div className="row">
                                    <div className="col-12 text-end">
                                        <a type="button" className="btn btn-primary" onClick={addMgOption}>
                                            <span className="tf-icons bx bx-plus"></span>&nbsp; Add Option
                                        </a>
                                    </div>
                                    <span className="error" style={{ color: "red" }}>{errors['imgOption']}</span>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Option 1</label>
                                            <input type="file" className="form-control input-file" id="inputGroupFile02"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score 1</label>
                                            <input type="number" className="form-control score-file" placeholder="10"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Option 2</label>
                                            <input type="file" className="form-control input-file" id="inputGroupFile02"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="mb-3">
                                            <label for="defaultFormControlInput" className="form-label">Score 2</label>
                                            <input type="number" className="form-control score-file" placeholder="10"/>
                                        </div>
                                    </div>
                                    {imgopt.map((result,index) => {
                                        return (
                                            result)})}
                                </div>
                                <div className='multimg'></div>
                                
                            </>
                            :
                            <>
                            <div className='multis'></div>
                                <div className='multimg'></div>
                            </>
                            }
                            <div className="row">
                                
                                <div className="col-md-12">
                                        <div className="form-check form-check-inline mt-2">
                                            <input className="form-check-input" type="radio" name="questionCategory" id="inlineRadio1" value="Text" defaultChecked onChange={e => onQstChange(e)}/>
                                            <label className="form-check-label" for="inlineRadio1">Only Text</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" type="radio" name="questionCategory" id="inlineRadio2" value="TextWithImage" onChange={e => onQstChange(e)}/>
                                            <label className="form-check-label" for="inlineRadio2">Text With Image</label>
                                        </div>
                                </div>
                                {(qst) ? 
                                <>
                                <div className="col-md-7">
                                        <div className="mb-3">
                                            <label for="exampleFormControlTextarea1" className="form-label">Question</label>
                                            <textarea className="form-control" id="exampleFormControlTextarea1" name="question" rows="4" placeholder="How do you feel about your digital content usage?" spellCheck="false" onChange={e => onInputChange(e)}></textarea>
                                            <span className="error" style={{ color: "red" }}>{errors['question']}</span>
                                        </div>
                                    </div>
                                    <div className="col-md-5">
                                        <div className="mb-3">
                                            <div className="d-flex align-items-start align-items-sm-center gap-4">
                                                <img src={file} alt="user-avatar" className="d-block rounded" height="100" width="100" id="uploadedAvatar"/>
                                                <div className="button-wrapper">
                                                    <label for="upload" className="btn btn-sm btn-primary me-2 mb-4" tabIndex="0">
                                                        <span className="d-none d-sm-block">Upload</span>
                                                        <i className="bx bx-upload d-block d-sm-none"></i>
                                                        <input type="file" id="upload" className="account-file-input" name="qstFile" hidden accept="image/png, image/jpeg" onChange={e => onFileChange(e,'qst')}/>
                                                    </label>
                                                    
                                                    <span className="error" style={{ color: "red" }}>{errors['qstFile']}</span>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </>
                                :
                                <>
                                    <div className="col-md-12">
                                        <div className="mb-3">
                                            <label for="exampleFormControlTextarea1" className="form-label">Question</label>
                                            <textarea className="form-control" id="exampleFormControlTextarea1" name="question" rows="4" placeholder="How do you feel about your digital content usage?" spellCheck="false" onChange={e => onInputChange(e)}></textarea>
                                            <span className="error" style={{ color: "red" }}>{errors['question']}</span>
                                        </div>
                                        
                                    </div>
                                </>
                                }
                            </div>  
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
                        {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
}

export default AddQuestion