"use client";

import React, { useEffect,useState } from 'react';
import { useRouter } from 'next/navigation'
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { BsColumnsGap } from "react-icons/bs";
import { useSession } from "next-auth/react";
import dynamic from 'next/dynamic';
// import Chart from "react-apexcharts";


const Dashboard = () => {
    const { data: session } = useSession() 
    const router = useRouter()
    const[leftSide,setLeft] = useState(null);
    const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });
    const[dashboard,SetDashboard] = useState({
        Grades: "",
        Teachers:"",
        Students: 0,
        completed: 0,
        Checkins: 0,
        circulated: 0,
        answered:0,
        GradeChart: {}
    });

    

    const [columnChart, setColumn] = React.useState({
        series: [{
            name: 'Check-ins by Grade',
            data: []
        }],
        options: {
            colors: ['#641cec'],
            chart: {
                type: 'bar',
                height: 250
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: false,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: [],
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false,
                },
            },
            fill: {
                opacity: 1
            },
            yaxis: {
                show: false
            },
            grid: {
                show: false
            }
        }
      });

     


      useEffect(() => {
    
        async function fetchData()
        {
            
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSchoolDashboard?id="+session?.user.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(res.gradeList)
                SetDashboard(list.output)
                
                let bchartArray = {...columnChart,'series': [{
                    name: 'Check-ins by Grade',
                    data: list.output.GradeChart.values
                }]}
                bchartArray = {...bchartArray,'options': {
                        xaxis: {
                            categories: list.output.GradeChart.labels,
                        },
                        colors: ['#641cec'],
                        fill: {
                            opacity: 1
                        },
                        yaxis: {
                            show: false
                        },
                        grid: {
                            show: false
                        },
                        stroke: {
                            show: false,
                            width: 2,
                            colors: ['transparent']
                        },
                        dataLabels: {
                            enabled: false
                        }
                    }}
        
                    setColumn(bchartArray)
                
            });

            
        }

        if(session?.user.role == 1)
        {
            fetchData();
        }
            
        
      }, [session]);
     
      const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }

  return (
    <>
    {/* {session?.user ? ( */}
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="dashboard" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                    <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                        <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                            <i className="bx bx-menu bx-sm"></i>
                        </a>
                    </div>
                    <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                        <div className="row d-flex align-items-center">
                            <h4 className="main-title">School Dashboard</h4>
                        </div>
                        
                        <Header />
                        
                    </div>
                </nav>

                <div className="container-xxl flex-grow-1 container-p-y">

                    <div className="card">
                        <div className="card-header d-flex align-items-center justify-content-between">
                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                                <h3 className="sub-heading m-0 p-0"><BsColumnsGap style={{verticalAlign:'text-top'  }}/><span style={{paddingLeft:6}}>Overview</span> </h3>
                            </div>
                            {/* <div className="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                                <input type="date" className="input-group-md form-control bg-date"/>
                            </div> */}
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="co1-12">
                                    <div className="row">
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <a href="/schoolGrades">
                                                <div className="card RgCard bg-themePx">
                                                    <div className="card-body">
                                                        <div className="d-flex align-items-start justify-content-between">
                                                            <h6 className="Rg-subHeading d-block mb-1 clr-theme">Grades</h6>
                                                            <div className="avatar avatar-md flex-shrink-0">
                                                                <img src="/assets/img/icons/pixelhealth/1.png" alt="School" className="rounded"/>
                                                            </div>
                                                        </div>

                                                        <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.Grades}</h1>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <a href="/teachers">
                                                <div className="card RgCard bg-primaryPx">
                                                    <div className="card-body">
                                                        <div className="d-flex align-items-start justify-content-between">
                                                            <h6 className="Rg-subHeading d-block mb-1 clr-primary">Teachers</h6>
                                                            <div className="avatar avatar-md flex-shrink-0">
                                                                <img src="/assets/img/icons/pixelhealth/2.png" alt="Grades" className="rounded"/>
                                                            </div>
                                                        </div>

                                                        <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.Teachers}</h1>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <a href="/students">
                                                <div className="card RgCard bg-infoPx">
                                                    <div className="card-body">
                                                        <div className="d-flex align-items-start justify-content-between">
                                                            <h6 className="Rg-subHeading d-block mb-1 clr-info">Students</h6>
                                                            <div className="avatar avatar-md flex-shrink-0">
                                                                <img src="/assets/img/icons/pixelhealth/3.png" alt="Students" className="rounded"/>
                                                            </div>
                                                        </div>

                                                        <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.Students}</h1>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="co1-12">
                                    <div className="row">
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <a href="/lessons">
                                                <div className="card RgCard bg-themePx">
                                                    <div className="card-body">
                                                        <div className="d-flex align-items-start justify-content-between">
                                                            <h6 className="Rg-subHeading d-block mb-1 clr-theme">Lessons Completed</h6>
                                                            <div className="avatar avatar-md flex-shrink-0">
                                                                <img src="/assets/img/icons/pixelhealth/4.png" alt="Schools Districts" className="rounded"/>
                                                            </div>
                                                        </div>

                                                        <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.completed}</h1>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <div className="card RgCard bg-primaryPx">
                                                <div className="card-body">
                                                    <div className="d-flex align-items-start justify-content-between">
                                                        <h6 className="Rg-subHeading d-block mb-1 clr-primary">Survey Sent</h6>
                                                        <div className="avatar avatar-md flex-shrink-0">
                                                            <img src="/assets/img/icons/pixelhealth/5.png" alt="Total Grade Check-in" className="rounded"/>
                                                        </div>
                                                    </div>

                                                    <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.circulated}</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <div className="card RgCard bg-infoPx">
                                                <div className="card-body">
                                                    <div className="d-flex align-items-start justify-content-between">
                                                        <h6 className="Rg-subHeading d-block mb-1 clr-info">Survey Completed</h6>
                                                        <div className="avatar avatar-md flex-shrink-0">
                                                            <img src="/assets/img/icons/pixelhealth/6.png" alt="Total Students Circulations" className="rounded"/>
                                                        </div>
                                                    </div>

                                                    <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.answered}</h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="col-xxl-12 col-md-12">
                                <div className="border rounded">
                                    <div className="card-header d-flex align-items-center justify-content-between">
                                        <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                                            <h5 className="fw-bold m-0 p-0">Check-in by Grade</h5>
                                        </div>
                                        <div className="col-lg-2 col-md-6 col-sm-6 col-xs-6">
                                            {/* <select id="defaultSelect" className="form-select">
                                                <option>2022</option>
                                                <option value="1">2018</option>
                                                <option value="2">2019</option>
                                                <option value="3">2021</option>
                                                <option value="3">2022</option>
                                            </select> */}
                                        </div>
                                    </div>

                                    <div className="card-body">

                                        
                                        <div id="columnChart">
                                            <Chart
                                                options={columnChart.options}
                                                series={columnChart.series}
                                                type="bar"
                                                height="250"
                                            />
                                        </div>

                                        
                                       

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
    {/* // ):
    // <>
    // {router.push('/login')}
    // </>
    // } */}
    </>
  )
}

export default Dashboard