"use client"
import { useEffect, useState } from "react";
import { useRouter } from 'next/navigation'
import $ from "jquery";
import jzip from 'jzip';
import Header from '@components/Header'
import Sidebar from '@components/Sidebar'
import Select from "react-select";
import { useSession } from "next-auth/react";


const page = () => {
    const { data: session } = useSession()
    const router = useRouter()
    const[districtData,SetDistrictData] = useState([]);
    const[districtId,setdistrictId] = useState();
    const[schoolData,SetSchoolData] = useState([]);
    const[schoolId,setschoolId] = useState();
    const[gradeData,SetGradeData] = useState([]);
    const[gradeId,setgradeId] = useState();
    const[questionData,SetQuestionData] = useState([]);
    const[questionId,setquestionId] = useState();
    const[surveyData,SetSurveyData] = useState([]);
    const[surveyId,setsurveyId] = useState();
    const[leftSide,setLeft] = useState(null);
    const[schoolReport,setSchoolReport] = useState({
        overall: "",
        wellness: "",
        knowledge: "",
        schoolData: []
    });
    const[filter,setFilter] = useState({
        districts: [],
        schools: [],
        grades: [],
        questions: [],
        surveys: [],
    })

    useEffect(() => {

        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            await import ('datatables.net-buttons/js/dataTables.buttons.min')
            await import ('datatables.net-buttons/js/buttons.html5.min')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": false, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              },
              dom: 'Bfrtip',
              buttons: [
                { extend: 'excelHtml5', text: 'Export',className: 'btn btn-theme btn-outline-theme me-2' }
              ]  
            });
        }
    
        callTable();
    
        
    },[schoolReport]);

    //effect on updating filter
    useEffect(() => {
    
        async function fetchData()
            {
                await import('datatables.net-dt/js/dataTables.dataTables')
                await import('datatables.net-dt/css/jquery.dataTables.min.css')
                
                window.JSZip = jzip;
    
                
                const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSchoolReport", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(filter),
                    cache: 'no-store'
                    }).then(res => res.json())
                    .then(schList => {
                            $('#RgTableX').DataTable().destroy();
                            // console.log(schList)
                            setSchoolReport(schList)
                            
                    });


                    const result1 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSchoolList", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify(filter),
                        cache: 'no-store'
                        }).then(res => res.json())
                        .then(list => {
                            
                        // console.log(list)
                        SetSchoolData(list.output)
                    });
    
            }
    
            fetchData();
      }, [filter]);

  useEffect(() => {
    
    async function fetchData()
        {

            const result = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getReportDistrictList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(list)
                SetDistrictData(list.output)
            });

            var data = {
                districts: []
            }

            const result1 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSchoolList", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(list)
                SetSchoolData(list.output)
            });

            var data = {
                schools: []
            }
            const result2 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getReportGradeList", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(list)
                SetGradeData(list.output)
            });

            const result3 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getQuestionList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(list)
                SetQuestionData(list.output)
            });

            const result4 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSurveyDropDownList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(list)
                SetSurveyData(list.output)
            });

        }

        fetchData();
  }, []);

  const changedistrict = async e => {

    var report = []
    for(var i = 0; i< e.length; i++)
    {
        report.push(e[i].value)
    }
    setFilter({ ...filter, 'districts': report});
    
    setdistrictId(e);
};

const changeschool = async e => {

    var report = []
    for(var i = 0; i< e.length; i++)
    {
        report.push(e[i].value)
    }
    setFilter({ ...filter, 'schools': report});
    
    setschoolId(e);
};

const changesurvey = async e => {

    var report = []
    for(var i = 0; i< e.length; i++)
    {
        report.push(e[i].value)
    }
    setFilter({ ...filter, 'surveys': report});
    
    setsurveyId(e);
}

const changequestion = async e => {

    var report = []
    for(var i = 0; i< e.length; i++)
    {
        report.push(e[i].value)
    }
    setFilter({ ...filter, 'questions': report});
    
    setquestionId(e);
};

const changeView = async e => {
    // console.log(e.target.value)
    router.push(process.env.NEXT_PUBLIC_URL+"/reports/"+e.target.value)
}

const generateReport = async () => {

    
    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/getSchoolReportInSheet', {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store'
        }).then(res => res.json())
        .then(res => {
            if(res.success)
            {
                let alink = document.createElement('a');
                alink.href = res.fileurl;
                alink.download = 'report.xlsx';
                alink.click();
            }
        });
}

const toggleMenu = () => {

    setLeft('layout-menu-expanded')
    
}
const menuCall = () => {

    setLeft(null)
    
}

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="reports" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Reports</h4>
                            </div>
                            <Header/>
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                        
                        <div className="card">
                            <div className="card-header">
                                <div className="row mb-3">
                                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4 mb-Rg-3">
                                        <label for="defaultSelect" className="form-label">Filter By</label>
                                        <Select 
                                            options={districtData}
                                            placeholder="School district"
                                            value={districtId}
                                            onChange={changedistrict}
                                            isSearchable={true}
                                            isMulti
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                        
                                    </div>
                                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4 mb-Rg-3">
                                        <label for="defaultSelect" className="form-label">Filter By</label>
                                        <Select 
                                            options={schoolData}
                                            placeholder="School"
                                            value={schoolId}
                                            onChange={changeschool}
                                            isSearchable={true}
                                            isMulti
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                    </div>
                                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4 mb-Rg-3">
                                        <label for="defaultSelect" className="form-label">Filter By</label>
                                        <Select 
                                            options={surveyData}
                                            placeholder="Survey"
                                            value={surveyId}
                                            onChange={changesurvey}
                                            isSearchable={true}
                                            isMulti
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                    </div>
                                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4 mb-Rg-3">
                                        <label for="defaultSelect" className="form-label">Filter By</label>
                                        <Select 
                                            options={questionData}
                                            placeholder="Question"
                                            value={questionId}
                                            onChange={changequestion}
                                            isSearchable={true}
                                            isMulti
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                    </div>
                                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4 mt-3 mb-Rg-3">
                                        {/* <a type="button" className="btn btn-theme btn-outline-theme me-2" onClick={generateReport}>
                                            <span className="bx bxs-download"></span>&nbsp; Export
                                        </a> */}
                                    </div>
                                    <div className="col-lg-2 col-md-4 col-sm-4 col-xs-4 ms-auto">
                                        <label for="defaultSelect" className="form-label clr-theme">Select View</label>
                                        <select id="defaultSelect" className="form-select" onChange={e => changeView(e)}>
                                            <option value="schoolReports">School</option>
                                            <option value="gradeReports">Grade</option>
                                            <option value="studentReports">Student</option>
                                            <option value="districtReports">School District</option>
                                        </select>
                                    </div>
                                </div>
                                <hr className="divider"/>
                                <div className="row">
                                    <div className="co1-12">
                                        <div className="row">
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-Rg-3">
                                                <div className="border rounded bg-themePx">
                                                    <div className="card-body p-3">
                                                        <div className="d-flex align-items-center justify-content-between">
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <h5 className="m-0 p-0">Overall</h5>
                                                            </div>
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-end">
                                                                <h1 className="Rg-Graph-Title hw-bold text-nowrap clr-theme m-0 p-0">{schoolReport.overall}</h1>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-Rg-3">
                                                <div className="border rounded bg-themePx">
                                                    <div className="card-body p-3">
                                                        <div className="d-flex align-items-center justify-content-between">
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <h5 className="m-0 p-0">Wellness</h5>
                                                            </div>
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-end">
                                                                <h1 className="Rg-Graph-Title hw-bold text-nowrap clr-theme m-0 p-0">{schoolReport.wellness}</h1>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-Rg-3">
                                                <div className="border rounded bg-themePx">
                                                    <div className="card-body p-3">
                                                        <div className="d-flex align-items-center justify-content-between">
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <h5 className="m-0 p-0">Knowledge</h5>
                                                            </div>
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-end">
                                                                <h1 className="Rg-Graph-Title hw-bold text-nowrap clr-theme m-0 p-0">{schoolReport.knowledge}</h1>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                    <table id="RgTableX" className="table RgTable" style={{width: "100%"}}>
                                        <thead className="RgBGTable">
                                            <tr>
                                                <th>School Name</th>
                                                <th>Response count</th>
                                                <th>Overall</th>
                                                <th>Wellness</th>
                                                <th>Knowledge</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {(schoolReport.schoolData.length > 0)?
                                        schoolReport.schoolData.map((result,key, row) => {
                                            return (
                                                <tr key={key}>
                                                <td>{result.schoolName}</td>
                                                <td>{result.response_count}</td>
                                                {/* <td>{Math.round((result.overall + Number.EPSILON) * 100) / 100}</td> */}
                                                <td>{result.overall}</td>
                                                <td>{result.wellness}</td>
                                                <td>{result.knowledge}</td>
                                                
                                            </tr>
                                            )})
                                            :
                                            <></>
                                        }
                                            
                                        </tbody>
                                    </table> 
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default page