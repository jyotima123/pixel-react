import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useFormik } from 'formik';
import Select from "react-select";



const Modal = ({ onClose, children, title }) => {
    const [isLoading, setLoading] = useState(false);
    const[countryData,SetcountryData] = useState([]);
    const[stateData,SetstateData] = useState([]);
    const[cityData,SetcityData] = useState([]);
    const[districtData,SetdistrictData] = useState([]);
    const[districtId,setdistrictId] = useState();
    const [school, setSchool] = useState({
        fullName: "",
        contactPerson:"",
        email: "",  
        phone: "",
        extension: "",
        address:"",
        country: 0,
        state:0,
        city:0,
        postalCode:0,
        district:0,
        schoolType:0,
        status:1
      });
      const [errors, setError] = useState({
        errors: "",
      });

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res,'');
    };

    useEffect(() => {
        async function fetchData()
        {
            const res = await fetch(process.env.NEXT_PUBLIC_URL+"/api/country", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                }
                }).then(res => res.json())
                .then(res => {
                    
                console.log(res.counrtyList)
                SetcountryData(res.counrtyList)
            console.log(countryData)});

            const result = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getLabelDistrictList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(districtList => {
                    
                console.log(districtList)
                SetdistrictData(districtList)
            console.log(districtData)});
        }
        fetchData();    
    },[]);

    const onCountryChange = async(e) => {

        setSchool({ ...school, [e.target.name]: e.target.value });

        const res = await fetch(process.env.NEXT_PUBLIC_URL+"/api/state/"+e.target.value, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                console.log(res.stateList)
                SetstateData(res.stateList)
            console.log(stateData)
            });
    };

    const onStateChange = async(e) => {
        
        setSchool({ ...school, [e.target.name]: e.target.value });

        const res = await fetch(process.env.NEXT_PUBLIC_URL+"/api/city/"+e.target.value, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                console.log(res.cityList)
                SetcityData(res.cityList)
                console.log(countryData);
            });
    };

    const onInputChange = e => {

        if(e.target.name == 'phone')
        {
            var contact = formatPhoneNumber(e.target.value)
            // console.log(contact)
            setSchool({ ...school, [e.target.name]: contact });
        }
        else{
            setSchool({ ...school, [e.target.name]: e.target.value });
        }
        
    };

    const changeDistrict = e => {
        console.log(e)
        setSchool({ ...school, 'district': e.value});
        setdistrictId(e);
    };

    const formatPhoneNumber = (phoneNumberString) => {
        var cleaned = ('' + phoneNumberString).replace(/\D/g, '');
        var match = phoneNumberString.match(/^([0-9]{3})([0-9]{3})([0-9]{4})$/);
        // console.log(match)
        if (match) {
          return  match[1] + ' ' + match[2] + ' ' + match[3];
        }
        return cleaned;
      }

    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        console.log(school);
        //username
        if(!school["fullName"]){
        formIsValid = false;
        errors["fullName"] = "School name Cannot be empty";
        }
        //contact person
        if(!school["contactPerson"]){
            formIsValid = false;
            errors["contactPerson"] = "Contact Person Cannot be empty";
            }
       
        //Email
         if(!school["email"]){
            formIsValid = false;
            errors["email"] = "Email cannot be empty";
          }
          
        //Mobile No.
        if(!school["phone"]){
            formIsValid = false;
            errors["phone"] = "Contact cannot be empty";
          }
            
            //Address
         if(!school["address"]){
            formIsValid = false;
            errors["address"] = "Address cannot be empty";
          }
          //Postal code
         if(!school["postalCode"]){
            formIsValid = false;
            errors["postalCode"] = "Postal code cannot be empty";
          }
         
    
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/school/add", {
            method: "POST",
            body: JSON.stringify(school),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success",res.message);
                }
                else{
                    onClose("error",res.message);
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };

      const customStyles = {
        control: base => ({
          ...base,
          height: 33,
          minHeight: 33,
          borderRadius: 5,
        })
      };

    

    const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Add School</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                        <form>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label for="defaultFormControlInput" className="form-label">School Name</label>
                                        <input type="text" placeholder="School" className="form-control" name="fullName" onChange={e => onInputChange(e)}/>
                        <span className="error" style={{ color: "red" }}>{errors['fullName']}</span>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label for="defaultFormControlInput" className="form-label">Contact Person</label>
                                        <input type="text" placeholder="Contact name" className="form-control" name="contactPerson" onChange={e => onInputChange(e)}/>
                        <span className="error" style={{ color: "red" }}>{errors['contactPerson']}</span>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label className="form-label" for="basic-icon-default-email">Email</label>
                                        <div class="input-group input-group-merge">
                                        <span className="input-group-text"><i className="bx bx-envelope"></i></span>
                                            <input type="text" id="basic-icon-default-email" className="form-control" placeholder="tom@example.com" name="email" onChange={e => onInputChange(e)}/>
                                            </div>
                        <span className="error" style={{ color: "red" }}>{errors['email']}</span>
                                        <div className="form-text">You can use letters, numbers &amp; periods</div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="mb-3">
                                        <label className="form-label" for="basic-icon-default-phone">Contact No.</label>
                                        <div className="input-group input-group-merge">
                                            <span id="basic-icon-default-phone2" className="input-group-text"><i className="bx bx-phone"></i></span>
                                            <input type="text" id="basic-icon-default-phone" value={school.phone} className="form-control phone-mask" placeholder="658 799 8941" maxLength={12} name="phone" onChange={e => onInputChange(e)}/>
                       
                                        </div>
                                        <span className="error" style={{ color: "red" }}>{errors['phone']}</span>
                                    </div>
                                </div>
                                <div className="col-md-2">
                                    <div className="mb-3">
                                        <label className="form-label" for="basic-icon-default-phone">Ext.</label>
                                        <div className="input-group input-group-merge">
                                            <input type="text" id="basic-icon-default-phone" value={school.extension} className="form-control phone-mask" placeholder="658" name="extension" onChange={e => onInputChange(e)}/>
                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="mb-3">
                                <label for="exampleFormControlTextarea1" className="form-label">Address</label>
                                <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" spellcheck="false" name="address" onChange={e => onInputChange(e)}></textarea>
                                
                        <span className="error" style={{ color: "red" }}>{errors['address']}</span>
                            </div>
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="mb-3">
                                        <label for="defaultSelect" className="form-label">Country</label>
                                        <select id="defaultSelect" className="form-select" name="country" onChange={e => onCountryChange(e)} required>
                                        <option>Select Country</option>
                                        {countryData.map((result,key) => {
                                            return (
                                                <option key={key} value={result.id}>{result.countryName}</option>
                                            )})}
                                        </select>
                                        
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="mb-3">
                                        <label for="defaultSelect" className="form-label">State</label>
                                        <select id="defaultSelect" name="state" className="form-select" onChange={e => onStateChange(e)} required>
                                            <option>Select State</option>
                                            {stateData.map((result,key) => {
                                            return (
                                                <option key={key} value={result.id}>{result.stateName}</option>
                                            )})}
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="mb-3">
                                        <label for="defaultSelect" className="form-label">City</label>
                                        <select id="defaultSelect" name="city" className="form-select" onChange={e => onInputChange(e)} required>
                                            <option>Select City</option>
                                            {cityData.map((result,key) => {
                                            return (
                                                <option key={key} value={result.id}>{result.cityName}</option>
                                            )})}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label for="defaultFormControlInput" className="form-label">Postal Code</label>
                                        <input type="number" className="form-control"  placeholder="45454" name="postalCode" onChange={e => onInputChange(e)}/>                                
                                <span className="error" style={{ color: "red" }}>{errors['postalCode']}</span>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label for="defaultSelect" className="form-label">School District</label>
                                        <Select 
                                            options={districtData}
                                            placeholder="Select District"
                                            value={districtId}
                                            onChange={changeDistrict}
                                            isSearchable={true}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                        {/* <select id="defaultSelect" name="district" className="form-select" onChange={e => onInputChange(e)} required>
                                        <option>Select District</option>
                                        {districtData.map((result,key) => {
                                            return (
                                                <option key={key} value={result.id}>{result.districtName}</option>
                                            )})}
                                        </select> */}
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label for="defaultSelect" className="form-label">School Type</label>
                                        <select id="defaultSelect" class="form-select" name="schoolType" onChange={e => onInputChange(e)}>
                                            <option value="0">Select School Type</option>
                                            <option value="1">Private</option>
                                            <option value="2">Public</option>
                                            <option value="3">Charter</option>
                                            <option value="4">Independent</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label d-block">Status</label>
                                        <div class="form-check form-check-inline mt-2">
                                            <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" defaultChecked onChange={e => onInputChange(e)}/>
                                            <label class="form-check-label" for="inlineRadio1">Active</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="0" onChange={e => onInputChange(e)}/>
                                            <label class="form-check-label" for="inlineRadio2">Inactive</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
            {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
};

export default Modal