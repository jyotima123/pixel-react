"use client";

import React, { useEffect,useState } from 'react';
import { useRouter } from 'next/navigation'
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { BsColumnsGap } from "react-icons/bs";
import { useSession } from "next-auth/react";
import dynamic from 'next/dynamic';
import $ from "jquery";
import jzip from 'jzip';


const Dashboard = () => {
    const { data: session } = useSession() 
    const router = useRouter()
    const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });
    const[surveyList,setSurveyList] = useState([]);
    const[leftSide,setLeft] = useState(null);
    const[dashboard,SetDashboard] = useState({
        Schools: "",
        Grades: "",
        Students: "",
        Districts: "",
        Checkins: 0,
        circulated: 0,
        answered:0,
        Piechart: {},
        CheckinsByGrade:{},
        CheckinsBySchoolType:{}
    });

    const [piChart, setPi] = React.useState({
        options: {
            series: [],
            colors: ['#00bdd6', '#8353e2', '#4069e5', '#ed7d2d'],
            labels: [],
            dataLabels: {
                enabled: false
            },
            chart: {
                height: 300,
                type: 'pie',
                toolbar: {
                    show: false
                }
            },
            legend: {
                show: false
            }
        }
      });

    const [barChart, setBar] = React.useState({
        series: [{
            name: 'check-ins',
            data: []
        }],
        options: {
            colors: ['#641cec'],
            labels: [],
            chart: {
                type: 'bar',
                height: 263,
                toolbar: {
                    show: false
                },
            },
            plotOptions: {
                bar: {
                    borderRadius: 1,
                    horizontal: true,
                },
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                labels: {
                    show: false
                }
            },
            yaxis: {
                show: false
            },
            grid: {
                show: false
            }
        }
      });

      const [schoolbarChart, setSchoolBar] = React.useState({
        series: [{
            name: 'check-ins',
            data: []
        }],
        options: {
            colors: ['#00bdd6'],
            labels: [],
            chart: {
                type: 'bar',
                height: 263,
                toolbar: {
                    show: false
                },
            },
            plotOptions: {
                bar: {
                    borderRadius: 1,
                    horizontal: true,
                },
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                labels: {
                    show: false
                }
            },
            yaxis: {
                show: false
            },
            grid: {
                show: false
            }
        }
      }); 

      useEffect(() => {
        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
            });
        }
    
        callTable();
        
    },[surveyList]);

      useEffect(() => {
    
        async function fetchData()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            window.JSZip = jzip;
            
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/dashboard", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(res.gradeList)
                SetDashboard(list.output)

                if(list.output.Schools > 0)
                {

                    let chartArray = {...piChart,'options': {
                        series: list.output.Piechart.values,
                        colors: ['#00bdd6', '#8353e2', '#4069e5', '#ed7d2d'],
                        labels: list.output.Piechart.labels,
                        dataLabels: { enabled: false },
                        legend: { show: false }
                    }}
                    
                    setPi(chartArray)

                    let bchartArray = {...barChart,'series': [{
                        name: 'check-ins',
                        data: list.output.CheckinsByGrade.values
                    }]}
                    bchartArray = {...bchartArray,'options': {
                        labels: list.output.CheckinsByGrade.labels,
                        dataLabels: { enabled: false },
                        legend: { show: false },
                        colors: ['#641cec'],
                        plotOptions: {
                            bar: {
                                borderRadius: 1,
                                horizontal: true,
                            },
                        },
                        xaxis: {
                            labels: {
                                show: false
                            }
                        },
                        yaxis: {
                            show: false
                        },
                        grid: {
                            show: false
                        }
                        }}
            
                        setBar(bchartArray)


                        let tchartArray = {...barChart,'series': [{
                            name: 'check-ins',
                            data: list.output.CheckinsBySchoolType.values
                        }]}
                        tchartArray = {...tchartArray,'options': {
                            labels: list.output.CheckinsBySchoolType.labels,
                            dataLabels: { enabled: false },
                            legend: { show: false },
                            colors: ['#00bdd6'],
                            plotOptions: {
                                bar: {
                                    borderRadius: 1,
                                    horizontal: true,
                                },
                            },
                            xaxis: {
                                labels: {
                                    show: false
                                }
                            },
                            yaxis: {
                                show: false
                            },
                            grid: {
                                show: false
                            }
                            }}
                
                            setSchoolBar(tchartArray)
                }
                else{
                    let chartArray = {...piChart,'options': {
                        series: [1],
                        colors: ['#f0f0f0'],
                        labels: ['No Data'],
                        // dataLabels: { enabled: false },
                        // legend: { show: false }
                    }}
                    
                    setPi(chartArray)
                }

            });


            const res1 = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/getSurveyDetailBySchools', {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(surveyList => {
                    if(surveyList.output.length > 0)
                    {
                        $('#RgTableX').DataTable().destroy();
                        setSurveyList(surveyList.output)
                    }
                        
                        
                });

            
        }

        if(session)
        {
            fetchData();
            
        }
        
            
        
      }, [session]);

      const generateReport = async () => {

    
        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/getStudentSurveyDataInSheet', {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                if(res.success)
                {
                    let alink = document.createElement('a');
                    alink.href = res.fileurl;
                    alink.download = 'fullreport.xlsx';
                    alink.click();
                }
            });
    }

    const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }
     

  return (
    <>
    {/* {session?.user ? ( */}
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="dashboard" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                    <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                        <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                            <i className="bx bx-menu bx-sm"></i>
                        </a>
                    </div>
                    <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                        <div className="row d-flex align-items-center">
                            <h4 className="main-title">Super Admin</h4>
                        </div>
                        
                        <Header />
                        
                    </div>
                </nav>

                <div className="container-xxl flex-grow-1 container-p-y">

                    <div className="card">
                        <div className="card-header d-flex align-items-center justify-content-between">
                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                                <h3 className="sub-heading m-0 p-0"><BsColumnsGap style={{verticalAlign:'text-top'  }}/><span style={{paddingLeft:6}}>Overview</span> </h3>
                            </div>
                            <div className="col-lg-3 col-md-6 col-sm-6 col-xs-6 text-end">
                                <a type="button" className="btn btn-theme btn-outline-theme me-2" onClick={generateReport}>
                                    <span className="bx bxs-download"></span>&nbsp; Export Full Data
                                </a>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="co1-12">
                                    <div className="row">
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <a href="/school">
                                                <div className="card RgCard bg-themePx">
                                                    <div className="card-body">
                                                        <div className="d-flex align-items-start justify-content-between">
                                                            <h6 className="Rg-subHeading d-block mb-1 clr-theme">Schools</h6>
                                                            <div className="avatar avatar-md flex-shrink-0">
                                                                <img src="/assets/img/icons/pixelhealth/1.png" alt="School" className="rounded"/>
                                                            </div>
                                                        </div>

                                                        <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.Schools}</h1>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <a href="/grades">
                                                <div className="card RgCard bg-primaryPx">
                                                    <div className="card-body">
                                                        <div className="d-flex align-items-start justify-content-between">
                                                            <h6 className="Rg-subHeading d-block mb-1 clr-primary">Grades</h6>
                                                            <div className="avatar avatar-md flex-shrink-0">
                                                                <img src="/assets/img/icons/pixelhealth/2.png" alt="Grades" className="rounded"/>
                                                            </div>
                                                        </div>

                                                        <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.Grades}</h1>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <div className="card RgCard bg-infoPx">
                                                <div className="card-body">
                                                    <div className="d-flex align-items-start justify-content-between">
                                                        <h6 className="Rg-subHeading d-block mb-1 clr-info">Students</h6>
                                                        <div className="avatar avatar-md flex-shrink-0">
                                                            <img src="/assets/img/icons/pixelhealth/3.png" alt="Students" className="rounded"/>
                                                        </div>
                                                    </div>

                                                    <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.Students}</h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="co1-12">
                                    <div className="row">
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <a href="/district">
                                                <div className="card RgCard bg-themePx">
                                                    <div className="card-body">
                                                        <div className="d-flex align-items-start justify-content-between">
                                                            <h6 className="Rg-subHeading d-block mb-1 clr-theme">Schools Districts</h6>
                                                            <div className="avatar avatar-md flex-shrink-0">
                                                                <img src="/assets/img/icons/pixelhealth/4.png" alt="Schools Districts" className="rounded"/>
                                                            </div>
                                                        </div>

                                                        <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.Districts}</h1>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <div className="card RgCard bg-primaryPx">
                                                <div className="card-body">
                                                    <div className="d-flex align-items-start justify-content-between">
                                                        <h6 className="Rg-subHeading d-block mb-1 clr-primary">Survey Sent</h6>
                                                        <div className="avatar avatar-md flex-shrink-0">
                                                            <img src="/assets/img/icons/pixelhealth/5.png" alt="Total Grade Check-in" className="rounded"/>
                                                        </div>
                                                    </div>

                                                    <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.circulated}</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                            <div className="card RgCard bg-infoPx">
                                                <div className="card-body">
                                                    <div className="d-flex align-items-start justify-content-between">
                                                        <h6 className="Rg-subHeading d-block mb-1 clr-info">Survey Completed</h6>
                                                        <div className="avatar avatar-md flex-shrink-0">
                                                            <img src="/assets/img/icons/pixelhealth/6.png" alt="Total Students Circulations" className="rounded"/>
                                                        </div>
                                                    </div>

                                                    <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.answered}</h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                
                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                    <div className="border rounded">
                                        <div className="card-header">
                                            <h5 className="fw-bold m-0 p-0">Schools By Type</h5>
                                        </div>
                                        <div className="">

                                        
                                            <div id="RgChart1"><Chart
                                                options={piChart.options}
                                                series={piChart.options.series}
                                                type="pie"
                                                height="300"
                                            /></div>

                                        
                                        </div>
                                    </div>
                                </div>
                            
                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                    <div className="border rounded">
                                        <div className="card-header">
                                            <h5 className="fw-bold m-0 p-0">Check-ins by Grade</h5>
                                        </div>
                                        <div className="">

                                        
                                            <div id="RgChart2">
                                            <Chart
                                                options={barChart.options}
                                                series={barChart.series}
                                                type="bar"
                                                height="263"
                                            />
                                            </div>

                                            
                                            

                                        </div>
                                    </div>
                                </div>
                            

                                
                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                    <div className="border rounded">
                                        <div className="card-header">
                                            <h5 className="fw-bold m-0 p-0">Check-ins by School Type</h5>
                                        </div>
                                        <div className="">

                                            
                                            <div id="RgChart3"> <Chart
                                                options={schoolbarChart.options}
                                                series={schoolbarChart.series}
                                                type="bar"
                                                height="263"
                                            /></div>

                                            
                                            

                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div className="row">
                                <div className="co1-12">
                                    <div className="table-responsive text-nowrap">
                                        <div className="container-fluid">
                                            {(session) ?
                                            <table id="RgTableX" className="table RgTable" style={{width:'100%'}}>
                                                <thead className="RgBGTable">
                                                    <tr>
                                                        <th>School Name</th>
                                                        <th>No.of Students</th>
                                                        <th>Survey Sent</th>
                                                        <th>Survey Completed</th>
                                                        <th>Student who completed 1 survey</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                {(surveyList.length > 0) ?
                                                surveyList.map((result,key, row) => {
                                                    return (
                                                        <tr key={key}>
                                                            <td>{result.schoolName}</td>
                                                            <td>{result.students}</td>
                                                            <td>{result.surveySent}</td>
                                                            <td>{result.surveyCompleted}</td>
                                                            <td>{result.studentSurveycount}</td>
                                                            
                                                        </tr>
                                                    )})
                                                    :
                                                    <></>
                                                    }
                                                    
                                                </tbody>
                                            </table>
                                            :
                                            <></>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>

                </div>
            </div>
        </div>
    </div>
    {/* // ):
    // <>
    // {router.push('/login')}
    // </>
    // } */}
    </>
  )
}

export default Dashboard