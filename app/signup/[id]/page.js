"use client";

import Image from 'next/image'
import { useRouter } from 'next/navigation'
import { useFormik } from 'formik';
import { useSearchParams,usePathname } from 'next/navigation'
import React, { useRef,useState } from "react";
import { signIn, signOut } from "next-auth/react"
import { alertService } from 'services';
import Alert  from '@components/Alert';

export default function Page() {
    const pathname = usePathname()
    const router = useRouter()
  const [isLoading, setLoading] = useState(false);
  const [options, setOptions] = useState({
    autoClose: true,
    keepAfterRouteChange: false
});
const sid = pathname.split('/')
  const [student, setStudent] = useState({
    encryptString: decodeURIComponent(sid[2]),
    fullName: "",
    email: "",  
    password:""
  });
  const [errors, setError] = useState({
    errors: "",
  });

  const onInputChange = e => {
    setStudent({ ...student, [e.target.name]: e.target.value });
};

const onConInputChange = e => {

    let errors = {};
    let formIsValid = true;
    let pwd = student.password
    if(pwd !== e.target.value)
    {
        formIsValid = false;
        errors["rePassword"] = "Password does not match";
    }      
    
    setError(errors);
    return formIsValid;
};

const handleValidation = (e) => {
    let errors = {};
    let formIsValid = true;
    
    //username
    if(!student["fullName"]){
    formIsValid = false;
    errors["fullName"] = "Name Cannot be empty";
    }
    
    //Email
     if(!student["email"]){
        formIsValid = false;
        errors["email"] = "Username cannot be empty";
      }

      if(!student["password"]){
        formIsValid = false;
        errors["password"] = "Plese provide your password";
      }
      
    setError(errors);
    return formIsValid;
}

const onSubmit = async e => {
    e.preventDefault();
    
    setLoading(true);
    if(handleValidation()){
       
        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/student/signup", {
        method: "POST",
        body: JSON.stringify(student),
        headers: {
            "Content-Type": "application/json",
        }
        }).then(res => res.json())
        .then(res => {

            setLoading(false);

            if(res.success)
            {
                alertService.success(res.message, options)
                router.push(process.env.NEXT_PUBLIC_URL+"/login")
            }
            else{
                alertService.error(res.message, options)
            }
            
        });

      }
      else{
        setLoading(false);
      }
   
  };
    

  return (
    <div class="login-bg">
        <div className="container">
            <div className="authentication-wrapper authentication-basic">
                <div className="row">
                    
                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-mb-none">
                        <div className="login-img">
                            <img src="/assets/img/home-bg.png" className="img-responsive"/>
                        </div>
                    </div>
                    
                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div className="position-container">
                            <div className="authentication-inner">
                            
                                <div className="card">
                                    <div className="card-body">
                                    
                                        <div className="app-brand justify-content-center">
                                            <a href="SuperAdmin-Dashboard.php" className="app-brand-link gap-2">
                                                <img src="/assets/img/pixel-logo.png" alt="" height="55px"/>
                                            </a>
                                        </div>
                                        
                                        <h3 className="mb-2 text-center fw-bold">Sign Up</h3>
                                        <p className="mb-4 text-center">Enter your details to signup into the portal </p>
                                        <Alert />
                                        <form className="mb-3">
                                           <div className="form-floating mb-3">
                                                <input type="text" name="fullName" onChange={e => onInputChange(e)} className="form-control" id="floatingInput" placeholder="John" />
                                                <label>Full Name</label>
                                                <span className="error" style={{ color: "red" }}>{errors['fullName']}</span>
                                            </div>
                                            
                                            <div className="form-floating mb-3">
                                                <input type="text" name="email" className="form-control" id="floatingInput" placeholder="username" onChange={e => onInputChange(e)}/>
                                                <label>Username</label>
                                                <span className="error" style={{ color: "red" }}>{errors['email']}</span>
                                            </div>
                                           
                                            <div className="form-floating mb-3">
                                                <input type="password" name="password" className="form-control" id="floatingPassword" placeholder="Enter at least 8+ characters" onChange={e => onInputChange(e)}/>
                                                <label>Password</label>
                                                <span className="error" style={{ color: "red" }}>{errors['password']}</span>
                                            </div>

                                            <div className="form-floating mb-3">
                                                <input type="password" name="repassword" className="form-control" id="floatingPassword" placeholder="Re-enter your password" onChange={e => onConInputChange(e)}/>
                                                <label>Confirm Password</label>
                                                <span className="error" style={{ color: "red" }}>{errors['rePassword']}</span>
                                            </div>
                                            
                                            {!isLoading &&<button type="button" className="btn btn-primary d-grid w-100" onClick={onSubmit}>sign up</button>}
                            {isLoading &&<button type="submit" className="btn btn-primary d-grid w-100" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                            </form>
                                            
                                    
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>

  )
}
