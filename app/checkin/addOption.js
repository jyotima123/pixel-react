import React,{ useEffect,useState } from 'react'

const AddOption = ({onClick, rand,survey,lesson,chopt}) => {
    const[opt,setOpt] = useState(false);

    const handleDelete = (e) =>{
        onClick(rand);
    }

    const onCheckinChange = (e) => {

        setOpt((prev) => !prev)
    }

      return (
       
        <div id={rand} className='checkin-type'>
            <div className='row'>
            <input type="hidden" className={'checks'+chopt} name="checkinid" value="new"/>
                <div className='col-md-10'>
                        <div className="mb-3">
                            <label className="form-label d-block">Select Check-in Type</label>
                            <div className="form-check form-check-inline mt-2">
                                <input className={"form-check-input checks"+chopt} type="radio" name={"type"+chopt}  value="1" defaultChecked onChange={e => onCheckinChange(e)}/>
                                <label className="form-check-label" for="inlineRadio1">Check-in</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input className={"form-check-input checks"+chopt} type="radio" name={"type"+chopt}  value="2" onChange={e => onCheckinChange(e)}/>
                                <label className="form-check-label" for="inlineRadio2">Lesson</label>
                            </div>
                        </div>
                </div>
                <div className="col-md-2 text-end">
                    <div className="mb-3">
                        <div className="demo-inline-spacing">
                            <button type="button" className="btn btn-sm btn-icon btn-primary" onClick={handleDelete}>
                                <span className="tf-icons bx bx-minus"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {(opt) ? 
            <div className="row d-flex align-items-center">
            
                <div className="col-md-5">
                    <div className="mb-3">
                        <label for="defaultFormControlInput" className="form-label">Lesson Name</label>
                        <select id="defaultSelect" className={"form-select checks"+chopt} name="lessonId">
                                     <option value="">Select Lesson</option>
                                     {lesson.map((res,key) => {
                                        return(
                                            <option value={res.id}>{res.lessonName}</option>
                                        )
                                     })}
                                 </select>
                    </div>
                </div>
                <div className="col-md-5">
                    <div className="mb-3">
                        <label for="defaultSelect" className="form-label">Survey</label>
                        <select id="defaultSelect" className={"form-select survey checks"+chopt} name="surveyId">
                            <option value="">Select Survey</option>
                            {survey.map((res,key) => {
                                return(
                                    <option value={res.id}>{res.surveyName}</option>
                                )
                            })}
                        </select>
                    </div>
                </div>
                
            </div>
            :
            <div className="row d-flex align-items-center">
            
                <div className="col-md-5">
                    <div className="mb-3">
                        <label for="defaultFormControlInput" className="form-label">Check-in Name</label>
                        <input type="text" className={"form-control checks"+chopt} id="defaultFormControlInput" placeholder="Lorem Ipsum" aria-describedby="defaultFormControlHelp" name="checkinName"/>
                    </div>
                </div>
                <div className="col-md-5">
                    <div className="mb-3">
                        <label for="defaultSelect" className="form-label">Survey</label>
                        <select id="defaultSelect" className={"form-select survey checks"+chopt} name="surveyId">
                            <option value="">Select Survey</option>
                            {survey.map((res,key) => {
                                return(
                                    <option value={res.id}>{res.surveyName}</option>
                                )
                            })}
                        </select>
                    </div>
                </div>
                
            </div>
            }
        </div>
          
        
      )
}

export default AddOption