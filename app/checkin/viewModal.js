import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useFormik } from 'formik';
import Select from "react-select";
import { useSession } from "next-auth/react";
import AddOption from './addOption';

const ViewModal = ({ onClose, children, title ,checkin_id}) => {
    const { data: session } = useSession()
    const [isLoading, setLoading] = useState(false);
    const[gradeData,SetGradeData] = useState([]);
    const[schoolData,setSchoolData] = useState([]);
    const[surveyData,setSurveyData] = useState([]);
    const[lessonData,setLessonData] = useState([]);
    const[gradeId,setgradeId] = useState();
    const[opt,setOpt] = useState(false);
    const[options,setOptions] = useState(null)
    const[msopt,setMsopt] = useState(2);
    const[chkopt,setChkOpt] = useState([]);
    const [isCheck, setCheck] = useState(false);
    const [schedule, setSchedule] = useState({
        id:"",
        scheduleName: "",
        schoolId: "",
        grades : [],
        status : 1,
        checkins :[]
      });
    const [schedulechk, setSchedulechk] = useState({
        checkinName: "",
        type: 1,
        lessonId : "",
        surveyId : ""
    });
    const [errors, setError] = useState({
        errors: "",
      });
        

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    useEffect(() => {
        async function fetchData()
        {

            const rest = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getScheduleById?id="+checkin_id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(schedule => {
                    
                // console.log(res.gradeList)
                setSchedule(schedule.output)
                setgradeId(schedule.output.grades);
                if(schedule.output.status == '1')
                {
                    setCheck((prev) => !prev) 
                    console.log(isCheck)
                }
                setMsopt((schedule.output.checkins).length+1)
            });


            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSelectFullGrade", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(gradeList => {
                    
                // console.log(res.gradeList)
                SetGradeData(gradeList)
            });


            const res1 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSchoolDropList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(scList => {
                    
                // console.log(res.gradeList)
                setSchoolData(scList.output)
            });

            const res2 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSurveyDropList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(scList => {
                    
                // console.log(res.gradeList)
                setSurveyData(scList.output)
            });

            const res3 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getLessonDropList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(scList => {
                    
                // console.log(res.gradeList)
                setLessonData(scList.output)
                //     scList.map()
                // setOptions
            });

        }
        fetchData();    
    },[]);

    const customStyles = {
        control: base => ({
          ...base,
          height: 33,
          minHeight: 33,
          borderRadius: 5,
        })
      };

     
   


  const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">View Check-in</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                    <form>
                    <div className="row">
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label htmlFor="defaultFormControlInput" className="form-label">Schedule Name</label>
                                 <input type="text" className="form-control" id="defaultFormControlInput" name="scheduleName" value={schedule.scheduleName} placeholder="Lorem Ipsum" aria-describedby="defaultFormControlHelp"/>
                                 
                             </div>
                         </div>
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label htmlFor="defaultSelect" className="form-label">School Name</label>
                                 <select id="defaultSelect" className="form-select" name="schoolId" value={schedule.schoolId}>
                                     <option value="">Select School</option>
                                     {schoolData.map((res,key) => {
                                        return (
                                            <option value={res.id}>{res.fullName}</option>
                                        )
                                    })}
                                     
                                 </select>
                                 
                             </div>
                         </div>
                     </div>
                     <div className="row">
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label className="form-label" htmlFor="basic-icon-default-email">Grades</label>
                                 <Select 
                                    options={gradeData}
                                    placeholder="Select Grade"
                                    value={gradeId}
                                    isSearchable={true}
                                    isMulti
                                    styles={customStyles}
                                    isDisabled={true}
                                />
                                <span className="error" style={{ color: "red" }}>{errors['scheduleGrades']}</span>
                             </div>
                         </div>
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label className="form-label d-block">Status</label>
                                 <div className="form-check form-check-inline mt-2">
                                     <input className="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" checked={isCheck === true } disabled/>
                                     <label className="form-check-label" htmlFor="inlineRadio1">Active</label>
                                 </div>
                                 <div className="form-check form-check-inline">
                                     <input className="form-check-input" type="radio" name="status" id="inlineRadio2" value="0" checked={isCheck === false } disabled/>
                                     <label className="form-check-label" htmlFor="inlineRadio2">Inactive</label>
                                 </div>
                             </div>
                         </div>
                     </div>
                     {
                     (schedule.checkins.map((res,key,row) => {
                        let keys = key+1
                        // console.log('ption ='+options)
                        return (
                        <div className='checkin-type' id={key}>
                            <div className='row'>
                            
                                <div className='col-md-9'>
                                        <div className="mb-3">
                                        <label className="form-label d-block">Select Check-in Type</label>
                                        <div className="form-check form-check-inline mt-2">
                                            <input className={"form-check-input checks"+keys} type="radio" name={"type"+keys} id="inlineRadio1" value="1" checked={res.type == 1} disabled/>
                                            <label className="form-check-label" htmlFor="inlineRadio1">Check-in</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className={"form-check-input checks"+keys} type="radio" name={"type"+keys} id="inlineRadio2" value="2" checked={res.type == 2} disabled/>
                                            <label className="form-check-label" htmlFor="inlineRadio2">Lesson</label>
                                        </div>
                                    </div>
                                </div>
                                {(key < 1) ?
                                <div className="col-md-3 text-end">
                                
                                    <div className="mb-3">
                                        {/* <div className="demo-inline-spacing">
                                        
                                            <button type="button" className="btn btn-sm btn-icon btn-primary" onClick={addOption}>
                                                <span className="tf-icons bx bx-plus"></span>
                                            </button>
                                        </div> */}
                                        
                                    </div>
                                </div>
                                :
                                <div className="col-md-3 text-end">
                                    <div className="mb-3">
                                        <div className="demo-inline-spacing">
                                            {/* <button type="button" className="btn btn-sm btn-icon btn-primary" onClick={(e) => removeEditBox(e,key)}>
                                                <span className="tf-icons bx bx-minus"></span>
                                            </button> */}
                                        </div>
                                    </div>
                                </div>
                                }
                                {/* <span className="error" style={{ color: "red" }}>{errors['checkinName']}</span> */}
                            </div>
                            {(res.type == 2) ? 
                            <div className="row d-flex align-items-center">
                                
                                <div className="col-md-5">
                                    <div className="mb-3">
                                        <label htmlFor="defaultFormControlInput" className="form-label">Lesson Name</label>
                                        {(lessonData.length > 0) ?
                                        <select id="defaultSelect" className={"form-select checks"+keys} name="lessonId" value={res.lessonId} >
                                            <option value="">Select Lesson</option>
                                            {lessonData.map((resl,key) => {
                                                return(
                                                    <option value={resl.id}>{resl.lessonName}</option>
                                                )
                                            })}
                                        </select>
                                        :
                                        <></>
                                        }
                                        
                                    </div>
                                </div>
                                <div className="col-md-5">
                                    <div className="mb-3">
                                        <label htmlFor="defaultSelect" className="form-label">Survey</label>
                                        {(surveyData.length > 0) ?
                                        <select id="defaultSelect" className={"form-select checks"+keys} name="surveyId" value={res.surveyId}>
                                            <option value="">Select Survey</option>
                                            {surveyData.map((ress,key) => {
                                                return(
                                                    <option value={ress.id}>{ress.surveyName}</option>
                                                )
                                            })}
                                        </select>
                                        :
                                        <></>
                                        }
                                    </div>
                                </div>
                                
                            </div>
                            :
                            <div className="row d-flex align-items-center">
                                
                                <div className="col-md-5">
                                    <div className="mb-3">
                                        <label htmlFor="defaultFormControlInput" className="form-label">Check-in Name</label>
                                        <input type="text" className={"form-control checks"+keys} id="defaultFormControlInput" value={(res.lessonId == 0) ?res.checkinName:''} name="checkinName" placeholder="Lorem Ipsum" aria-describedby="defaultFormControlHelp" />
                                        
                                    </div>
                                </div>
                                <div className="col-md-5">
                                    <div className="mb-3">
                                        <label htmlFor="defaultSelect" className="form-label">Survey</label>
                                        {(surveyData.length > 0) ?
                                        <select id="defaultSelect" className={"form-select checks"+keys} name="surveyId" value={res.surveyId}>
                                            <option value="">Select Survey</option>
                                            {surveyData.map((ress,key) => {
                                                return(
                                                    <option value={ress.id}>{ress.surveyName}</option>
                                                )
                                            })}
                                        </select>
                                        :
                                        <></>
                                        }
                                        
                                    </div>
                                </div>
                                
                            </div>

                            }
                        </div>
                        )
                     }))}
                     
                     {chkopt.map((result,index) => {
                        return (
                            result)
                        }
                      )}
                 </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
}

export default ViewModal