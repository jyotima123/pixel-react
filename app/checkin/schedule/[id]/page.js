"use client"
import { useEffect, useState } from "react";
import { BiPlusMedical } from "react-icons/bi";
import { usePathname } from 'next/navigation'
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { alertService } from 'services';
import Alert  from '@components/Alert';
import Modal from "../modal";
export const dynamic = 'force-dynamic'
import { useSession } from "next-auth/react";


const page = () => {
    const { data: session } = useSession()
    const pathname = usePathname()
    const[checkinData,SetCheckinData] = useState([]);
    const[classData,SetClassData] = useState([]);
    const[classes,setClasses] = useState(0);
    const [showModal, setShowModal] = useState(false);
    const[checkinId,setcheckinId] = useState(false);
    const[surveyId,setsurveyId] = useState(false);
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });

    const sid = pathname.split('/')

    useEffect(() => {
    
        async function fetchData()
            {
    
                await import('datatables.net-dt/js/dataTables.dataTables')
                await import('datatables.net-dt/css/jquery.dataTables.min.css')
                window.JSZip = jzip;
                const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getScheduleCheckinsById?id="+sid[3], {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    cache: 'no-store'
                    }).then(res => res.json())
                    .then(list => {
                            // console.log(schoolList)
                            SetCheckinData(list.output)
                            
                    });
                
            }
            if(session)
            {
                fetchData();
            }
        
      }, [session]);



    const changeStatus = async (event, scheduleCheckinId,surveyId) => {
        

        var data = {
            schoolId: session?.user.id,
            scheduleCheckinId: scheduleCheckinId,
            surveyId: surveyId,
            scheduleId: sid[3],
            status : event.target.checked
        }
        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateScheduleCheckinsLessonStatusBySchool", {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                
                
                if(res.success)
                {
                    location.reload()
                    alertService.success('Status Updated Successfully', options)
                }
                else{
                    alertService.error('Something went wrong', options)
                }
            });
      }

      const openShowModal = async (e,checkinid,surveyid,send) => {

        if(surveyid > 0 && send == 0)
        {
            setShowModal(true)
            setcheckinId(checkinid)
            setsurveyId(surveyid)
        }

      }

      const childCall = (res,msg) =>{

        setShowModal(false)
        if(res == 'success')
        {  
            location.reload() 
            alertService.success(msg, options)
        }
        if(res == 'error')
        alertService.error(msg, options)
    
      } 

      const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }


  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
        <Sidebar menu="checkin" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Schedule Check-ins</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>
    

                    <div className="container-xxl flex-grow-1 container-p-y">

                    <Alert/>
                        <div className="page-content page-container" id="page-content">
                            <div className="card">
                                <div className="card-header">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-end">
                                        <a href="/schoolCheckin" style={{marginRight:'20px'}}> <span className="tf-icons bx bx bx-detail"></span>&nbsp;Back To List</a>
                                        </div>
                                       
                                    </div>
                                </div>
                                <div className="container-fluid">
                                    <div className="row" id="sortable">
                                    { (checkinData.length > 0) ?
                                        checkinData.map((result,key) => {

                                            return(
                                                <div className="col-md-12">
                                                    <div className="" style={{cursor: (result.checkinSend == 0 && result.surveyId > 0) ? 'pointer':'',background:(result.surveyId > 0) ? (result.checkinSend == 0) ?'#D9CBF6FF' : '#DEE1E6FF':'#F5F2FDFF'}}>
                                                        <div className="Rg-card-header mb-3">
                                                            <div className="row d-flex align-items-center justify-content-between">
                                                                <div className="col-lg-6 col-md-3 col-sm-3 col-xs-6 mb-Rg-3" onClick={event => openShowModal(event, result.checkinId,result.surveyId,result.checkinSend)}>
                                                                    {(result.type == 2) ?
                                                                    <a href={"/schoolLesson/"+result.lessonId} style={{cursor:"pointer"}} target="_blank">
                                                                    <h3 className="sub-heading m-0 p-0">{result.checkinName}</h3>
                                                                    </a>
                                                                    :
                                                                    <h3 className="sub-heading m-0 p-0">{result.checkinName}</h3>
                                                                    }
                                                                </div>
                                                                <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6 mb-Rg-3" onClick={event => openShowModal(event, result.checkinId,result.surveyId,result.checkinSend)}>
                                                                    {(result.type == 2) ?
                                                                    <label>Lesson Timing : {result.lessonTiming} minutes</label>
                                                                    :
                                                                    <></>
                                                                     }
                                                                </div>
                                                                <div className="col-lg-3 col-md-6 col-sm-6 col-xs-12 mb-Rg-3 text-end">
                                                                {(result.type == 2 && session?.user.role == 2) ?
                                                                    <div className="form-check form-check-inline">
                                                                        <input className="form-check-input" type="checkbox" id="inlineCheckbox1" checked={result.lessonStatus} value="1" onChange={event => changeStatus(event, result.checkinId,result.surveyId )}/>
                                                                        <label className="form-check-label" for="inlineCheckbox1">Lesson Completed</label>
                                                                    </div>
                                                                    :
                                                                    <></>
                                                                }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                        :
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="Rg-card-header bg-themePx mb-3 text-center">
                                                <h3 className="sub-heading m-0 p-0">No Data Available</h3>
                                            </div>
                                        </div>
                                    </div>
                            }
                                        
                                       
                                        
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div>
                        {showModal && <Modal checkinid={checkinId} scheduleId={sid[3]} surveyId={surveyId} onClose={childCall}/>}
         
                    </div>
                </div>
            </div>
        </div>
        {/*Below we add the modal wrapper*/}
        <div id="modal-root"></div>
    </div>
  )
}

export default page