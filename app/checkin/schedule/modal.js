import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useSession } from "next-auth/react";

const Modal = ({ onClose, children, title,checkinid,surveyId,scheduleId }) => {
    const { data: session } = useSession()
    const [isLoading, setLoading] = useState(false);
    const[classData,SetClassData] = useState([]);
    const[classes,setClasses] = useState(0);
    const [errors, setError] = useState({
        errors: "",
      });

      const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res,'');
    };

    useEffect(() => {
    
        async function fetchData()
        {

            var data = {
                scheduleId: scheduleId,
                schoolId: session?.user.id,
                checkinId: checkinid
            }

            const result = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getClassListBySchoolIdAndScheduleId?id="+session?.user.id+"&sch="+scheduleId, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store',
                body: JSON.stringify(data),
                }).then(res => res.json())
                .then(list => {
                    
                    SetClassData(list.output)
            
            });

            
        }
            if(session)
            {
                fetchData();
            }
        
      }, [session]);
    

      const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        
        if(classes == 0){
        formIsValid = false;
        errors["className"] = "please select any class";
        }
        
        setError(errors);
        return formIsValid;
    }


    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        
        if(handleValidation()){

            let data = {
                schoolId : session?.user.id,
                scheduleId: scheduleId,
                checkinId: checkinid,
                surveyId: surveyId,
                classId: classes
            }
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/scheduleCheckinsSurveySendBySchool", {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success",res.message);
                }
                else{
                    onClose("error",res.message);
                }
            });
        }
        else{
            setLoading(false);
          }
  
          
      };


  const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Are You Sure</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                        <form>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className='mb-3'>
                                        <label for="defaultSelect" className="form-label">Select Class to send the Survey</label>
                                        <select id="defaultSelect" className="form-select" value={classes} onChange={e => setClasses(e.target.value)}>
                                            <option value="0">Select Class</option>
                                            {(classData.length>0)?classData.map((result,key) => {
                                            return (
                                                <option key={key} value={result.id}>{result.className}</option>
                                            )}):
                                            <></>}
                                        </select>
                                        <span className="error" style={{ color: "red" }}>{errors['className']}</span>
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <div className='mb-3'>
                                        <p className="alert alert-success" role="alert">Do you really want to send this survey!</p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
            {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
}

export default Modal