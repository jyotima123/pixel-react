import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useFormik } from 'formik';
import Select from "react-select";
import { useSession } from "next-auth/react";
import AddOption from './addOption';

const EditModal = ({ onClose, children, title ,checkin_id}) => {
    const [isLoading, setLoading] = useState(false);
    const[gradeData,SetGradeData] = useState([]);
    const[classData,SetClassData] = useState([]);
    const[schoolData,setSchoolData] = useState([]);
    const[surveyData,setSurveyData] = useState([]);
    const[lessonData,setLessonData] = useState([]);
    const[gradeId,setgradeId] = useState();
    const[classId,setclassId] = useState();
    const[opt,setOpt] = useState(false);
    const[options,setOptions] = useState(null)
    const[msopt,setMsopt] = useState(2);
    const[chkopt,setChkOpt] = useState([]);
    const [isCheck, setCheck] = useState(false);
    const [schedule, setSchedule] = useState({
        id:"",
        scheduleName: "",
        schoolId: "",
        grades : [],
        classes : [],
        status : 1,
        checkins :[]
      });
    const [schedulechk, setSchedulechk] = useState({
        checkinName: "",
        type: 1,
        lessonId : "",
        surveyId : ""
    });
    const [errors, setError] = useState({
        errors: "",
      });
        

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    useEffect(() => {
        async function fetchData()
        {

            const rest = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getScheduleById?id="+checkin_id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(schedule => {
                    
                // console.log(res.gradeList)
                setSchedule(schedule.output)
                setgradeId(schedule.output.grades);
                SetGradeData(schedule.output.fullGrades)
                setclassId(schedule.output.classes)
                SetClassData(schedule.output.fullClasses)
                if(schedule.output.status == '1')
                {
                    setCheck((prev) => !prev) 
                    console.log(isCheck)
                }
                setMsopt((schedule.output.checkins).length+1)
            });


            const res1 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSchoolDropList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(scList => {
                    
                // console.log(res.gradeList)
                setSchoolData(scList.output)
            });

            const res2 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSurveyDropList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(scList => {
                    
                // console.log(res.gradeList)
                setSurveyData(scList.output)
            });

            const res3 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getLessonDropList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(scList => {
                    
                // console.log(res.gradeList)
                setLessonData(scList.output)
                //     scList.map()
                // setOptions
            });

        }
        fetchData();    
    },[]);

    const customStyles = {
        control: base => ({
          ...base,
          height: 33,
          minHeight: 33,
          borderRadius: 5,
        })
      };

      const changegrade = async e => {
        console.log(e)
        setSchedule({ ...schedule, 'grades': e});
        setgradeId(e);

        let data = {
            schoolId : schedule.schoolId,
            grades : e
        }

        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getClassesDropListBySchoolIdAndGradeId", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store',
            body: JSON.stringify(data)
            }).then(res => res.json())
            .then(res => {
               
                SetClassData(res.output)
            
            });
    };

    const changeclass = async e => {
        // console.log(e)
        setSchedule({ ...schedule, 'classes': e});
        setclassId(e);

    };

    const removeBox = (rand)=>{
        
        const list = document.getElementById(rand);        
        if(document.getElementById(rand))        
        {            
            document.getElementById(rand).removeChild(list.firstElementChild);  
            document.getElementById(rand).removeChild(list.lastElementChild);      
        }
        
    }

    const removeEditBox = (e,rand) => {
        const list = document.getElementById(rand);        
        if(document.getElementById(rand))        
        {            
            document.getElementById(rand).removeChild(list.firstElementChild);  
            document.getElementById(rand).removeChild(list.lastElementChild);      
        }
    }

    const addOption = () => {

        setMsopt((msopt) => (msopt+1))
        const min = 1;
        const max = 100;
        const rand = min + Math.random() * (max - min);

        setChkOpt([ ...chkopt, <AddOption rand={rand} chopt={msopt} survey={surveyData} lesson={lessonData} onClick={removeBox}/> ]);      

    }
    const onInputChange = async e => {

        setSchedule({ ...schedule, [e.target.name]: e.target.value });

        if(e.target.name == 'schoolId')
        {
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSelectFullGradeBySchoolId?id="+e.target.value, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(gradeList => {
                    
                // console.log(res.gradeList)
                SetGradeData(gradeList.output)
            });
        }
    };
    const onchInputChange = e => {

        
        if(schedulechk.type == '2' && e.target.name == 'lessonId')
        {
            
            setSchedulechk({ ...schedulechk, 'checkinName': e.target[e.target.selectedIndex].text, [e.target.name]: e.target.value });
        }
        else{
            setSchedulechk({ ...schedulechk, [e.target.name]: e.target.value });
        }

        
    };

    const onCheckinChange = (index) => {

        setOptions((prev) => {
            return prev === index ? null : index;
        });
        let chk = schedule.checkins[index]
        if(chk.type == 1)
        chk.type = 2
        else
        chk.type = 1

        let optvar = schedule
        optvar.checkins[index] = chk
        setSchedule(optvar)
        
        // console.log('clicked', index);
        
    }

    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        
        if(!schedule["scheduleName"]){
        formIsValid = false;
        errors["scheduleName"] = "Schedule name Cannot be empty";
        }
        //grades
        if(schedule["grades"].length == 0){
            formIsValid = false;
            errors["scheduleGrades"] = "Please select atleast 1 grade";
        }
        if(schedule["classes"].length == 0){
            formIsValid = false;
            errors["scheduleClasses"] = "Please select atleast 1 class";
        }
        if(!schedule["schoolId"]){
            formIsValid = false;
            errors["schoolId"] = "Please select school";
        }
        
        for(var i = 1;i<msopt;i++)
        {
            var els = document.getElementsByClassName("checks"+i);
            Array.prototype.forEach.call(els, function(el) {
                
                
                if(el.name=='lessonId' && !el.value)
                {
                    console.log('less')
                    formIsValid = false;
                    errors["checkinName"] = "Please select lesson";
                    
                }
                
                
            });
            Array.prototype.forEach.call(els, function(el) {
                
                
                
                if(el.name == 'checkinName' && !el.value)
                {
                    console.log('chk')
                    formIsValid = false;
                    errors["checkinName"] = "Please enter Check-in name";
                   
                    
                }
                
                
            });
            // Array.prototype.forEach.call(els, function(el) {
                
                
            //     if(el.name == 'surveyId' && !el.value)
            //     {
            //         console.log('surv')
            //         formIsValid = false;
            //         errors["checkinName"] = "Please select survey";
                    
                                        
            //     }
                
                
            // });
            
        }
    
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        var checkinArray = []
        for(var i = 1;i<msopt;i++)
        {
            var els = document.getElementsByClassName("checks"+i);
                var checkin = {}
            Array.prototype.forEach.call(els, function(el) {
                
                if(el.name=='type'+i)
                {
                    var typo =  document.querySelector('input[name="type'+i+'"]:checked').value
                    checkin["type"] = typo
                
                }
                if(el.name=='lessonId')
                {
                    var chkname = el[el.selectedIndex].text
                    var lesid = el.value
                    console.log(el[el.selectedIndex].text)
                    checkin["checkinName"] = chkname
                    checkin["lessoonId"] = lesid
                    
                }
                if(el.name == 'checkinName')
                {
                    var chkname = el.value
                    checkin["checkinName"] = chkname
                    
                }
                if(el.name == 'checkinid')
                {
                    var chkid = el.value
                    checkin["id"] = chkid
                    
                }
                if(el.name == 'surveyId')
                {
                    var survid = el.value
                    checkin["surveyId"]= survid
                    
                }
                
                
            });

            if(Object.keys(checkin).length>0)
            checkinArray.push(checkin)
            
        }

        // console.log(checkinArray)
     
        let types = {
            "id":schedule.id,
            "scheduleName": schedule.scheduleName,
            "schoolId": schedule.schoolId,
            "scheduleGrades" :schedule.grades,
            "scheduleClasses" :schedule.classes,
            "status" : schedule.status,
            "checkins" : checkinArray
          }
          console.log(types)
        
        setLoading(true);
        if(handleValidation()){
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateSchedule", {
            method: "PUT",
            body: JSON.stringify(types),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success");
                }
                else{
                    onClose("error");
                }
            });
  
          }
          else{
            setLoading(false);
          }
        
      };


  const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Add Check-in</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                    <form>
                    <div className="row">
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label htmlFor="defaultFormControlInput" className="form-label">Schedule Name</label>
                                 <input type="text" className="form-control" id="defaultFormControlInput" name="scheduleName" value={schedule.scheduleName} placeholder="Lorem Ipsum" aria-describedby="defaultFormControlHelp" onChange={e => onInputChange(e)}/>
                                 <span className="error" style={{ color: "red" }}>{errors['scheduleName']}</span>
                             </div>
                         </div>
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label htmlFor="defaultSelect" className="form-label">School Name</label>
                                 <select id="defaultSelect" className="form-select" name="schoolId" value={schedule.schoolId} onChange={e => onInputChange(e)}>
                                     <option value="">Select School</option>
                                     {schoolData.map((res,key) => {
                                        return (
                                            <option value={res.id}>{res.fullName}</option>
                                        )
                                    })}
                                     
                                 </select>
                                 <span className="error" style={{ color: "red" }}>{errors['schoolId']}</span>
                             </div>
                         </div>
                     </div>
                     <div className="row">
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label className="form-label" htmlFor="basic-icon-default-email">Grades</label>
                                 <Select 
                                    options={gradeData}
                                    placeholder="Select Grade"
                                    value={gradeId}
                                    onChange={changegrade}
                                    isSearchable={true}
                                    isMulti
                                    className="basic-multi-select"
                                    classNamePrefix="select"
                                />
                                <span className="error" style={{ color: "red" }}>{errors['scheduleGrades']}</span>
                             </div>
                         </div>
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label className="form-label" htmlFor="basic-icon-default-email">Classes</label>
                                 <Select 
                                    options={classData}
                                    placeholder="Select Class"
                                    value={classId}
                                    onChange={changeclass}
                                    isSearchable={true}
                                    isMulti
                                    className="basic-multi-select"
                                    classNamePrefix="select"
                                />
                                <span className="error" style={{ color: "red" }}>{errors['scheduleClasses']}</span>
                             </div>
                         </div>
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label className="form-label d-block">Status</label>
                                 <div className="form-check form-check-inline mt-2">
                                     <input className="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" checked={isCheck === true } onChange={e => onInputChange(e)}/>
                                     <label className="form-check-label" htmlFor="inlineRadio1">Active</label>
                                 </div>
                                 <div className="form-check form-check-inline">
                                     <input className="form-check-input" type="radio" name="status" id="inlineRadio2" value="0" checked={isCheck === false } onChange={e => onInputChange(e)}/>
                                     <label className="form-check-label" htmlFor="inlineRadio2">Inactive</label>
                                 </div>
                             </div>
                         </div>
                     </div>
                     {
                     (schedule.checkins.map((res,key,row) => {
                        let keys = key+1
                        // console.log('ption ='+options)
                        return (
                        <div className='checkin-type' id={key}>
                            <div className='row'>
                            <input type="hidden" className={'checks'+keys} name="checkinid" value={res.id}/>
                                <div className='col-md-9'>
                                        <div className="mb-3">
                                        <label className="form-label d-block">Select Check-in Type</label>
                                        <div className="form-check form-check-inline mt-2">
                                            <input className={"form-check-input checks"+keys} type="radio" name={"type"+keys} id="inlineRadio1" value="1" defaultChecked={res.type == 1} onChange={e => onCheckinChange(key)}/>
                                            <label className="form-check-label" htmlFor="inlineRadio1">Check-in</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className={"form-check-input checks"+keys} type="radio" name={"type"+keys} id="inlineRadio2" value="2" defaultChecked={res.type == 2} onChange={e => onCheckinChange(key)}/>
                                            <label className="form-check-label" htmlFor="inlineRadio2">Lesson</label>
                                        </div>
                                    </div>
                                </div>
                                {(key < 1) ?
                                <div className="col-md-3 text-end">
                                
                                    <div className="mb-3">
                                        <div className="demo-inline-spacing">
                                        
                                            <button type="button" className="btn btn-sm btn-icon btn-primary" onClick={addOption}>
                                                <span className="tf-icons bx bx-plus"></span>
                                            </button>
                                        </div>
                                        
                                    </div>
                                </div>
                                :
                                <div className="col-md-3 text-end">
                                    <div className="mb-3">
                                        <div className="demo-inline-spacing">
                                            <button type="button" className="btn btn-sm btn-icon btn-primary" onClick={(e) => removeEditBox(e,key)}>
                                                <span className="tf-icons bx bx-minus"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                }
                                <span className="error" style={{ color: "red" }}>{errors['checkinName']}</span>
                            </div>
                            {(res.type == 2) ? 
                            <div className="row d-flex align-items-center">
                                
                                <div className="col-md-5">
                                    <div className="mb-3">
                                        <label htmlFor="defaultFormControlInput" className="form-label">Lesson Name</label>
                                        {(lessonData.length > 0) ?
                                        <select id="defaultSelect" className={"form-select checks"+keys} name="lessonId" defaultValue={res.lessonId} onChange={e => onchInputChange(e)}>
                                            <option value="">Select Lesson</option>
                                            {lessonData.map((resl,key) => {
                                                return(
                                                    <option value={resl.id}>{resl.lessonName}</option>
                                                )
                                            })}
                                        </select>
                                        :
                                        <></>
                                        }
                                        
                                    </div>
                                </div>
                                <div className="col-md-5">
                                    <div className="mb-3">
                                        <label htmlFor="defaultSelect" className="form-label">Survey</label>
                                        {(surveyData.length > 0) ?
                                        <select id="defaultSelect" className={"form-select checks"+keys} name="surveyId" defaultValue={res.surveyId} onChange={e => onchInputChange(e)}>
                                            <option value="">Select Survey</option>
                                            {surveyData.map((ress,key) => {
                                                return(
                                                    <option value={ress.id}>{ress.surveyName}</option>
                                                )
                                            })}
                                        </select>
                                        :
                                        <></>
                                        }
                                    </div>
                                </div>
                                
                            </div>
                            :
                            <div className="row d-flex align-items-center">
                                
                                <div className="col-md-5">
                                    <div className="mb-3">
                                        <label htmlFor="defaultFormControlInput" className="form-label">Check-in Name</label>
                                        <input type="text" className={"form-control checks"+keys} id="defaultFormControlInput" defaultValue={(res.lessonId == 0) ?res.checkinName:''} name="checkinName" placeholder="Lorem Ipsum" aria-describedby="defaultFormControlHelp" onChange={e => onchInputChange(e)}/>
                                        
                                    </div>
                                </div>
                                <div className="col-md-5">
                                    <div className="mb-3">
                                        <label htmlFor="defaultSelect" className="form-label">Survey</label>
                                        {(surveyData.length > 0) ?
                                        <select id="defaultSelect" className={"form-select checks"+keys} name="surveyId" defaultValue={res.surveyId} onChange={e => onchInputChange(e)}>
                                            <option value="">Select Survey</option>
                                            {surveyData.map((ress,key) => {
                                                return(
                                                    <option value={ress.id}>{ress.surveyName}</option>
                                                )
                                            })}
                                        </select>
                                        :
                                        <></>
                                        }
                                        
                                    </div>
                                </div>
                                
                            </div>

                            }
                        </div>
                        )
                     }))}
                     
                     {chkopt.map((result,index) => {
                        return (
                            result)
                        }
                      )}
                 </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
                        {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
}

export default EditModal