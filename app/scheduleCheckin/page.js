"use client"
import { useEffect, useState } from "react";
import { BiPlusMedical } from "react-icons/bi";
import { useRouter } from 'next/navigation'
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { alertService } from 'services';
import Alert  from '@components/Alert';
import Modal from "./modal";
export const dynamic = 'force-dynamic'
import { useSession } from "next-auth/react";


const page = () => {
    const { data: session } = useSession()
    const[checkinData,SetCheckinData] = useState([]);
    const[classData,SetClassData] = useState([]);
    const[classes,setClasses] = useState(0);
    const [showModal, setShowModal] = useState(false);
    const[checkinId,setcheckinId] = useState(false);
    const[surveyId,setsurveyId] = useState(false);
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });

    useEffect(() => {
    
        async function fetchData()
            {
    
                await import('datatables.net-dt/js/dataTables.dataTables')
                await import('datatables.net-dt/css/jquery.dataTables.min.css')
                window.JSZip = jzip;
                const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getScheduleCheckinsByTeacherId?id="+session?.user.id, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    cache: 'no-store'
                    }).then(res => res.json())
                    .then(list => {
                            // console.log(schoolList)
                            SetCheckinData(list.output)
                            
                    });

                    const result = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getClassListByTeacherId?id="+session?.user.id, {
                        method: "GET",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        cache: 'no-store'
                        }).then(res => res.json())
                        .then(list => {
                            
                            SetClassData(list.output)
                            if(list.output.length == 1)
                            {
                                setClasses(list.output[0].id)
                            }
                   
                    });
    
                
            }
            if(session && classes == 0)
            {
                fetchData();
            }
        
      }, [session]);


      const getFilter = async(e,calltype) => {

        var url = process.env.NEXT_PUBLIC_APP_URL+'/getScheduleCheckinsByTeacherIdAndClassId'

        if(calltype == 'select')
        {
            var classId = e.target.value
            setClasses(e.target.value)
        }
        else
        {
            var classId = classes
        }

        let data = {
            teacherId : session?.user.id,
            classId : classId
        }

        const res = await fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store',
                body: JSON.stringify(data)
                }).then(res => res.json())
                .then(res => {
                    if(res.success)
                    {
                        SetCheckinData([])
                        SetCheckinData(res.output)
                    }
                });
       
    }

    const changeStatus = async (event, scheduleCheckinId,surveyId) => {
        

        var data = {
            teacherId: session?.user.id,
            scheduleCheckinId: scheduleCheckinId,
            classId: classes,
            surveyId: surveyId,
            status : event.target.checked
        }
        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateScheduleCheckinsLessonStatus", {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                
                
                if(res.success)
                {
                    getFilter('','child')
                    alertService.success('Status Updated Successfully', options)
                }
                else{
                    alertService.error('Something went wrong', options)
                }
            });
      }

      const openShowModal = async (e,checkinid,surveyid,send) => {

        if(surveyid > 0 && send == 0)
        {
            setShowModal(true)
            setcheckinId(checkinid)
            setsurveyId(surveyid)
        }

      }

      const childCall = (res,msg) =>{

        setShowModal(false)
        if(res == 'success')
        {   
            getFilter('','child')
            alertService.success(msg, options)
        }
        if(res == 'error')
        alertService.error(msg, options)
    
      } 

      const changeBackground = (e,type,send)=> {
        if(type == 1 && send == 0)
        {
            e.target.style.background = '#D9CBF6FF';
        }
        else if(type == 1 && send == 1){
            e.target.style.background = '#DEE1E6FF';
        }
        else {
            e.target.style.background = '#F5F2FDFF';
        }
        
      }

      const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="scheduleCheckin" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Schedule Check-ins</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>
    

                    <div className="container-xxl flex-grow-1 container-p-y">

                    <Alert/>
                        <div className="page-content page-container" id="page-content">
                            <div className="card">
                                <div className="card-header">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <div className="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                            <label for="defaultSelect" className="form-label">Filter By</label>
                                            <select id="defaultSelect" className="form-select"  value={classes} onChange={e => getFilter(e,'select')}>
                                                <option value="0">Select Class</option>
                                                {(classData.length>0)?classData.map((result,key) => {
                                                return (
                                                    <option key={key} value={result.id}>{result.className}</option>
                                                )}):
                                                <></>}
                                            </select>
                                        </div>
                                       
                                    </div>
                                </div>
                                <div className="container-fluid">
                                    <div className="row" id="sortable">
                                    { (checkinData.length > 0) ?
                                        checkinData.map((result,key) => {

                                            return(
                                                <div className="col-md-12">
                                                     <div className="" style={{cursor: (result.checkinSendStatus == 0 && result.surveyId > 0) ? 'pointer':'',background:(result.surveyId > 0) ? (result.checkinSendStatus == 0) ?'#D9CBF6FF' : '#DEE1E6FF':'#F5F2FDFF'}}>
                                                        <div className="Rg-card-header mb-3" >
                                                            <div className="row d-flex align-items-center justify-content-between">
                                                                <div className="col-lg-6 col-md-3 col-sm-3 col-xs-6 mb-Rg-3"  onClick={event => openShowModal(event, result.checkinId,result.surveyId,result.checkinSendStatus)}>
                                                                {(result.type == 2) ?
                                                                    <a href={"/teacherLesson/"+result.lessonId} style={{cursor:"pointer"}} target="_blank">
                                                                    <h3 className="sub-heading m-0 p-0">{result.checkinName}</h3>
                                                                    </a>
                                                                    :
                                                                    <h3 className="sub-heading m-0 p-0">{result.checkinName}</h3>
                                                                    }
                                                                </div>
                                                                <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6 mb-Rg-3"  onClick={event => openShowModal(event, result.checkinId,result.surveyId,result.checkinSendStatus)}>
                                                                    {(result.type == 2) ?
                                                                    <label>Lesson Timing : {result.lessonTiming} minutes</label>
                                                                    :
                                                                    <></>
                                                                     }
                                                                </div>
                                                                <div className="col-lg-3 col-md-6 col-sm-6 col-xs-12 mb-Rg-3 text-end">
                                                                {(result.type == 2) ?
                                                                    <div className="form-check form-check-inline">
                                                                        <input className="form-check-input" type="checkbox" id="inlineCheckbox1" checked={result.lessonStatus} value="1" onChange={event => changeStatus(event, result.checkinId,result.surveyId )}/>
                                                                        <label className="form-check-label" for="inlineCheckbox1">Lesson Completed</label>
                                                                    </div>
                                                                    :
                                                                    <></>
                                                                }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                        :
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="Rg-card-header bg-themePx mb-3 text-center">
                                                <h3 className="sub-heading m-0 p-0">No Data Available</h3>
                                            </div>
                                        </div>
                                    </div>
                            }
                                        
                                       
                                        
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div>
                        {showModal && <Modal classes={classes} checkinid={checkinId} surveyId={surveyId} onClose={childCall}/>}
         
                    </div>
                </div>
            </div>
        </div>
        {/*Below we add the modal wrapper*/}
        <div id="modal-root"></div>
    </div>
  )
}

export default page