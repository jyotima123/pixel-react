import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useSession } from "next-auth/react";

const Modal = ({ onClose, children, title,checkinid,surveyId,classes }) => {
    const { data: session } = useSession()
    const [isLoading, setLoading] = useState(false);
   
      const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res,'');
    };
    

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        
        let data = {
            teacherId : session?.user.id,
            checkinId: checkinid,
            surveyId: surveyId,
            classId: classes
        }
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/scheduleCheckinsSurveySend", {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success",res.message);
                }
                else{
                    onClose("error",res.message);
                }
            });
  
          
      };


  const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Are You Sure</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="alert alert-success" role="alert">Do you really want to send this survey!</p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
            {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
}

export default Modal