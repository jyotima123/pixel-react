import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useSession } from "next-auth/react";
import Select from "react-select";

const EditModal = ({ onClose, children, title,teacher_id }) => {
    const { data: session } = useSession()
    const [isLoading, setLoading] = useState(false);
    const [isCheck, setCheck] = useState(false);
    const[classData,SetClassData] = useState([]);
    const[classId,setclassId] = useState();
    const [teacher, setTeacher] = useState({
        id:"",
        fullName: "",
        email: "",  
        classes:[],
        status:1
      });
      const [errors, setError] = useState({
        errors: "",
      });

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    useEffect(() => {
        async function fetchData()
        {
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getClassesSelectListBySchoolId?id="+session?.user.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(res.list)
                SetClassData(list.output)
            });

            const res1 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getTeacherById?id="+teacher_id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store' 
                }).then(res => res.json())
                .then(res => {
                    
                
                setTeacher(res.output)
                setclassId(res.output.classes);
                if(res.output.status == '1')
                {
                    setCheck((prev) => !prev) 
                }
               
                
            });

        }
        fetchData();    
    },[]);

    
    const onInputChange = e => {
        setTeacher({ ...teacher, [e.target.name]: e.target.value });
    };

    const changeclass = e => {
       
        setTeacher({ ...teacher, 'classes': e});
        setclassId(e);
    };


    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        
        //username
        if(!teacher["fullName"]){
        formIsValid = false;
        errors["fullName"] = "Name Cannot be empty";
        }
        //contact person
        if(teacher["classes"].length == 0){
            formIsValid = false;
            errors["classes"] = "Please select atleast 1 class";
            }
       
        //Email
         if(!teacher["email"]){
            formIsValid = false;
            errors["email"] = "Email cannot be empty";
          }
          
        
    
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/teacher/update", {
            method: "PUT",
            body: JSON.stringify(teacher),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success",res.message);
                }
                else{
                    onClose("error",res.message);
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };

      const customStyles = {
        control: base => ({
          ...base,
          height: 33,
          minHeight: 33,
          borderRadius: 5,
        })
      };

    const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Add Teacher</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                    <form>
                     <div class="row">
                         <div class="col-md-6">
                             <div class="mb-3">
                                 <label for="defaultFormControlInput" class="form-label">Full Name</label>
                                 <input type="text" class="form-control" id="defaultFormControlInput" placeholder="Rajiv" aria-describedby="defaultFormControlHelp" value={teacher.fullName} name="fullName" onChange={e => onInputChange(e)}/>
                                <span className="error" style={{ color: "red" }}>{errors['fullName']}</span>
                             </div>
                         </div>
                         <div class="col-md-6">
                             <div class="mb-3">
                                 <label class="form-label" for="basic-icon-default-email">Email</label>
                                 <div class="input-group input-group-merge">
                                     <span class="input-group-text"><i class="bx bx-envelope"></i></span>
                                     <input type="text" id="basic-icon-default-email" class="form-control" placeholder="Rajiv.doe@example.com" aria-label="Rajiv.doe@example.com" aria-describedby="basic-icon-default-email2" value={teacher.email} name="email" onChange={e => onInputChange(e)}/>
                                 </div>
                                <span className="error" style={{ color: "red" }}>{errors['email']}</span>
                             </div>
                         </div>
                     </div>
                     <div class="row">
                         
                         <div class="col-md-6">
                             <div class="mb-3">
                                 <label class="form-label" for="basic-icon-default-email">Assign Class</label>
                                 <Select 
                                    options={classData}
                                    placeholder="Select Class"
                                    value={classId}
                                    onChange={changeclass}
                                    isSearchable={true}
                                    isMulti
                                    className="basic-multi-select"
                                    classNamePrefix="select"
                                />
                             </div>
                             <span className="error" style={{ color: "red" }}>{errors['classes']}</span>
                         </div>

                         <div class="col-md-6">
                             <div class="">
                                 <label class="form-label d-block">Status</label>
                                 <div class="form-check form-check-inline mt-2">
                                     <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" checked={isCheck === true }  onChange={e => onInputChange(e)}/>
                                     <label class="form-check-label" for="inlineRadio1">Active</label>
                                 </div>
                                 <div class="form-check form-check-inline">
                                     <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="0" checked={isCheck === false} onChange={e => onInputChange(e)}/>
                                     <label class="form-check-label" for="inlineRadio2">Inactive</label>
                                 </div>
                             </div>
                         </div>
                         
                     </div>
                     
                 </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
                        {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
}

export default EditModal