import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useSession } from "next-auth/react";

const ResetModal = ({ onClose, children, title,student_id }) => {
    const { data: session } = useSession()
    const [isLoading, setLoading] = useState(false);
    const [student, setStudent] = useState({
        studentId:student_id,
        password: ""
      });
      const [errors, setError] = useState({
        errors: "",
      });

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

   
    
    const onInputChange = e => {
        setStudent({ ...student, [e.target.name]: e.target.value });
    };

    
    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        
        //username
        if(!student["password"]){
        formIsValid = false;
        errors["password"] = "Please enter password";
        }
        
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/resetStudentPassword", {
            method: "PUT",
            body: JSON.stringify(student),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success");
                }
                else{
                    onClose("error");
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };

     

    const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Reset Student Password</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                    <form>
                     <div class="row">
                        <div class="col-md-12">
                             <div class="mb-3">
                                 <label for="defaultFormControlInput" class="form-label">Password</label>
                                 <input type="text" class="form-control" id="defaultFormControlInput" placeholder="34534" aria-describedby="defaultFormControlHelp" name="password" onChange={e => onInputChange(e)}/>
                                <span className="error" style={{ color: "red" }}>{errors['password']}</span>
                             </div>
                         </div>
                         
                     </div>
                    
                 </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
                        {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
}

export default ResetModal