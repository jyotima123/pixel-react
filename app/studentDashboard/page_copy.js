"use client";

import React, { useEffect,useState } from 'react';
import Sidebar from '@components/Sidebar'
import Header from '@components/Header'
import { useSession } from "next-auth/react";
import { BsFillHeartFill } from "react-icons/bs";

const page = () => {
    const { data: session } = useSession() 
    const[dashboard,SetDashboard] = useState({
        TotalCheckins: 0,
        CheckinsCompleted: 0,
        CheckinsPending: 0,
        OverallPercent: 0,
        WellnessPercent: 0,
        KnowledgePercent :0
    });


    useEffect(() => {
    
        async function fetchData()
        {
            
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getStudentDashboard?id="+session?.user.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                SetDashboard(list.output)
                
                   
            });

            
        }

        if(session?.user.role == 3)
        {
            fetchData();
        }
            
        
      }, [session]);
     

  return (
    <div className="layout-wrapper layout-content-navbar">
        <div className="layout-container">
            <Sidebar menu="dashboard"/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4">
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Student Dashboard</h4>
                            </div>
                        <Header/>
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">
                        <div className="card">
                            <div className="card-header">
                                <div className="col-12">
                                    <h3 className="sub-heading  m-0 p-0"><i className="bi bi-columns-gap pe-2"></i>Overview </h3>
                                </div>
                            </div>
                            <div className="card-body">

                                <div className="row">
                                    <div className="co1-12">
                                        <div className="row">
                                            
                                            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                                <a>
                                                    <div className="card RgCard bg-primaryPx">
                                                        <div className="card-body">
                                                            <div className="d-flex align-items-start justify-content-between">
                                                                <h6 className="Rg-subHeading d-block mb-1 clr-primary">Total Check-ins</h6>
                                                                <div className="avatar avatar-md flex-shrink-0">
                                                                    <img src="/assets/img/icons/pixelhealth/2.png" alt="Total Check-ins Completed" className="rounded"/>
                                                                </div>
                                                            </div>

                                                            <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.TotalCheckins}</h1>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                                <a>
                                                    <div className="card RgCard bg-themePx">
                                                        <div className="card-body">
                                                            <div className="d-flex align-items-start justify-content-between">
                                                                <h6 className="Rg-subHeading d-block mb-1 clr-theme">Check-ins Completed</h6>
                                                                <div className="avatar avatar-md flex-shrink-0">
                                                                    <img src="/assets/img/icons/pixelhealth/4.png" alt="Lessons Completed" className="rounded"/>
                                                                </div>
                                                            </div>

                                                            <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.CheckinsCompleted}</h1>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-4">
                                                <a href="/studentCheckins/pending">
                                                    <div className="card RgCard bg-themePx">
                                                        <div className="card-body">
                                                            <div className="d-flex align-items-start justify-content-between">
                                                                <h6 className="Rg-subHeading d-block mb-1 clr-theme">Check-ins Pending</h6>
                                                                <div className="avatar avatar-md flex-shrink-0">
                                                                    <img src="/assets/img/icons/pixelhealth/4.png" alt="Lessons Completed" className="rounded"/>
                                                                </div>
                                                            </div>

                                                            <h1 className="card-title Rg-Title hw-bold text-nowrap mb-2">{dashboard.CheckinsPending}</h1>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="co1-12">
                                        <div className="row">
                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-4">
                                                <div className="card bg-themePx">
                                                    <div className="card-body d-flex align-items-end justify-content-between">
                                                        <div>
                                                            <h4 className="fw-bold clr-theme m-0 p-0">Overall Score</h4>
                                                        </div>
                                                        <div className="d-flex align-items-center">
                                                            <span>
                                                                <h1 className="fw-bold m-0 p-0 me-3">{dashboard.OverallPercent}</h1>
                                                            </span>
                                                            <a href="/studentDetailedReports" className="h5 m-0 p-0 text-decoration-underline clr-theme">View my report </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="co1-12">
                                        <div className="row">
                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-4">
                                                <div className="card bg-themePx">
                                                    <div className="card-body d-flex align-items-end justify-content-between">
                                                        <div className="col-md-5">
                                                            <div>
                                                                <h4 className="fw-bold clr-theme m-0 p-0">Wellness</h4>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-1">
                                                            <div className="clr-theme">
                                                                <div className="avatar avatar-md flex-shrink-0">
                                                                    <BsFillHeartFill className='icon-lg'/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="d-flex align-items-center justify-content-end">
                                                                <span>
                                                                    <h1 className="fw-bold m-0 p-0 me-3">{dashboard.WellnessPercent}</h1>
                                                                </span>
                                                                <a href="/studentDetailedReports" className="h5 m-0 p-0 text-decoration-underline clr-theme">View my report </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="co1-12">
                                        <div className="row">
                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div className="card bg-themePx">
                                                    <div className="card-body d-flex align-items-end justify-content-between">
                                                        <div className="col-md-5">
                                                            <div>
                                                                <h4 className="fw-bold clr-theme m-0 p-0">Knowledge </h4>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-1">
                                                            <div className="clr-theme">
                                                                <div className="avatar avatar-md flex-shrink-0">
                                                                    <img src="/assets/img/icons/brain-icon.png" alt="Lessons Completed" className="rounded"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="d-flex align-items-center justify-content-end">
                                                                <span>
                                                                    <h1 className="fw-bold m-0 p-0 me-3">{dashboard.KnowledgePercent}</h1>
                                                                </span>
                                                                <a href="/studentDetailedReports" className="h5 m-0 p-0 text-decoration-underline clr-theme">View my report </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
  )
}

export default page