"use client"
import { useEffect, useState } from "react";
import { BiPlusMedical } from "react-icons/bi";
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import Modal from "./addModal";
import EditModal from "./editModal";
import { alertService } from 'services';
import Alert  from '@components/Alert';
import { useSession } from "next-auth/react";

const page = () => {
    const { data: session,update } = useSession()
    const [showModal, setShowModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const[resourceList,setResourceList] = useState([]);
    const[editId,setId] = useState(false);
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });

   
  useEffect(() => {
    
    async function fetchData()
        {
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getResources", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(resources => {
                        // console.log(schoolList)
                        setResourceList(resources.output)
                        
                });
        }
        fetchData();
    
  }, []);

    const childCall = (msg) =>{

        setShowModal(false)
        if(msg == 'success')
        {   
            location.reload();
            alertService.success('Resource Added Successfully', options)
        }
        if(msg == 'error')
        alertService.error('Something went wrong', options)
    
      } 

    const openEditModal = (event, param) => {
        setId(param);
        setEditModal(true);
    }
    const editchildCall = (msg) =>{

        setEditModal(false)
        
        if(msg == 'success')
        {   
            location.reload();
            alertService.success('Resource Updated Successfully', options)
        }
        if(msg == 'error')
        {
            location.reload();
            alertService.error('Something went wrong', options)
        }

    }

    const handleDelete = async(event, param) => {
        if (confirm("Do you really want to delete this data.")) {
    
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/resource/delete/"+param, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                if(res.success)
                {
                    location.reload()
                    alertService.success('Resource deleted successfully', options)
                }
                else{
                    alertService.error('Something went wrong', options)
                }
            });
        }
      }

      const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="resources" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Resources</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>
                    <div className="container-xxl flex-grow-1 container-p-y">
                        <Alert />
                        <div className="card">
                            <div className="card-header">
                                <div className="d-flex align-items-center justify-content-between">
                                    <div className="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        {/* <div className="input-group input-group-merge">
                                            <span className="input-group-text" id="basic-addon-search31"><i className="bx bx-search"></i></span>
                                            <input type="text" className="form-control" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon-search31"/>
                                        </div> */}
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-end">
                                    {(session?.user.role == 0) ?
                                        <a type="button" className="btn btn-primary" onClick={() => setShowModal(true)}>
                                            <BiPlusMedical />&nbsp; Add Resources
                                        </a>
                                        :
                                        <></>
                                    }
                                    </div>
                                </div>
                            </div>
                            <div className="container-fluid">
                            { (resourceList.length > 0) ?
                             
                                    resourceList.map((result,key, row) => {
                                            return (
                                    <div className="row" key={key}>
                                        <div className="col-md-12">
                                            <div className="Rg-card-header bg-themePx mb-3">
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <div className="col-lg-8 col-md-6 col-sm-6 col-xs-6 mb-Rg-3">
                                                        <h3 className="sub-heading m-0 p-0">{result.title}</h3>
                                                    </div>
                                                    <div className="col-lg-2 col-md-6 col-sm-6 col-xs-6 mb-Rg-3">
                                                        <a className="btn btn-outline-primary" href={result.url} target="blank">View</a>
                                                    </div>
                                                    {(session?.user.role == 0) ?
                                                    <div className="col-lg-2 col-md-6 col-sm-6 col-xs-6 mb-Rg-3">
                                                        <a onClick={event => openEditModal(event, result.id)} style={{cursor:"pointer"}}><i className="bx bx-edit-alt me-1"></i>
                                                                    </a>
                                                        <a onClick={event => handleDelete(event, result.id)} style={{cursor:"pointer"}}><i className="bx bx-trash me-1"></i>
                                                                    </a>
                                                    </div>
                                                    :
                                                    <></>
                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )})
                            :
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="Rg-card-header bg-themePx mb-3 text-center">
                                                <h3 className="sub-heading m-0 p-0">No Data Available</h3>
                                            </div>
                                        </div>
                                    </div>
                            }
                            </div>
                        </div>
                    </div>
                    <div>
                            { showModal && <Modal onClose={childCall}/>  }
                            { editModal && <EditModal res_id = {editId} onClose={editchildCall}/> }
                    </div>
                </div>
            </div>
        </div>
        <div id="modal-root"></div>
    </div>
  )
}

export default page