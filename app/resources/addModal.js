import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";

const Modal = ({ onClose, children, title }) => {
    const [isLoading, setLoading] = useState(false);
    const [resource, setResource] = useState({
        title: "",
        url:""
      });
      const [errors, setError] = useState({
        errors: "",
      });
      const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };
    const onInputChange = e => {
        setResource({ ...resource, [e.target.name]: e.target.value });
    };

    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        // console.log(school);
        //username
        if(!resource["title"]){
        formIsValid = false;
        errors["title"] = "Resource title Cannot be empty";
        }
        //contact person
        if(!resource["url"]){
            formIsValid = false;
            errors["url"] = "URL Cannot be empty";
            }         
    
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/resource/add", {
            method: "POST",
            body: JSON.stringify(resource),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success");
                }
                else{
                    onClose("error");
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };


  const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Add Resource</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label for="defaultFormControlInput" class="form-label">Title</label>
                                        <input type="text" class="form-control" id="defaultFormControlInput" placeholder="Add Title" aria-describedby="defaultFormControlHelp" name="title" onChange={e => onInputChange(e)}/>
                                        <span className="error" style={{ color: "red" }}>{errors['title']}</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label for="defaultFormControlInput" class="form-label">URL</label>
                                        <input type="text" class="form-control" id="defaultFormControlInput" placeholder="Paste Url" aria-describedby="defaultFormControlHelp" name="url" onChange={e => onInputChange(e)}/>
                                        <span className="error" style={{ color: "red" }}>{errors['url']}</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
            {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
}

export default Modal