import { NextRequest, NextResponse } from "next/server";

export async function GET(request) {

    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSurveyList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                });
    const surveyList = await res.json();

    let json_response = {
        status: "success",
        results: surveyList.output.length,
        surveyList,
      };
      return NextResponse.json(json_response);
}