import { NextRequest, NextResponse } from "next/server";

export async function GET(request) {

    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getDistrictList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                });
    const districtList = await res.json();

    let json_response = {
        status: "success",
        results: districtList.length,
        districtList,
      };
      return NextResponse.json(json_response);
}