import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';


const handler = NextAuth({
    providers : [
        CredentialsProvider({
            name : "credentials",
            async authorize(credentials, req){
               
                const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/user/login", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    email: credentials?.email,
                    password: credentials?.password,
                }),
                });
                const userData = await res.json();
                const user = userData.data;

                if (user) {
                    // Any object returned will be saved in `user` property of the JWT
                    return user
                  } else {
                    // If you return null then an error will be displayed advising the user to check their details.
                    return null
            
                    // You can also Reject this callback with an Error thus the user will be sent to the error page with the error message as a query parameter
                  }

            }
        })
    ],
    secret: process.env.NEXTAUTH_SECRET,
    session: {
        strategy: 'jwt',
        maxAge: 3 * 24 * 60 * 60
    },
    pages : {
        signIn : '/login'
    },
    callbacks: {
        jwt: ({ token, user,trigger,session }) => {
          // console.log("JWT Callback", { token, user });

          if(trigger === 'update'){
            return {...token, ...session.user}
          }
          if (user) {
            const u = user;
            return {
              ...token,
              id: u.id,
              role:u.role,
              type: (u.role == 0) ? 'admin' : 'other',
              loginId: u.id,
              loginrole: u.role,
              district: u.district,
              randomKey: u.randomKey,
            };
          }
          return token;
        },
        session: ({ session, token }) => {
            // console.log("Session Callback", { session, token });
            return {
              ...session,
              user: {
                ...session.user,
                id: token.id,
                role: token.role,
                type: token.type,
                loginId: token.loginId,
                loginrole: token.loginrole,
                district: token.district,
                randomKey: token.randomKey,
              },
            };
          }
       }
})

export { handler as GET, handler as POST }