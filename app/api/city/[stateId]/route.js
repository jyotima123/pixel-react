import { NextRequest, NextResponse } from "next/server";

export async function GET(request,{params}) {

    
    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getCityList?state_id="+params.stateId, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                }
                });
    const cityList = await res.json();

    let json_response = {
        status: "success",
        results: cityList.length,
        cityList,
      };
      return NextResponse.json(json_response);
}