import { NextRequest, NextResponse } from "next/server";

export async function GET(request) {

    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getGradeList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                });
    const gradeList = await res.json();

    let json_response = {
        status: "success",
        gradeList,
      };
      return NextResponse.json(json_response);
}