import { NextRequest, NextResponse } from "next/server";

export async function GET(request,{params}) {


    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getGradeById?id="+params.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store' 
                });
    const grades = await res.json();
    // console.log(schoolDetail)

    let json_response = {
        status: "success",
        grades,
      };
      return NextResponse.json(json_response);
}