import { NextRequest, NextResponse } from "next/server";

export async function GET(request) {

    console.log(request);

    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getCountryList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                }
                });
    const counrtyList = await res.json();

    let json_response = {
        status: "success",
        results: counrtyList.length,
        counrtyList,
      };
      return NextResponse.json(json_response);
}