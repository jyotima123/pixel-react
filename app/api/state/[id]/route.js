import { NextRequest, NextResponse } from "next/server";

export async function GET(request,{params}) {

    
    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getStateList?id="+params.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                }
                });
    const stateList = await res.json();

    let json_response = {
        status: "success",
        results: stateList.length,
        stateList,
      };
      return NextResponse.json(json_response);
}