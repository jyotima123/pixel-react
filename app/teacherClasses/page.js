"use client"
import { useEffect, useState } from "react";
import { BiPlusMedical } from "react-icons/bi";
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { alertService } from 'services';
import Alert  from '@components/Alert';
export const dynamic = 'force-dynamic'

import { useSession } from "next-auth/react";

const page = () => {
    const { data: session } = useSession()
   
    const[classList,setClassList] = useState([]);
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });


    useEffect(() => {
        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
            });
        }
    
        callTable();
        
    },[classList]);

  useEffect(() => {
    
    async function fetchData()
        {

            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            window.JSZip = jzip;
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getClassListByTeacherId?id="+session?.user.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    if(list.output.length > 0)
                    {
                        $('#RgTableX').DataTable().destroy();
                        setClassList(list.output)
                    }
                       
                        
                });

        }

        if(session)
        {
            fetchData();
        }
        
  }, [session]);


    

    const copyUrl = (e,url) => {

        // navigator.clipboard.writeText(process.env.NEXT_PUBLIC_URL+'/signup/'+url);

        const textArea = document.createElement("textarea");
            textArea.value = process.env.NEXT_PUBLIC_URL+'/signup/'+url;
            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();
            try {
                document.execCommand('copy');
                alertService.success('URL copied Successfully', options)
            } catch (err) {
                // console.error('Unable to copy to clipboard', err);
                alertService.success('Unable to copy to clipboard', options)
            }
            document.body.removeChild(textArea);
        // 
    }

    const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }


  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="teacherClass" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">Classes</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                    <Alert />
                        <div className="card">
                            <div className="card-header">
                                
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                    <table id="RgTableX" className="table RgTable" style={{width: '100%'}}>
                                        <thead className="RgBGTable">
                                            <tr>
                                                <th>Class Name</th>
                                                <th></th>
                                                <th>Signup Link</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {(classList.length > 0) ?
                                        classList.map((result,key, row) => {

                                            let url = encodeURIComponent(result.url);
                                            

                                            return (
                                                <tr key={key}>
                                                <td>{result.className}</td>
                                               
                                                <td></td>
                                                <td>{process.env.NEXT_PUBLIC_URL+'/signup/'+url} <a className="btn" onClick={event => copyUrl(event,url)}>copy</a></td>
                                               
                                            </tr>
                                            )}):
                                            <></>}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        
    </div>
  )
}

export default page