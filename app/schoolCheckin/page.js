"use client"
import { useEffect, useState } from "react";
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { BiPlusMedical } from "react-icons/bi";
import { BsFillEyeFill } from "react-icons/bs";

import { alertService } from 'services';
import Alert  from '@components/Alert';
import { useSession } from "next-auth/react";

const page = () => {
    const { data: session } = useSession()
   
    const[checkinList,setCheckinList] = useState([]);
    const[leftSide,setLeft] = useState(null);
    
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });

    useEffect(() => {

        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
            });
        }
    
        callTable();
    
        
    },[checkinList]);

  useEffect(() => {
    
    async function fetchData()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;

            
            var url = "/getSchedulesBySchoolId?id="+session?.user.id
            
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+url, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(schList => {
                    if(schList.output.length > 0)
                    {
                        $('#RgTableX').DataTable().destroy();
                        setCheckinList(schList.output)
                    }
                        
                });

        }

        if(session?.user.role == 1)
        {
            fetchData();
        }
        
    
  }, [session]);

  

  const toggleMenu = () => {

    setLeft('layout-menu-expanded')
    
}
const menuCall = () => {

    setLeft(null)
    
}

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
        <Sidebar menu="checkin" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Schedule</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>
    

                    <div className="container-xxl flex-grow-1 container-p-y">
                    <Alert/>
                        
                        <div className="card">
                            <div className="card-header">
                                <div className="d-flex justify-content-end ">
                                
                                </div>
                            </div>
                            <div className="table-responsive table-nowrap">
                                <div className="container-fluid">
                                    {(session) ?
                                    <table id="RgTableX" className="table RgTable" style={{width: "100%"}}>
                                        <thead className="RgBGTable">
                                            <tr>
                                                <th>Schedule name</th>
                                                
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {(checkinList.length > 0)?
                                            checkinList.map((res,key) => {
                                                return (
                                            <tr key={key}>
                                                <td>{res.scheduleName}</td>
                                                
                                                <td><a href={"/checkin/schedule/"+res.id} style={{cursor:"pointer"}}><BsFillEyeFill /> View</a></td>
                                            </tr>
                                                )
                                            })
                                        :
                                        <></>}
                                            
                                            
                                        </tbody>
                                    </table>
                                    :
                                    <></>
                                    }
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        
    </div>
  )
}

export default page