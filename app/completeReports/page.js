"use client"
import { useEffect, useState } from "react";
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import Select from "react-select";

const page = () => {
    const[reportList,setReportList] = useState([]);
    const[schoolData,SetSchoolData] = useState([]);
    const[schoolId,setschoolId] = useState();
    const[gradeData,SetGradeData] = useState([]);
    const[gradeId,setgradeId] = useState();
    const[surveyData,SetSurveyData] = useState([]);
    const[surveyId,setsurveyId] = useState();
    const[leftSide,setLeft] = useState(null);
    const[filter,setFilter] = useState({
        surveys: [],
        schools: [],
        grades: []
    })

    useEffect(() => {
        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            await import ('datatables.net-buttons/js/dataTables.buttons.min')
            await import ('datatables.net-buttons/js/buttons.html5.min')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              },
              dom: 'Bfrtip',
              buttons: [
                { extend: 'excelHtml5', text: 'Export',className: 'btn btn-theme btn-outline-theme me-2' }
              ]  
            });
        }
    
        callTable();
        
    },[reportList]);


     //effect on updating filter
     useEffect(() => {
    
        async function fetchData()
            {
                await import('datatables.net-dt/js/dataTables.dataTables')
                await import('datatables.net-dt/css/jquery.dataTables.min.css')
                
                window.JSZip = jzip;
    
                
                const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSurveyCompletionReport", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    cache: 'no-store',
                    body: JSON.stringify(filter),
                    }).then(res => res.json())
                    .then(list => {
    
                        $('#RgTableX').DataTable().destroy();
                            setReportList(list.output)
                           
                    });


                    
                    const result2 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getReportGradeList", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify(filter),
                        cache: 'no-store'
                        }).then(res => res.json())
                        .then(list => {
                            
                        // console.log(list)
                        SetGradeData(list.output)
                    });
    
            }
    
            fetchData();
      }, [filter]);


    useEffect(() => {
    
        async function fetchData()
            {
    
                await import('datatables.net-dt/js/dataTables.dataTables')
                await import('datatables.net-dt/css/jquery.dataTables.min.css')
                window.JSZip = jzip;
                

                    var data = {
                        districts: []
                    }
    
                    const result1 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSchoolList", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify(data),
                        cache: 'no-store'
                        }).then(res => res.json())
                        .then(list => {
                            
                        // console.log(list)
                        SetSchoolData(list.output)
                    });

                   

                    
                    const result3 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSurveyDropDownList", {
                        method: "GET",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        cache: 'no-store'
                        }).then(res => res.json())
                        .then(list => {
                            
                        // console.log(list)
                        SetSurveyData(list.output)
                    });
            }
            fetchData();
        
      }, []);


      const changeschool = async e => {

        var report = []
        for(var i = 0; i< e.length; i++)
        {
            report.push(e[i].value)
        }
        setFilter({ ...filter, 'schools': report});
        
        setschoolId(e);
    };

    const changegrade = async e => {

        var report = []
        for(var i = 0; i< e.length; i++)
        {
            report.push(e[i].value)
        }
        setFilter({ ...filter, 'grades': report});
        
        setgradeId(e);
    }

    const changesurvey = async e => {

        var report = []
        for(var i = 0; i< e.length; i++)
        {
            report.push(e[i].value)
        }
        setFilter({ ...filter, 'surveys': report});
        
        setsurveyId(e);
    }

    const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }


  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="surveyreports" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Survey Completion Report</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                    
                        <div className="card">
                            <div className="card-header">
                                <div className="row d-flex align-items-center">
                                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3 mb-Rg-3">
                                        <label for="defaultSelect" className="form-label">Filter By</label>
                                        <Select 
                                            options={schoolData}
                                            placeholder="School"
                                            value={schoolId}
                                            onChange={changeschool}
                                            isSearchable={true}
                                            isMulti
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                    </div>
                                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3 mb-Rg-3">
                                        <label for="defaultSelect" className="form-label">Filter By</label>
                                        <Select 
                                            options={gradeData}
                                            placeholder="Grade"
                                            value={gradeId}
                                            onChange={changegrade}
                                            isSearchable={true}
                                            isMulti
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                    </div>
                                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3 mb-Rg-3">
                                        <label for="defaultSelect" className="form-label">Filter By</label>
                                        <Select 
                                            options={surveyData}
                                            placeholder="Survey"
                                            value={surveyId}
                                            onChange={changesurvey}
                                            isSearchable={true}
                                            isMulti
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                
                                <table id="RgTableX" className="table RgTable" style={{width: '100%'}}>
                                        <thead className="RgBGTable">
                                            <tr>
                                                <th>Student Id</th>
                                                <th>School Name</th>
                                                <th>Grade</th>
                                                <th>Class</th>
                                                <th>Checkin Name</th>
                                                <th>Survey Name</th>
                                                <th>Survey Sent</th>
                                                <th>Survey Start</th>
                                                <th>Survey Complete</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {(reportList.length > 0)?
                                        reportList.map((result,key, row) => {
                                            return (
                                                <tr key={key}>
                                                <td>{result.studentId}</td>
                                                <td>{result.schoolName}</td>
                                                <td>{result.gradeName}</td>
                                                <td>{result.className}</td>
                                                <td>{result.checkinName}</td>
                                                <td>{result.surveyName}</td>
                                                <td>{result.surveySent}</td>
                                                <td>{result.surveyStart}</td>
                                                <td>{result.surveyComplete}</td>
                                                
                                            </tr>
                                            )}):
                                            <></>
                                            }                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                   
                </div>
            </div>
        </div>
        
    </div>
  )
}

export default page