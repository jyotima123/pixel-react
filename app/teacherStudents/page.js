"use client"
import { useEffect, useState } from "react";
import { BiPlusMedical } from "react-icons/bi";
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { alertService } from 'services';
import Alert  from '@components/Alert';
export const dynamic = 'force-dynamic'
import EditModal from "./editModal";
import Modal from "./addModal";
import ResetModal from "@app/students/resetModal";
import { useSession } from "next-auth/react";

const page = () => {
    const { data: session,update } = useSession()
    const [showModal, setShowModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const [resetModal, setResetModal] = useState(false);
    const[studentList,setStudentList] = useState([]);
    const[classData,SetClassData] = useState([]);
    const[classes,setClasses] = useState(0);
    const[editId,setId] = useState(false);
    const[schoolId,setSchoolId] = useState(false);
    const[schooladdId,setSchooladdId] = useState(false);
    const[resetId,setRId] = useState(false);
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });

    useEffect(() => {
        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
            });
        }
    
        callTable();
        
    },[studentList]);

  useEffect(() => {
    
    async function fetchData()
        {

            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            window.JSZip = jzip;
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getStudentListByTeacherId?id="+session?.user.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                        $('#RgTableX').DataTable().destroy();
                        // console.log(list.output[0].school)
                        setStudentList(list.output)
                        setSchooladdId(list.school_id)
                        
                });

            const result = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getClassesDropListByTeacherId?id="+session?.user.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                    SetClassData(list.output)
           
            });
        }

        if(session?.user.role == 2 && classes == 0)
        {
            fetchData();
        }
        
  }, [session]);

  const childCall = (res,msg) =>{

    setShowModal(false)
    if(res == 'success')
    {   
        location.reload();
        alertService.success(msg, options)
    }
    if(res == 'error')
    alertService.error(msg, options)

} 
    const openEditModal = (event, param,school) => {
        setId(param);
        setSchoolId(school)
        setEditModal(true);
    }
    const editchildCall = (res,msg) =>{

        setEditModal(false)
        if(res == 'success')
        {   
            location.reload();
            alertService.success(msg, options)
        }
        if(res == 'error')
        {
            
            alertService.error(msg, options)
        }

    }

    const openResetModal = (e,id) => {
        setRId(id);
        setResetModal(true)
    }
    const resetchildCall = (msg) =>{

        setResetModal(false)
        if(msg == 'success')
        {   
            location.reload();
            alertService.success('Password Updated Successfully', options)
        }
        if(msg == 'error')
        {
            location.reload();
            alertService.error('Something went wrong', options)
        }

    }


    const getFilter = async(e) => {

        var url = process.env.NEXT_PUBLIC_APP_URL+'/getStudentListByTeacherIdAndClassId'

        let data = {
            teacherId : session?.user.id,
            classId : e.target.value
        }

        setClasses(e.target.value)

        const res = await fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store',
                body: JSON.stringify(data)
                }).then(res => res.json())
                .then(res => {
                    if(res.success)
                    {
                        $('#RgTableX').DataTable().destroy();
                        setStudentList([])
                        setStudentList(res.output)
                    }
                });
       
    }

    const generateReport = async () => {

        
        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/getStudentListInSheetByTeacherId?id='+session?.user.id, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                if(res.success)
                {
                    let alink = document.createElement('a');
                    alink.href = res.fileurl;
                    alink.download = 'report.xlsx';
                    alink.click();
                }
            });
    }

    const login = async(e,values) => {

        if(session?.user.loginrole == 0)
        {
            if (typeof window !== "undefined" && window.localStorage) {
                localStorage.setItem("schoolTeacher", session?.user.id);
            }
        }
        

        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/getUserById?id='+values, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store',
            }).then(res => res.json())
            .then(async res => {
                if(res.success)
                {
                    
                    await update({
                        ...session,
                        user:{
                            ...session?.user,
                            name: res.data.fullName,
                            role: res.data.role,
                            email: res.data.email,
                            id: res.data.id
                        },
                    })
                    
                    
                    window.location.href = '/studentDashboard'
                }
            });
            
    }

    const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
           <Sidebar menu="students" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">Students</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">
                    <Alert/>
                        
                        <div className="card">
                            <div className="card-header">
                                <div className="row d-flex align-items-center">
                                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-4 mb-Rg-3">
                                        <label for="defaultSelect" className="form-label">Filter By</label>
                                        <select id="defaultSelect" className="form-select"  value={classes} onChange={e => getFilter(e)}>
                                            <option value="0">Select Class</option>
                                            {(classData.length > 0)?
                                            classData.map((result,key) => {
                                            return (
                                                <option key={key} value={result.id}>{result.className}</option>
                                            )}):
                                            <></>}
                                        </select>
                                    </div>
                                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-4 mt-3 mb-Rg-3">
                                        <a type="button" className="btn btn-theme btn-outline-theme"  onClick={generateReport}>
                                            <span className="bx bxs-download"></span>&nbsp; Export
                                        </a>
                                    </div>
                                    <div className="col-lg-6 col-md-4 col-sm-4 col-xs-4 text-end">
                                        <a type="button" className="btn btn-primary"  onClick={() => setShowModal(true)}>
                                            <BiPlusMedical />&nbsp; Add Student
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                    <table id="RgTableX" className="table RgTable" style={{width: '100%'}}>
                                        <thead className="RgBGTable">
                                            <tr>
                                                <th>Student Name</th>
                                                <th>Email/Username</th>
                                                
                                                <th>ClassName</th>
                                                
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {(studentList.length > 0) ?
                                            (studentList.map((result,key) => {
                                                return(
                                                    <tr key={key}>
                                                        <td>{result.fullName}</td>
                                                    
                                                        <td>{result.email}</td>
                                                        
                                                        <td>{result.className}</td>
                                                        
                                                        <td>
                                                        {/* <a onClick={event => login(event, result.id)} style={{cursor:"pointer"}} title="Login As Student"><i style={{fontSize:'2.15rem'}} className="bx bx-right-arrow-circle"></i>
                                                                </a> */}
                                                            <a onClick={event => openEditModal(event, result.id,result.school)} style={{cursor:"pointer"}}><i className="bx bx-edit-alt me-1"></i> </a>
                                                            
                                                            <a className="btn btn-primary btn-xs" onClick={event => openResetModal(event, result.id)} style={{cursor:"pointer"}} title="Reset Password"><i className="bx bx-reset me-1"></i> </a>
                                                        </td>
                                                    </tr>
                                                )
                                            })):
                                            <></>}
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                    <div>
                        {showModal && <Modal schoolId={schooladdId} onClose={childCall}/> }
                        {editModal && <EditModal schoolId={schoolId} student_id = {editId} onClose={editchildCall}/> }
                        {resetModal && <ResetModal student_id = {resetId} onClose={resetchildCall}/> }
                    </div>
                </div>
            </div>
        </div>
        {/*Below we add the modal wrapper*/}
        <div id="modal-root"></div>
    </div>
  )
}

export default page