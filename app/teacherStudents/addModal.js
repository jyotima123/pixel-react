import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useSession } from "next-auth/react";

const Modal = ({ onClose, children, title,schoolId }) => {
    const { data: session } = useSession()
    const [isLoading, setLoading] = useState(false);
    const[gradeData,SetGradeData] = useState([]);
    const[classData,SetClassData] = useState([]);
    const[classId,setclassId] = useState();
    const [student, setStudent] = useState({
        studentId:"",
        fullName: "",
        email: "",  
        grade:"",
        studentClass:"",
        status:1,
        password:"password",
        schoolId:schoolId
      });
      const [errors, setError] = useState({
        errors: "",
      });

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    useEffect(() => {
        async function fetchData()
        {
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getGradeDropListBySchoolId?id="+schoolId, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(res.list)
                SetGradeData(list)
            });

        }
        fetchData();    
    },[]);

    
    const onInputChange = e => {
        setStudent({ ...student, [e.target.name]: e.target.value });
    };

    const onGradeChange = async e => {
       
        setStudent({ ...student, [e.target.name]: e.target.value });

        let data = {
            schoolId : schoolId,
            gradeId : e.target.value
        }

        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getClassesDropListByGradeId", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store',
            body: JSON.stringify(data)
            }).then(res => res.json())
            .then(res => {
               
                SetClassData(res.output)
            
            });

    };


    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        
        //username
        if(!student["fullName"]){
        formIsValid = false;
        errors["fullName"] = "Name Cannot be empty";
        }
        //contact person
        // if(!student["studentId"]){
        //     formIsValid = false;
        //     errors["studentId"] = "Please enter student id";
        //     }
       
        //Email
         if(!student["email"]){
            formIsValid = false;
            errors["email"] = "Email cannot be empty";
          }

          if(!student["grade"]){
            formIsValid = false;
            errors["grade"] = "Grade cannot be empty";
          }
          if(!student["studentClass"]){
            formIsValid = false;
            errors["studentClass"] = "Class cannot be empty";
          }
          
        
    
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/student/add", {
            method: "POST",
            body: JSON.stringify(student),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success",res.message);
                }
                else{
                    onClose("error",res.message);
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };

     

    const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Add Student</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                    <form>
                     <div class="row">
                        {/* <div class="col-md-6">
                             <div class="mb-3">
                                 <label for="defaultFormControlInput" class="form-label">Student ID</label>
                                 <input type="text" class="form-control" id="defaultFormControlInput" placeholder="34534" aria-describedby="defaultFormControlHelp" name="studentId" onChange={e => onInputChange(e)}/>
                                <span className="error" style={{ color: "red" }}>{errors['studentId']}</span>
                             </div>
                         </div> */}
                         <div class="col-md-6">
                             <div class="mb-3">
                                 <label for="defaultFormControlInput" class="form-label">Student Name</label>
                                 <input type="text" class="form-control" id="defaultFormControlInput" placeholder="Rajiv" aria-describedby="defaultFormControlHelp" name="fullName" onChange={e => onInputChange(e)}/>
                                <span className="error" style={{ color: "red" }}>{errors['fullName']}</span>
                             </div>
                         </div>
                         <div class="col-md-6">
                             <div class="mb-3">
                                 <label class="form-label" for="basic-icon-default-email">Email</label>
                                 <div class="input-group input-group-merge">
                                     <span class="input-group-text"><i class="bx bx-envelope"></i></span>
                                     <input type="text" id="basic-icon-default-email" class="form-control" placeholder="Rajiv.doe@example.com" aria-label="Rajiv.doe@example.com" aria-describedby="basic-icon-default-email2" name="email" onChange={e => onInputChange(e)}/>
                                 </div>
                                <span className="error" style={{ color: "red" }}>{errors['email']}</span>
                             </div>
                         </div>
                         <div class="col-md-6">
                             <div class="mb-3">
                                <label for="defaultSelect" className="form-label">Grade</label>
                                <select id="defaultSelect" className="form-select" name="grade" onChange={e => onGradeChange(e)} required>
                                    <option>Select Grade</option>
                                    {gradeData.map((result,key) => {
                                        return (
                                            <option key={key} value={result.gradeId}>{result.gradeName}</option>
                                        )})}
                                </select>
                             </div>
                             <span className="error" style={{ color: "red" }}>{errors['grade']}</span>
                         </div>
                         <div className="col-md-6">
                            <div className="mb-3">
                                <label for="defaultSelect" className="form-label">Class</label>
                                <select id="defaultSelect" name="studentClass" className="form-select" onChange={e => onInputChange(e)} required>
                                    <option>Select Class</option>
                                    {classData.map((result,key) => {
                                    return (
                                        <option key={key} value={result.id}>{result.className}</option>
                                    )})}
                                </select>
                            </div>
                            <span className="error" style={{ color: "red" }}>{errors['studentClass']}</span>
                        </div>
                     </div>
                     <div class="row">
                         
                         
                         

                         <div class="col-md-6">
                             <div class="">
                                 <label class="form-label d-block">Status</label>
                                 <div class="form-check form-check-inline mt-2">
                                     <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" defaultChecked  onChange={e => onInputChange(e)}/>
                                     <label class="form-check-label" for="inlineRadio1">Active</label>
                                 </div>
                                 <div class="form-check form-check-inline">
                                     <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="0"  onChange={e => onInputChange(e)}/>
                                     <label class="form-check-label" for="inlineRadio2">Inactive</label>
                                 </div>
                             </div>
                         </div>
                         
                     </div>
                   
                 </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
                        {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
}

export default Modal