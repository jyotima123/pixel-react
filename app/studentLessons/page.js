"use client"
import { useEffect, useState } from "react";
import { BiPlusMedical } from "react-icons/bi";
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { alertService } from 'services';
import Alert  from '@components/Alert';
export const dynamic = 'force-dynamic'
import { useSession } from "next-auth/react";

const page = () => {
    const { data: session } = useSession()
    const[lessonData,SetLessonData] = useState([]);
    const[classData,SetClassData] = useState([]);
    const[classes,setClasses] = useState(0);
    const[tab,setTab] = useState(0)
    const[leftSide,setLeft] = useState(null);

    useEffect(() => {
    
        async function fetchData()
            {
    
                await import('datatables.net-dt/js/dataTables.dataTables')
                await import('datatables.net-dt/css/jquery.dataTables.min.css')
                window.JSZip = jzip;
                const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getLessonsByStudentId?id="+session?.user.id, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    cache: 'no-store'
                    }).then(res => res.json())
                    .then(list => {
                            // console.log(schoolList)
                            SetLessonData(list.output)
                            
                    });

                    
            }
            if(session?.user.role == 3)
            {
                fetchData();
            }
        
      }, [session]);


      const openTab = (e,key) => {

        setTab(key)

      }


      const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }
   

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
        <Sidebar menu="studentLesson" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">Lessons</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                        
                        <div className="card">
                            <div className="card-header">
                                <div className="row align-items-center justify-content-between border-bottom">
                                    
                                    
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="nav-align-top mb-4">
                                    <ul className="nav nav-tabs nav-fill" role="tablist">
                                        {(lessonData.map((result,key) => {
                                            return(
                                                <li className="nav-item">
                                                    <button type="button" id={"p_"+key} className={(tab == key)? "nav-link active":"nav-link"} role="tab" onClick={event => openTab(event,key)}>
                                                        <i className="tf-icons bx bxs-book-open"></i> {result.lessonName}
                                                    </button>
                                                </li>
                                            )
                                        }))}
                                        
                                    </ul>
                                    <div className="tab-content">
                                        {(lessonData.map((result,key) => {
                                            return(
                                                <div className={(tab == key) ?"tab-pane fade active show" : "tab-pane fade"} id="navs-justified-Rgtab1" role="tabpanel">
                                                <div className="row">
                                                    <div className="col-md-12 mb-3">
                                                        <div className="Rg-img"><img className="lesson-img" src={result.cover}/></div>
                                                        <div className="text-end mt-2">
                                                            <span className="badge bg-secondary">Timing: {result.timing} minutes</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12 border-bottom mb-3">
                                                        <h3 className="sub-heading clr-theme">Description</h3>
                                                        <p className="mb-2">
                                                            {result.description}
                                                        </p>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="row">
                                                           
                                                            {/* <div className="col-lg-12 mb-2">
                                                                <a href={result.resourceUrl} type="button" className="btn d-block rounded-pill btn-primary" target="_blank">
                                                                    &nbsp; View Slide Deck
                                                                </a>
                                                            </div> */}
                                                            
                                                            <div className="col-lg-12 mb-2">
                                                                <a href={result.resourcePdf} type="button" className="btn d-block rounded-pill btn-primary" download target="_blank">
                                                                    <span className="tf-icons bx bx-download"></span>&nbsp; Download Resource Deck
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            )
                                        }))}
                                       
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
    </div>
  )
}

export default page