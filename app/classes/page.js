"use client"
import { useEffect, useState } from "react";
import { BiPlusMedical } from "react-icons/bi";
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { alertService } from 'services';
import Alert  from '@components/Alert';
export const dynamic = 'force-dynamic'
import Modal from "./addModal";
import EditModal from "./editModal";
import { useSession } from "next-auth/react";

const page = () => {
    const { data: session } = useSession()
    const [showModal, setShowModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const[classList,setClassList] = useState([]);
    const[editId,setId] = useState(false);
    const[grade,setGrade] = useState(0);
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });


    useEffect(() => {
        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
            });
        }
    
        callTable();
        
    },[classList]);

  useEffect(() => {
    
    async function fetchData()
        {

            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            window.JSZip = jzip;
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getClassListBySchoolId?id="+session?.user.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {

                    if(list.output.length > 0)
                    {
                        $('#RgTableX').DataTable().destroy();
                        setClassList(list.output)
                    }
                       
                });

        }

        if(session)
        {
            fetchData();
        }
        
  }, [session]);


    const childCall = (msg) =>{

        setShowModal(false)
        if(msg == 'success')
        {   
            location.reload();
            alertService.success('Class Added Successfully', options)
        }
        if(msg == 'error')
        alertService.error('Something went wrong', options)
    
    } 

    const changeStatus = async (event, param) => {

        var data = {
            'id' : param,
            'status' : event.target.checked
        }
        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateClassStatus", {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
            
            
            if(res.success)
            {
                location.reload()
                alertService.success('Status Updated Successfully', options)
            }
            else{
                alertService.error('Something went wrong', options)
            }
        });
    }

    const handleDelete = async(event, param) => {
        if (confirm("Do you really want to delete this data.")) {

            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/class/delete/"+param, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                if(res.success)
                {
                    location.reload()
                    alertService.success('Class deleted successfully', options)
                }
                else{
                    alertService.error(res.message, options)
                }
            });
        }
    }

    const openEditModal = (event, param) => {
        setId(param);
        
        setEditModal(true);
    }
    const editchildCall = (msg) =>{

        setEditModal(false)
        if(msg == 'success')
        {   
            location.reload();
            alertService.success('Class Updated Successfully', options)
        }
        if(msg == 'error')
        {
            location.reload();
            alertService.error('Something went wrong', options)
        }

    }

    const copyUrl = (e,url) => {

        // navigator.clipboard.writeText(process.env.NEXT_PUBLIC_URL+'/signup/'+url);

        const textArea = document.createElement("textarea");
            textArea.value = process.env.NEXT_PUBLIC_URL+'/signup/'+url;
            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();
            try {
                document.execCommand('copy');
                alertService.success('URL copied Successfully', options)
            } catch (err) {
                // console.error('Unable to copy to clipboard', err);
                alertService.success('Unable to copy to clipboard', options)
            }
            document.body.removeChild(textArea);
        // 
    }

    const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }


  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="classes" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4"  onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">Classes</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                    <Alert />
                        <div className="card">
                            <div className="card-header">
                                <div className="row d-flex align-items-center">
                                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-4 mb-Rg-3">
                                        
                                    </div>
                                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-4 mt-3 mb-Rg-3">
                                       
                                    </div>
                                    <div className="col-lg-6 col-md-4 col-sm-4 col-xs-4 text-end">
                                        <a type="button" className="btn btn-primary" onClick={() => setShowModal(true)}>
                                            <BiPlusMedical />&nbsp; Add Class
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                    <table id="RgTableX" className="table RgTable" style={{width: '100%'}}>
                                        <thead className="RgBGTable">
                                            <tr>
                                                <th>Class Name</th>
                                                <th>Grade</th>
                                                <th>Signup Link</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {(classList.length > 0)?
                                        classList.map((result,key, row) => {

                                            let url = encodeURIComponent(result.url);
                                            

                                            return (
                                                <tr key={key}>
                                                <td>{result.className}</td>
                                               
                                                <td>{result.gradeName}</td>
                                                <td>{process.env.NEXT_PUBLIC_URL+'/signup/'+url} <a className="btn" onClick={event => copyUrl(event,url)}>copy</a></td>
                                                <td>
                                                    <div className="form-check form-switch mb-2">
                                                        <input className="form-check-input" type="checkbox" id="flexSwitchCheckDefault" checked={result.status} onChange={event => changeStatus(event, result.id)}/>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a onClick={event => openEditModal(event, result.id)} style={{cursor:"pointer"}}><i className="bx bx-edit-alt me-1"></i>
                                                                </a>
                                                    <a onClick={event => handleDelete(event, result.id)} style={{cursor:"pointer"}}><i className="bx bx-trash me-1"></i>
                                                                </a>
                                                </td>
                                            </tr>
                                            )}):
                                            <></>}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div>
                        {showModal && <Modal onClose={childCall}/> }
                        {editModal && <EditModal class_id = {editId} onClose={editchildCall}/> }
                    </div>
                </div>
            </div>
        </div>
        {/*Below we add the modal wrapper*/}
        <div id="modal-root"></div>
    </div>
  )
}

export default page