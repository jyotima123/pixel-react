import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import Select from "react-select";
import { useSession } from "next-auth/react";

const Modal = ({ onClose, children, title }) => {
    const { data: session } = useSession()
    const [isLoading, setLoading] = useState(false);
    const[gradeData,SetGradeData] = useState([]);
    const [classes, setClasses] = useState({
        className: "",
        gradeId:"",
        status:1,
        schoolId:session?.user.id
      });
      const [errors, setError] = useState({
        errors: "",
      });

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    useEffect(() => {
        async function fetchData()
        {
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getGradeDropListBySchoolId?id="+session?.user.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(gradeList => {
                    
                // console.log(res.gradeList)
                SetGradeData(gradeList)
            });

        }
        fetchData();    
    },[]);

    
    const onInputChange = e => {
        setClasses({ ...classes, [e.target.name]: e.target.value });
    };


    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        
        //username
        if(!classes["className"]){
        formIsValid = false;
        errors["className"] = "Name Cannot be empty";
        }
        //contact person
        if(!classes["gradeId"]){
            formIsValid = false;
            errors["grades"] = "Please select grade";
            }
       
    
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/class/add", {
            method: "POST",
            body: JSON.stringify(classes),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success");
                }
                else{
                    onClose("error");
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };

      const customStyles = {
        control: base => ({
          ...base,
          height: 33,
          minHeight: 33,
          borderRadius: 5,
        })
      };

    const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Add Class</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                    <form>
                     <div class="row">
                         <div class="col-md-6">
                             <div class="mb-3">
                                 <label for="defaultFormControlInput" class="form-label">Class Name</label>
                                 <input type="text" class="form-control" id="defaultFormControlInput" placeholder="class 1" aria-describedby="defaultFormControlHelp" name="className" onChange={e => onInputChange(e)}/>
                                <span className="error" style={{ color: "red" }}>{errors['className']}</span>
                             </div>
                         </div>
                         <div class="col-md-6">
                             <div class="mb-3">
                                <label for="defaultSelect" className="form-label">Grade</label>
                                <select id="defaultSelect" className="form-select" name="gradeId"  onChange={e => onInputChange(e)}>
                                    <option value="0">Select Grade</option>
                                    {gradeData.map((result,key) => {
                                    return (
                                        <option key={key} value={result.gradeId}>{result.gradeName}</option>
                                    )})}
                                </select>
                             </div>
                             <span className="error" style={{ color: "red" }}>{errors['grades']}</span>
                         </div>
                     </div>
                     
                   
                 </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
                        {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
}

export default Modal