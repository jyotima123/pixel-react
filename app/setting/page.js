"use client"
import { useEffect, useState } from "react";
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { useSession } from "next-auth/react";
import { alertService } from 'services';
import Alert  from '@components/Alert';

const page = () => {
    const { data: session } = useSession()
    const [isLoading, setLoading] = useState(false);
    const[leftSide,setLeft] = useState(null);
    const [userData, setUserData] = useState({
        id:"",
        fullName: "",
        email: "",  
        phone: "",
        district:""
      });
      const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });

    useEffect(() => {
    
        async function fetchData()
            {
                
                const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getUserById?id="+session?.user.id, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    cache: 'no-store'
                    }).then(res => res.json())
                    .then(userData => {
                            
                            console.log(userData)
                            setUserData(userData.data)
                            
                    });
    
                
            }

            if(session)
            {
                fetchData();
            }
            
        
      }, [session]);


      const onInputChange = e => {

        if(e.target.name == 'phone')
        {
            var contact = formatPhoneNumber(e.target.value)
            // console.log(contact)
            setUserData({ ...userData, [e.target.name]: contact });
        }
        else{
            setUserData({ ...userData, [e.target.name]: e.target.value });
        }
        
    };

    const formatPhoneNumber = (phoneNumberString) => {
        var cleaned = ('' + phoneNumberString).replace(/\D/g, '');
        var match = phoneNumberString.match(/^([0-9]{3})([0-9]{3})([0-9]{4})$/);
        // console.log(match)
        if (match) {
          return  match[1] + ' ' + match[2] + ' ' + match[3];
        }
        return cleaned;
      }

      const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/user/update", {
            method: "PUT",
            body: JSON.stringify(userData),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    location.reload();
                    alertService.success('Account Updated Successfully', options)
                }
                else{
                    alertService.error('Something went wrong', options)
                }
            });
  
          
      };

      const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar onClick={menuCall}/>
            <div className="layout-page">

                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Update Details</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">
                        <Alert />
                        <div className="row">
                            <div className="col-md-12">
                                <ul className="nav nav-pills flex-column flex-md-row mb-4">
                                    <li className="nav-item">
                                        <a className="nav-link active"><i className="bx bx-user me-1"></i> Account</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="/changePassword"><i className="bx bx bxs-key me-1"></i> Change Password</a>
                                    </li>
                                </ul>
                                <div className="card mb-4">
                                    <h5 className="card-header">Profile Details</h5>
                                    
                                    <hr className="my-0" />
                                    <div className="card-body">
                                        <form id="formAccountSettings" method="POST" onsubmit="return false">
                                            <div className="row">
                                                <div className="mb-3 col-md-6">
                                                    <label for="firstName" className="form-label">Name</label>
                                                    <input className="form-control" type="text" id="firstName" name="fullName" value={userData.fullName} autofocus onChange={e => onInputChange(e)}/>
                                                </div>
                                                {/* <div className="mb-3 col-md-6">
                                                    <label for="lastName" className="form-label">Last Name</label>
                                                    <input className="form-control" type="text" name="lastName" id="lastName" value="Doe" />
                                                </div> */}
                                                <div className="mb-3 col-md-6">
                                                    <label for="email" className="form-label">E-mail</label>
                                                    <input className="form-control" type="text" id="email" name="email" value={userData.email} placeholder="Rajiv.doe@example.com" readOnly/>
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label className="form-label" for="phoneNumber">Phone Number</label>
                                                    <div className="input-group input-group-merge">
                                                        {/* <span className="input-group-text">US (+1)</span> */}
                                                        <input type="text" id="phoneNumber" name="phone" value={userData.phone} className="form-control" placeholder="202 555 0111" maxLength={12} onChange={e => onInputChange(e)}/>
                                                    </div>
                                                </div>
                                                {(session?.user.role == 1) ?
                                                 <div className="mb-3 col-md-6">
                                                    <label for="district" className="form-label">District</label>
                                                    <input type="text" className="form-control" id="district" value={userData.district} name="district" placeholder="" readOnly/>
                                                </div>
                                                :
                                                (session?.user.role == 2) ?
                                                <div className="mb-3 col-md-6">
                                                    <label for="district" className="form-label">School Name</label>
                                                    <input type="text" className="form-control" id="district" value={userData.district} name="district" placeholder="" readOnly/>
                                                </div>
                                                :
                                                <>
                                                </>
                                                }
                                               {/* <div className="mb-3 col-md-6">
                                                    <label for="state" className="form-label">State</label>
                                                    <input className="form-control" type="text" id="state" name="state" placeholder="California" />
                                                </div>
                                                <div className="mb-3 col-md-6">
                                                    <label for="zipCode" className="form-label">Zip Code</label>
                                                    <input type="text" className="form-control" id="zipCode" name="zipCode" placeholder="231465" maxlength="6" />
                                                </div> */}
                                                {/* <div className="mb-3 col-md-6">
                                                    <label className="form-label" for="country">Country</label>
                                                    <select id="country" className="select2 form-select">
                                                        <option value="">Select</option>
                                                        <option value="Australia">Australia</option>
                                                        <option value="Bangladesh">Bangladesh</option>
                                                        <option value="Belarus">Belarus</option>
                                                        <option value="Brazil">Brazil</option>
                                                        <option value="Canada">Canada</option>
                                                        <option value="China">China</option>
                                                        <option value="France">France</option>
                                                        <option value="Germany">Germany</option>
                                                        <option value="India">India</option>
                                                        <option value="Indonesia">Indonesia</option>
                                                        <option value="Israel">Israel</option>
                                                        <option value="Italy">Italy</option>
                                                        <option value="Japan">Japan</option>
                                                        <option value="Korea">Korea, Republic of</option>
                                                        <option value="Mexico">Mexico</option>
                                                        <option value="Philippines">Philippines</option>
                                                        <option value="Russia">Russian Federation</option>
                                                        <option value="South Africa">South Africa</option>
                                                        <option value="Thailand">Thailand</option>
                                                        <option value="Turkey">Turkey</option>
                                                        <option value="Ukraine">Ukraine</option>
                                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                                        <option value="United Kingdom">United Kingdom</option>
                                                        <option value="United States">United States</option>
                                                    </select>
                                                </div> */}
                                            </div>
                                            <div className="mt-2">
                                            {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Save Changes</button>}
            {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default page