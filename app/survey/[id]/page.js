"use client"
import { useEffect, useState } from "react";
import { useSearchParams,usePathname } from 'next/navigation'
import { BiPlusMedical } from "react-icons/bi";
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import AddGroup from "@app/addSurvey/addGroup";
import EditGroup from "@app/addSurvey/editGroup";
import AddQuestion from "@app/addSurvey/addQuestion";
import EditQuestion from "@app/addSurvey/editQuestion";
import { alertService } from 'services';
import Alert  from '@components/Alert';
export const dynamic = 'force-dynamic'
import { useSession } from "next-auth/react";

const page = () => {
    const { data: session,update } = useSession()
    const pathname = usePathname()
    const [isLoading, setLoading] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const[editId,setId] = useState(false);
    const [qstModal, setQstModal] = useState(false);
    const [editqstModal, setEditQstModal] = useState(false);
    const[qstId,setqstId] = useState(false);
    const[groupList,setGroupList] = useState([]);
    const[grpId,setGrpId] = useState(0);
    const[leftSide,setLeft] = useState(null);

    const sid = pathname.split('/')
    
    const [survey, setSurvey] = useState({
        id:"",
        surveyName: "",
        groupIds:[]
      });
    const [errors, setError] = useState({
        errors: "",
      });
      const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });

    useEffect(() => {
        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $(".RgTableSimple").DataTable({
                "searching": false,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": false,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": false, // Will Disabled Record number per page  
                "paging": false,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              }
              });
        }
    
        callTable();

        
    },[groupList]);

    useEffect(() => {
    
        async function fetchData()
            {
    
                await import('datatables.net-dt/js/dataTables.dataTables')
                await import('datatables.net-dt/css/jquery.dataTables.min.css')
                window.JSZip = jzip;
                const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSurveyById?id="+sid[2], {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    cache: 'no-store'
                    }).then(res => res.json())
                    .then(grplist => {
                            $('.RgTableSimple').DataTable().destroy();
                            // console.log(grplist)
                            setGroupList(grplist.output.groups)
                            var gd = []
                            grplist.output.groups.map((res,key,row) => {
                            
                                gd.push(res.groupId) 
                            })
                           
                            // setSurvey({ ...survey, groupIds: gd });
                            setSurvey({ id: grplist.output.id, groupIds: gd, surveyName: grplist.output.surveyName});
                            // setSurvey({ ...survey, surveyName: grplist.output.surveyName });
                            
                    });
    
            }
            fetchData();
        
      }, []);

    const groupChildCall = (msg) =>{

        setShowModal(false)
        if(msg == 'success')
        {   
            location.reload();
            alertService.success('Group Added Successfully', options)
        }
        if(msg == 'error')
        alertService.error('Something went wrong', options)
    
      }
    const openQstModal = (event, param) => {
        setGrpId(param);
        
        setQstModal(true);
    }

    const qstChildCall = (msg) =>{

        setQstModal(false)
        if(msg == 'success')
        {   
            location.reload();
            alertService.success('Question Added Successfully', options)
        }
        if(msg == 'error')
        alertService.error('Something went wrong', options)

    }
    const openQstEditModal = (event, param) => {
        setqstId(param);
        
        setEditQstModal(true);
    }
    const openEditModal = (event, param) => {
        setId(param);
        
        setEditModal(true);
    }
    const editchildCall = (msg) =>{

        setEditModal(false)
        if(msg == 'success')
        {   
            location.reload();
            alertService.success('Group Updated Successfully', options)
        }
        if(msg == 'error')
        alertService.error('Something went wrong', options)

    }
    const editqstchildCall = (msg) =>{

        setEditQstModal(false)
        if(msg == 'success')
        {   
            location.reload();
            alertService.success('Question Updated Successfully', options)
        }
        if(msg == 'error')
        alertService.error('Something went wrong', options)

    }
    const handleDelete = async(event, param) => {
        if (confirm("Do you really want to delete this data.")) {

            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/question/delete/"+param, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                if(res.success)
                {
                    location.reload()
                    alertService.success('Question deleted successfully', options)
                }
                else{
                    alertService.error('Something went wrong', options)
                }
            });
        }
    }
    const changeStatus = async (event, param) => {
   
        var data = {
            'id' : param,
            'status' : event.target.checked
        }
        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateQuestionStatus", {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                
                
                if(res.success)
                {
                    location.reload()
                    alertService.success('Status Updated Successfully', options)
                }
                else{
                    alertService.error('Something went wrong', options)
                }
            });
    }
    const changeMandetory = async (event, param) => {
   
        var data = {
            'id' : param,
            'mandatory' : event.target.checked
        }
        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateQuestionMandatory", {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                
                
                if(res.success)
                {
                    location.reload()
                    // alertService.success('Status Updated Successfully', options)
                }
                else{
                    alertService.error('Something went wrong', options)
                }
            });
    }
    const onInputChange = e => {

        setSurvey({ ...survey, [e.target.name]: e.target.value });
        
    };

    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        
        if(!survey["surveyName"]){
        formIsValid = false;
        errors["surveyName"] = "Survey name Cannot be empty";
        }
        
        if(survey["groupIds"].length==0){
            formIsValid = false;
            errors["groupIds"] = "Please Add Atleast 1 group";
            }         
    
        setError(errors);
        return formIsValid;
    }


    const onSubmit = async e => {
        e.preventDefault();
        console.log(survey)
        setLoading(true);
        if(handleValidation()){
        //    console.log(survey)
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/survey/update", {
            method: "PUT",
            body: JSON.stringify(survey),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    location.reload()
                    alertService.success('Survey Updated Successfully', options)
                }
                else{
                    alertService.error('Something went wrong', options)
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };

      const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="survey" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
               
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}} >
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Update Survey</h4>
                            </div>
                                <Header />
                        </div>
                    </nav>
                    

                    <div className="container-xxl flex-grow-1 container-p-y">
                    <Alert />
                        <div className="card">
                            <div className="card-header">
                                <div className="d-flex align-items-center justify-content-between">
                                    <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                                        <label htmlFor="defaultFormControlInput" className="form-label">Survey Name</label>
                                        {(session?.user.role == 0) ?
                                        <input type="text" name="surveyName" value={survey.surveyName} className="form-control" id="defaultFormControlInput" placeholder="Survey A" aria-describedby="defaultFormControlHelp" onChange={e => onInputChange(e)}/>
                                        :
                                        <input type="text" name="surveyName" value={survey.surveyName} className="form-control" id="defaultFormControlInput" placeholder="Survey A" aria-describedby="defaultFormControlHelp"/>
                                        }
                                        <span className="error" style={{ color: "red" }}>{errors['surveyName']}</span>
                                    </div>

                                    <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-end">
                                    <span className="error" style={{ color: "red",marginRight:'23px' }}>{errors['groupIds']}</span>
                                    <a href="/survey" style={{marginRight:'20px'}}> <span className="tf-icons bx bx bx-detail"></span>&nbsp;Back To List</a>
                                    {(session?.user.role == 0) ?
                                        <a type="button" className="btn btn-primary" onClick={() => setShowModal(true)}>
                                            <BiPlusMedical />&nbsp; Add New Group
                                        </a>
                                        :
                                        <></>
                                    }
                                    </div>
                                </div>
                                
                            </div>
                            {groupList.map((result,key, row) => {
                                            return (
                                                <div key={key}>
                                                <div className="container-fluid mb-2">
                                                    <div className="card-header bg-themePx">
                                                        <div className="d-flex align-items-center justify-content-between">
                                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                                                                <h3 className="sub-heading m-0 p-0">{result.groupName} 
                                                                {(session?.user.role == 0) ?
                                                                <a onClick={event => openEditModal(event, result.groupId)} style={{cursor:"pointer"}}><i className="bx bx-edit-alt"></i>
                                                                        </a>
                                                                        :
                                                                        <></>
                                                                }
                                                                </h3>
                                                                
                                                            </div>
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-end">
                                                            {(session?.user.role == 0) ?
                                                                <a type="button" className="btn btn-primary" onClick={event => openQstModal(event, result.groupId)}>
                                                                    <BiPlusMedical />&nbsp; Add Question
                                                                </a>
                                                                :
                                                                <></>
                                                            }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="table-responsive text-nowrap mb-4">
                                                    <div className="container-fluid">
                                                        <table className="table RgTable RgTableSimple">
                                                            <thead className="RgBGTable">
                                                                <tr>
                                                                    <th>Mandatory</th>
                                                                    <th>Question Text</th>
                                                                    <th>Category</th>
                                                                    <th>Type</th>
                                                                    {(session?.user.role == 0) ?
                                                                    <>
                                                                    <th>Edit</th>
                                                                    <th>Delete</th>
                                                                    <th>Active</th>
                                                                    </>
                                                                    :
                                                                    <></>
                                                                    }
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            {(result.surveygroup_questions).map((res,key,row) => {
                                                                return (
                                                                <tr>
                                                                    {(session?.user.role == 0) ?
                                                                    <td><input className="form-check-input" type="checkbox" checked={res.mandatory} onChange={event => changeMandetory(event, res.id)} /></td>
                                                                    :
                                                                    <td><input className="form-check-input" type="checkbox" checked={res.mandatory} disabled/></td>
                                                            }
                                                                    <td>{res.question}</td>
                                                                    <td>{res.bracket}</td>
                                                                    <td>{res.questionType}</td>
                                                                    {(session?.user.role == 0) ?
                                                                    <>
                                                                    <td>
                                                                        <a onClick={event => openQstEditModal(event, res.id)} className="icon icon-sm icon-primary" style={{cursor:"pointer"}}><i className="bx bx-edit-alt"></i>
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                        
                                                                        <a className="icon icon-sm icon-danger" onClick={event => handleDelete(event, res.id)} style={{cursor:"pointer"}}><i className="bx bx-trash"></i>
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                        <div className="form-check form-switch">
                                                                            <input className="form-check-input" type="checkbox" checked={res.status} id="flexSwitchCheckDefault" onChange={event => changeStatus(event, res.id)}/>
                                                                        </div>
                                                                    </td>
                                                                    </>
                                                                    :
                                                                    <></>
                                                                    }
                                                                </tr>
                                                                )})}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                </div>
                                                )})}
                                            

                            <div className="container-fluid">
                                <div className="row">
                                {(session?.user.role == 0) ?
                                    <div className="col-lg-12 col-md-6 col-sm-6 col-xs-6 text-end py-4">
                                    {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Save</button>}
                                    {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                                    </div>
                                    :
                                    <></>
                                }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        {showModal && <AddGroup surveyId={sid[2]} onClose={groupChildCall}/>}
                        {qstModal && <AddQuestion group_id = {grpId} onClose={qstChildCall}/>}
                        {editModal && <EditGroup group_id = {editId} onClose={editchildCall}/>}
                        {editqstModal && <EditQuestion group_id = {grpId} question_id = {qstId} onClose={editqstchildCall}/>}
                    </div>
                </div>
            </div>
        </div>
        {/*Below we add the modal wrapper*/}
        <div id="modal-root"></div>
    </div>
  )
}

export default page