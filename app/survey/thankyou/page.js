
"use client"


import { usePathname } from "next/navigation";
import { useState } from "react";
import { useEffect } from "react";
import { useSession } from "next-auth/react";

const page = () => {
    const { data: session } = useSession()
    
   
    const[questions, setQuestions] = useState([]);
    const[currentQuesIndex, setcurrentQuesIndex] = useState(0);
    const[currentQuesCategory, setcurrentQuesCategory] = useState('');

    

    return (
        <div className="">
            <section className="app-header">
                <a href="SuperAdmin-Dashboard.php">
                    <div className="logo">
                        <img src="/assets/img/pixel-logo.png" alt=""/>
                    </div>
                </a>
            </section>
            <div className="container-xxl container-p-y">
                <div class="misc-wrapper">
                    <form id="formAuthentication" action="#" method="POST">
                        <div class="mb-5 text-left">
                            <h2>Thank You! Your Survey is Submitted Successfully.</h2>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    )
}

export default page