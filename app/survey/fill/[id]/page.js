
"use client"
import Header from "@components/Header"
import FreeText from "@components/Questions/FreeText";
import LikertScale from "@components/Questions/LikertScale";
import MultipleChoice from "@components/Questions/MultipleChoice";
import MultipleImageChoice from "@components/Questions/MultipleImageChoice";
import SingleChoice from "@components/Questions/SingleChoice";
import SingleImageChoice from "@components/Questions/SingleImageChoice";
import Sidebar from "@components/Sidebar"
import { usePathname,useRouter } from "next/navigation";
import { useState } from "react";
import { useEffect } from "react";
import { useSession } from "next-auth/react";

const page = () => {
    const { data: session } = useSession()
    const router = useRouter()
    const pathname = usePathname()
    const sid = pathname.split('/')
    const cid = sid[3].split('-')
    const[questions, setQuestions] = useState([]);
    const[currentQuesIndex, setcurrentQuesIndex] = useState(0);
    const[currentQuesCategory, setcurrentQuesCategory] = useState('');
    const [errors, setError] = useState({
        errors: "",
      });

    useEffect(() => {
        fetchData()        
    }, []);
    

    useEffect(() => {
        getCurrentQuestCategory(currentQuesIndex)
    }, [currentQuesIndex, currentQuesCategory])

    const fetchData = async () => {
        let res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSurveyQuestionsById?id="+cid[0], {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
                cache: 'no-store'
            })

        let survey = await res.json()
        if(survey && survey.output.questions){
            setQuestions(survey.output.questions)
            setcurrentQuesCategory(survey.output.questions[0].questionType)
        }        
        
    }

    const handleBack = () => {
        if(currentQuesIndex > 0){
            setcurrentQuesIndex(currentQuesIndex - 1)
        }
        // call api for answer save

        let errors = {};
        setError(errors)
    }

    const handleNext = async () => {

        let errors = {};

        
        if(session?.user.role == 3)
        {

            if(questions[currentQuesIndex].hasOwnProperty('answer'))
            {
                var res = false
                if(questions[currentQuesIndex].questionType == 'Likert Scale')
                var res = true
                if(questions[currentQuesIndex].answer != "")
                var res = true
                    
                if(res)
                {
                    if(questions.length - 1 > currentQuesIndex){
        
                        var survey={
                            studentId: session?.user.id,
                            surveyId: cid[0],
                            status: 0,
                            studentCheckinSurveyId:cid[1],
                            questions: questions
                        }
                    }
                    else{
                        var survey={
                            studentId: session?.user.id,
                            surveyId: cid[0],
                            status: 1,
                            studentCheckinSurveyId:cid[1],
                            questions: questions
                        }
                    }
        
        
                    // call api for answer save
                
                    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/addSurveyAnswer", {
                        method: "POST",
                        body: JSON.stringify(survey),
                        headers: {
                            "Content-Type": "application/json",
                        }
                        }).then(res => res.json())
                        .then(res => {
                            
                            
                            if(res.success && survey.status == 1)
                            {
                                router.push('/survey/thankyou')
                            }
                            
                        });

                    if(questions.length - 1 > currentQuesIndex){
                        setcurrentQuesIndex(currentQuesIndex + 1)
                        let errors = {};
                        setError(errors)
                    }
                } 
                else{
                
                    errors["answer"] = "Please select answer";
                    setError(errors);
                }
                
            }
            else{
                
                errors["answer"] = "Please select answer";
                setError(errors);
            }
            
        }
        else{
            if(questions.length - 1 > currentQuesIndex){
                setcurrentQuesIndex(currentQuesIndex + 1)
            }
        }
        
    }

    const getCurrentQuestCategory = (index) => {
        let temp = questions[index]
        // console.log("question",temp);
        if(temp){            
            setcurrentQuesCategory(temp.questionType)
        }
        
    }

    return (
        <div className="">
            <section className="app-header">
                <a href="SuperAdmin-Dashboard.php">
                    <div className="logo">
                        <img src="/assets/img/pixel-logo.png" alt=""/>
                    </div>
                </a>
            </section>
            <div className="container-xxl container-p-y">
                <div className="misc-wrapper">
                    <form className="w-100" id="formAuthentication" action="#" method="POST">
                    
                        {
                            currentQuesCategory === 'Likert Scale' ? 
                                <LikertScale quesIndex={currentQuesIndex} questions={questions} setQuestions={setQuestions}/> : 
                                currentQuesCategory === 'Multiple Choice' ?
                                <MultipleChoice quesIndex={currentQuesIndex} questions={questions} setQuestions={setQuestions}/> : 
                                currentQuesCategory === 'Single Choice' ?
                                <SingleChoice quesIndex={currentQuesIndex} questions={questions} setQuestions={setQuestions}/> : 
                                currentQuesCategory === 'Single Image Choice' ?
                                <SingleImageChoice quesIndex={currentQuesIndex} questions={questions} setQuestions={setQuestions}/> :
                                currentQuesCategory === 'Multiple Image Choice' ?
                                <MultipleImageChoice quesIndex={currentQuesIndex} questions={questions} setQuestions={setQuestions}/> :
                                currentQuesCategory === 'Free Text' ?
                                <FreeText quesIndex={currentQuesIndex} questions={questions} setQuestions={setQuestions}/> : ''
                        }
                        <span className="error" style={{ color: "red" }}>{errors['answer']}</span>
                        {(questions.length) ?
                        <div className="d-flex justify-content-between pt-5">
                            
                            {(currentQuesIndex > 0) ?
                            <button type="button" className="btn btn-outline-secondary" onClick={handleBack}><span className="bi bi-chevron-left"></span>&nbsp; Back </button>
                            :
                            <>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>
                            }
                            {(questions.length - 1 > currentQuesIndex) ?
                            <button type="button" className="btn btn-primary" onClick={handleNext}> Next &nbsp; <span className="bi bi-chevron-right"></span></button>
                            :
                            (session?.user.role == 3) ?
                            <button type="button" className="btn btn-primary" onClick={handleNext}> Submit &nbsp; <span className="bi bi-chevron-right"></span></button>
                            :
                            <></>
                            }
                        </div>
                        :
                        <></>
                        }
                    </form>
                </div>
            </div>
        </div>
    )
}

export default page