"use client"
import { useEffect, useState } from "react";
import { BiPlusMedical } from "react-icons/bi";
import { BsFillEyeFill } from "react-icons/bs";
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { alertService } from 'services';
import Alert  from '@components/Alert';
export const dynamic = 'force-dynamic'
import { useSession } from "next-auth/react";

const page = () => {
    const { data: session,update } = useSession()
    const[surveyList,setSurveyList] = useState([]);
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });
    
    useEffect(() => {
        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
            });
        }
    
        callTable();
        
    },[surveyList]);

    useEffect(() => {
    
        async function fetchData()
            {
    
                await import('datatables.net-dt/js/dataTables.dataTables')
                await import('datatables.net-dt/css/jquery.dataTables.min.css')
                window.JSZip = jzip;

                if(session?.user.role == 0)
                var url = "/getSurveyList"
                else
                var url = "/getSurveyListBySchoolId?id="+session?.user.id

                const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+url, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    cache: 'no-store'
                    }).then(res => res.json())
                    .then(surveyList => {
                        if(surveyList.output.length > 0)
                        {
                            $('#RgTableX').DataTable().destroy();
                            setSurveyList(surveyList.output)
                        }
                            
                            
                    });
    
            }
            if(session)
            {
                fetchData();
            }
            
        
      }, [session]);


      const changeStatus = async (event, param) => {
   
        var data = {
            'id' : param,
            'status' : event.target.checked
        }
        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateSurveyStatus", {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                
                
                if(res.success)
                {
                    location.reload()
                    alertService.success('Status Updated Successfully', options)
                }
                else{
                    alertService.error('Something went wrong', options)
                }
            });
      }

    const handleDelete = async(event, param) => {
        if (confirm("Do you really want to delete this data.")) {

            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/survey/delete/"+param, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                if(res.success)
                {
                    location.reload()
                    alertService.success('Survey deleted successfully', options)
                }
                else{
                    alertService.error('Something went wrong', options)
                }
            });
        }
    }

    const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="survey" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">Surveys</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                    <Alert />
                        <div className="card">
                            <div className="card-header align-items-center d-flex justify-content-between">
                                <a type="button" >
                                    {/* <span className="bx bxs-download"></span>&nbsp; Export */}
                                </a>
                                {(session?.user.role == 0) ?
                                <a href="/addSurvey" className="btn btn-primary"> <span className="tf-icons bx bx bxs-user-plus"></span>&nbsp; Add Survey</a>
                                :
                                <></>
                                }
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                    {(session) ?
                                    <table id="RgTableX" className="table RgTable" style={{width:'100%'}}>
                                        <thead className="RgBGTable">
                                            <tr>
                                                <th>Survey Name</th>
                                                <th>Total Questions</th>
                                                <th>Mandatory Questions</th>
                                                <th>Created On</th>
                                                {/* <th>Total Check-ins</th> */}
                                                <th>Surveys sent</th>
                                                <th>Surveys Answered</th>
                                                {(session?.user.role == 0) ?
                                                <>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </>
                                                :
                                                <>
                                                    <th>Action</th>
                                                    <th></th>
                                                </>
                                                }
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {(surveyList.length > 0) ?
                                        surveyList.map((result,key, row) => {
                                            return (
                                                <tr key={key}>
                                                <td><a href={"/survey/"+result.id} style={{cursor:"pointer"}}>{result.surveyName}</a></td>
                                                <td>{result.questionCount}</td>
                                                <td>{result.mandatoryQuestionCount}</td>
                                                <td>{result.createdOn}</td>
                                                {/* <td>{result.checkIns}</td> */}
                                                <td>{result.surveyCirculation}</td>
                                                <td>{result.answerCirculation}</td>
                                                {(session?.user.role == 0) ?
                                                <>
                                                    <td>
                                                        <div className="form-check form-switch mb-2">
                                                            <input className="form-check-input" type="checkbox" id="flexSwitchCheckDefault" checked={result.status}  onChange={event => changeStatus(event, result.id)}/>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href={"/survey/fill/"+result.id} style={{cursor:"pointer"}} title="view Detail"  target="_blank"><BsFillEyeFill /></a>
                                                        <a onClick={event => handleDelete(event, result.id)} style={{cursor:"pointer"}}><i className="bx bx-trash me-1"></i>
                                                                    </a>
                                                    </td>
                                                </>
                                                :
                                                <>
                                                <td>
                                                    {/* <a href={"/survey/"+result.id} style={{cursor:"pointer"}} title="view Detail"><BsFillEyeFill /></a> */}
                                                    <a href={"/survey/fill/"+result.id} style={{cursor:"pointer"}} title="view Detail"  target="_blank"><BsFillEyeFill /></a>
                                                    
                                                </td>
                                                <td></td>
                                                </>
                                                }
                                                
                                            </tr>
                                             )})
                                            :
                                            <></>
                                            }
                                            
                                        </tbody>
                                    </table>
                                    :
                                    <></>
                                    }
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
  )
}

export default page