import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useFormik } from 'formik';



const EditModal = ({ onClose, children, title,grade_id }) => {
    const [isLoading, setLoading] = useState(false);
    const[surveyList,SetSurveyList] = useState([]);
    const [grade, setGrade] = useState({
        id:"",
        gradeName: "",
        surveyId:0
      });
      const [errors, setError] = useState({
        errors: "",
      });

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    useEffect(() => {
        
        async function fetchData()
        {

            const resschool = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getGradeById?id="+grade_id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store' 
                }).then(res => res.json())
                .then(grades => {
                    
                // console.log(res)
                setGrade(grades.gradeList)
             });


             const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSurveyList", {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    cache: 'no-store'
                    }).then(res => res.json())
                    .then(surveyList => {
                        
                    // console.log(res.surveyList)
                    SetSurveyList(surveyList.output)
                    // console.log(surveyList)
                });
        }
        fetchData();    
    },[]);

   

    const onInputChange = e => {
        setGrade({ ...grade, [e.target.name]: e.target.value });
    };

    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        console.log(grade);
        //username
        if(!grade["gradeName"]){
        formIsValid = false;
        errors["gradeName"] = "Grade Cannot be empty";
        } 
               
    
        setError(errors);
        return formIsValid;
    }


    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/grade/update", {
            method: "PUT",
            body: JSON.stringify(grade),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success");
                }
                else{
                    onClose(res.message);
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };

    

    const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Edit Grade</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                        <form>
                            <div className="mb-3">
                                <label for="defaultFormControlInput" className="form-label">Grade Name</label>
                                <input type="hidden" name="id" value={grade.id}/>
                                <input type="text" placeholder="Grade" className="form-control" name="gradeName" value={grade.gradeName} onChange={e => onInputChange(e)}/>
                                 <span className="error" style={{ color: "red" }}>{errors['gradeName']}</span>
                            </div>
                            {/* <div className="mb-3">
                                <label for="defaultSelect" className="form-label">Assign Survey</label>
                                <select id="defaultSelect" class="form-select" name="surveyId" value={grade.surveyId} onChange={e => onInputChange(e)}>
                                <option>Select Survey</option>
                                    {surveyList.map((result,key) => {
                                    return (
                                        <option key={key} value={result.id}>{result.surveyName}</option>
                                    )})}
                                </select>
                            </div> */}
                            
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
            {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
};

export default EditModal