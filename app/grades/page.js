"use client"
import { useEffect, useState } from "react";

import $ from "jquery";
import jzip from 'jzip';
import { BiPlusMedical } from "react-icons/bi";

import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import Modal from "./addModal";
import EditModal from "./editModal";
import { alertService } from 'services';
import Alert  from '@components/Alert';


// Initialize jquery and Datatable
// 

const grade = () => {
    const [showModal, setShowModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const[gradeList,SetGradeList] = useState([]);
    const[report,setReport] = useState([]);
    const[editId,setId] = useState(false);
    
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });

    useEffect(() => {

        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
              
            });
        }
    
        callTable();
    
        
    },[gradeList]);

  useEffect(() => {
    
    async function fetchData()
        {

            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
            
            const result = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getGradeList", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(res => {
                    
                // console.log(res)
                if(res.output.length>0)
                {
                    $('#RgTableX').DataTable().destroy();
                    SetGradeList(res.output)
                }
               
            });
        }
        fetchData();
    
  }, []);

  

  const childCall = (msg) =>{

    setShowModal(false)
    if(msg == 'success')
    {   
        location.reload();
        alertService.success('Grade Added Successfully', options)
    }
    if(msg == 'error')
    alertService.error('Something went wrong', options)

  } 


  const openEditModal = (event, param) => {
    setId(param);
    setEditModal(true);
  }
  const editchildCall = (msg) =>{

    setEditModal(false)
   
    if(msg == 'success')
    {   location.reload();
        alertService.success('Grade Updated Successfully', options)
    }
    if(msg == 'error')
    {
        alertService.error(msg, options)
    }
    

  }

  const handleDelete = async(event, param) => {
    if (confirm("Do you really want to delete this data.")) {

        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/grade/delete/"+param, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store'
        }).then(res => res.json())
        .then(res => {
            if(res.success)
            {
                alertService.success('Grade deleted successfully', options)
                location.reload()
            }
            else{
                alertService.error(res.message, options)
            }
        });
    }
  }

  const onInputChange = e => {
    setReport([...report, e.target.value ]);
};

const generateReport = async () => {

    let abc = {
        id : report
    }

    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/getGradeListInSheet', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store',
        body: JSON.stringify(abc)
        }).then(res => res.json())
        .then(res => {
            if(res.success)
            {
                let alink = document.createElement('a');
                alink.href = res.fileurl;
                alink.download = 'report.xlsx';
                alink.click();
            }
        });
}

const toggleMenu = () => {

    setLeft('layout-menu-expanded')
    
}
const menuCall = () => {

    setLeft(null)
    
}

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="grades" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">Grades</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                    <Alert />
                        <div className="card">
                            <div className="card-header">
                                <div className="d-flex align-items-center justify-content-between">
                                    <a type="button" className="btn btn-theme btn-outline-theme" onClick={generateReport}>
                                        <span className="bx bxs-download"></span>&nbsp; Export
                                    </a>
                                    <a type="button" className="btn btn-primary" onClick={() => setShowModal(true)}>
                                        <BiPlusMedical />&nbsp; Add Grade
                                    </a>
                                </div>
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                
                                <table id="RgTableX" className="table RgTable" style={{width: '100%'}}>
                                        <thead className="RgBGTable">
                                        <tr>
                                            <th></th>
                                            <th>Grade Name</th>
                                           
                                            <th>Operational in Schools</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {(gradeList.length > 0) ?
                                        gradeList.map((result,key, row) => {
                                            return (
                                                <tr key={key}>
                                                <td><input className="form-check-input" type="checkbox" onChange={e => onInputChange(e)} value={result.id} id="flexCheckDefault"/></td>
                                                <td>{result.gradeName}</td>
                                                
                                                <td><a href={"/grades/"+result.id} style={{cursor:"pointer"}}>{result.schoolCount}</a></td>
                                               
                                                <td>
                                                    <a onClick={event => openEditModal(event, result.id)} style={{cursor:"pointer"}}><i className="bx bx-edit-alt me-1"></i>
                                                               </a>
                                                    <a onClick={event => handleDelete(event, result.id)} style={{cursor:"pointer"}}><i className="bx bx-trash me-1"></i>
                                                               </a>
                                                </td>
                                            </tr>
                                            )})
                                        :
                                        <></>
                                        }                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div>
                    {showModal &&
            <Modal onClose={childCall}/>
                
        }
         {editModal &&
            <EditModal grade_id = {editId} onClose={editchildCall}/>
                
        }
        </div>
                </div>
            </div>
        </div>
        {/*Below we add the modal wrapper*/}
        <div id="modal-root"></div>
    </div>

    
  )
}

export default grade