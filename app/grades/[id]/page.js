"use client"
import { useEffect, useState } from "react";

import { BiPlusMedical } from "react-icons/bi";
import { usePathname } from 'next/navigation'
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';

import { alertService } from 'services';
import Alert  from '@components/Alert';
export const dynamic = 'force-dynamic'
import { useSession } from "next-auth/react";

// Initialize jquery and Datatable
// 

const school = () => {
    const { data: session } = useSession()
    const pathname = usePathname()
    const[schoolList,setSchoolList] = useState([]);
    const[grade,setGrade] = useState('');
    const[leftSide,setLeft] = useState(null);
    
    
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });
  

    const sid = pathname.split('/')

    useEffect(() => {
        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
            });
        }
    
        callTable();
        
    },[schoolList]);

  useEffect(() => {
    
    async function fetchData()
        {

            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            window.JSZip = jzip;
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSchoolsByGrade?id="+sid[2], {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(schoolList => {
                        $('#RgTableX').DataTable().destroy();
                        // console.log(schoolList)
                        setSchoolList(schoolList.output)
                        
                });

                const resschool = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getGradeById?id="+sid[2], {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    cache: 'no-store' 
                    }).then(res => res.json())
                    .then(grades => {
                        
                    // console.log(res)
                    setGrade(grades.gradeList.gradeName)
                 });

            
        }
        fetchData();
    
  }, []);

 
  const toggleMenu = () => {

    setLeft('layout-menu-expanded')
    
}
const menuCall = () => {

    setLeft(null)
    
}


  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="grades" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">Schools By Grade: {grade}</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                    <Alert />
                        <div className="card">
                            <div className="card-header">
                                <div className="row d-flex align-items-center">
                                    
                                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-end">
                                        
                                    <a href="/grades" style={{marginRight:'20px'}}> <span className="tf-icons bx bx bx-detail"></span>&nbsp;Back To List</a>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                
                                <table id="RgTableX" className="table RgTable" style={{width: '100%'}}>
                                        <thead className="RgBGTable">
                                            <tr>
                                                
                                                <th>School Name</th>
                                                <th>No.of Classes</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {schoolList.map((result,key, row) => {
                                            return (
                                                <tr key={key}>
                                               
                                                <td>{result.fullName}</td>
                                                <td>{result.classes}</td>
                                                
                                               
                                            </tr>
                                            )})}
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                 
                </div>
            </div>
        </div>
        
    </div>

    
  )
}

export default school