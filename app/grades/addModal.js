import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useFormik } from 'formik';



const Modal = ({ onClose, children, title }) => {
    const [isLoading, setLoading] = useState(false);
    const[surveyList,SetSurveyList] = useState([]);
    const [grade, setGrade] = useState({
        gradeName: "",
        surveyId: 0
      });
      const [errors, setError] = useState({
        errors: "",
      });

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };


    const onInputChange = e => {
        setGrade({ ...grade, [e.target.name]: e.target.value });
    };

    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        console.log(grade);
        //username
        if(!grade["gradeName"]){
        formIsValid = false;
        errors["gradeName"] = "Grade Cannot be empty";
        } 
        // if(!grade["surveyId"]){
        // formIsValid = false;
        // errors["surveyId"] = "Please select Survey";
        // }         
    
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/grade/add", {
            method: "POST",
            body: JSON.stringify(grade),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success");
                }
                else{
                    onClose("error");
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };

    

    const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Add Grade</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                        <form>
                            <div className="mb-3">
                                <label for="defaultFormControlInput" className="form-label">Grade Name</label>
                                <input type="text" placeholder="Grade" className="form-control" name="gradeName" onChange={e => onInputChange(e)}/>
                                <span className="error" style={{ color: "red" }}>{errors['gradeName']}
                                </span>
                            </div>
                            
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
            {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
};

export default Modal