"use client"
import { useEffect, useState } from "react";
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { BiPlusMedical } from "react-icons/bi";
import { BsFillEyeFill } from "react-icons/bs";
import Modal from "./addModal";
import EditModal from "./editModal";
import ReadModal from "./readModal";
import { alertService } from 'services';
import Alert  from '@components/Alert';
import { useSession } from "next-auth/react";


// Initialize jquery and Datatable
// 

const lessons = () => {
    const { data: session,update } = useSession()
    const [showModal, setShowModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const [readModal, setReadModal] = useState(false);
    const[gradeName,setGradeName] = useState(0);
    const[gradeData,SetGradeData] = useState([]);
    const[lessonList,setLessonList] = useState([]);
    const[editId,setId] = useState(false);   
    const[readId,setReadId] = useState(false);    
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });

    useEffect(() => {

        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": true,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
            });
        }
    
        callTable();
    
        
    },[lessonList]);

  useEffect(() => {
    
    async function fetchData()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;

            if(session?.user.role == 0)
            {
                var url = "/getLessons"
                var gurl = "/getFullGrade"
            }
            else
            {
                var url = "/getLessonsBySchoolId?id="+session?.user.id
                var gurl = "/getFullGradeBySchoolId?id="+session?.user.id
            }
            
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+url, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(lessonList => {

                    if(lessonList.output.length > 0)
                    {
                        $('#RgTableX').DataTable().destroy();
                        setLessonList(lessonList.output)
                    }
                        
                });

            const result = await fetch(process.env.NEXT_PUBLIC_APP_URL+gurl, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(gradeList => {
                    
                // console.log(gradeList)
                SetGradeData(gradeList.output)
            });
        }
        if(session && gradeName == 0)
        {
            fetchData();
        }

  }, [session]);

  

  const childCall = (msg) =>{

    setShowModal(false)
    if(msg == 'success')
    {   
        location.reload();
        alertService.success('Lesson Added Successfully', options)
    }
    if(msg == 'error')
    alertService.error('Something went wrong', options)

  } 

  
  const openEditModal = (event, param) => {
    setId(param);
   
    setEditModal(true);
  }
  const openReadModal = (event, param) => {
    setReadId(param);
    setReadModal(true);
  }
  const editchildCall = (msg) =>{

    setEditModal(false)
    
    if(msg == 'success')
    {   
        location.reload();
        alertService.success('Lesson Updated Successfully', options)
    }
    if(msg == 'error')
    {
        location.reload();
        alertService.error('Something went wrong', options)
    }

  }
  const readchildCall = () =>{
    setReadModal(false)
  }
  const changeStatus = async (event, param) => {
   
    var data = {
        'id' : param,
        'status' : event.target.checked
    }
    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateLessonStatus", {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store'
        }).then(res => res.json())
        .then(res => {
            
            
            if(res.success)
            {
                location.reload()
                alertService.success('Status Updated Successfully', options)
            }
            else{
                alertService.error('Something went wrong', options)
            }
        });
  }

  const handleDelete = async(event, param) => {
    if (confirm("Do you really want to delete this data.")) {

        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/lesson/delete/"+param, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store'
        }).then(res => res.json())
        .then(res => {
            if(res.success)
            {
                location.reload()
                alertService.success('Lesson deleted successfully', options)
            }
            else{
                alertService.error(res.message, options)
            }
        });
    }
  }

  const getFilter = async(e) => {
   
    setGradeName(e.target.value)

    if(session?.user.role == 0)
    {
        var url = process.env.NEXT_PUBLIC_APP_URL+'/getLessonsByGrade?id='+e.target.value
    }
    else
    {
        var url = process.env.NEXT_PUBLIC_APP_URL+'/getLessonsByGrade?id='+e.target.value+'&sid='+session?.user.id
    }
    
    
    if(url != '')
    {
        const res = await fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                if(res.success)
                {
                    $('#RgTableX').DataTable().destroy();
                    setLessonList(res.output)
                }
            });
    }
   
  }

  const toggleMenu = () => {

    setLeft('layout-menu-expanded')
    
}
const menuCall = () => {

    setLeft(null)
    
}


  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="lessons" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">Lessons</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                        <Alert />
                        <div className="card">
                            <div className="card-header">
                                <div className="d-flex align-items-center justify-content-between">
                                    <div className="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-Rg-3">
                                        <label htmlFor="defaultSelect" className="form-label">Filter By</label>
                                        <select id="defaultSelect" name="gradeName" className="form-select" onChange={e => getFilter(e)} value={gradeName}>
                                            <option value="0">Select Grade</option>
                                            {(gradeData.length > 0)?
                                            gradeData.map((result,key) => {
                                                return (
                                            <option key={key} value={result.gradeId}>{result.gradename}</option>
                                                                )})
                                                            :
                                                            <></>}
                                        </select>
                                    </div>
                                    
                                    <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-end">
                                    {(session?.user.role == 0) ?
                                    <a type="button" className="btn btn-primary" onClick={() => setShowModal(true)}>
                                        <BiPlusMedical />&nbsp; Add Lesson
                                    </a>
                                    :
                                    <></>
                                    }
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                    {(session)?
                                    <table id="RgTableX" className="table RgTable" style={{width: "100%"}}>
                                        <thead className="RgBGTable">
                                            <tr>
                                                <th>Lesson Name</th>
                                                <th>Grade Level</th>
                                                <th>Timing</th>
                                                <th>Description</th>
                                                <th>Lesson Plan</th>
                                                <th>Slide Deck</th>
                                                <th>Status</th>
                                                {(session?.user.role == 0) ?
                                                <th>Action</th>
                                                :
                                                <th></th>
                                                }
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {(lessonList.length > 0)?
                                        lessonList.map((result,key, row) => {
                                            return (
                                                <tr key={key}>
                                                <td>{result.lessonName}</td>
                                                <td>{result.lessonGrades}</td>
                                                <td>{result.timing} minutes</td>
                                                
                                                <td>
                                                    <p className="ReadMore">{result.description.slice(0, 30)}
                                                    {(result.description.length > 30) ?
                                                    <>
                                                        ....<a  className="card-link" onClick={event => openReadModal(event, result.id)} style={{cursor:'pointer'}}> Read more </a>
                                                    </>
                                                    :
                                                    <></>
                                                    }
                                                    </p>
                                                </td>
                                                
                                                
                                                <td><a href={result.lessonUrl} target="_blank"><BsFillEyeFill /></a> </td>
                                                <td><a href={result.resourceUrl} target="_blank"><BsFillEyeFill /> </a></td>
                                                <td>
                                                {(session?.user.role == 0) ?
                                                    <div className="form-check form-switch mb-2">
                                                        <input className="form-check-input" type="checkbox" id="flexSwitchCheckDefault" checked={result.status} onChange={event => changeStatus(event, result.id)}/>
                                                    </div>
                                                    :
                                                    <div className="form-check form-switch mb-2">
                                                        <input className="form-check-input" type="checkbox" id="flexSwitchCheckDefault" checked={result.status} disabled/>
                                                    </div>
                                                }
                                                </td>
                                                {(session?.user.role == 0) ?
                                                <td>
                                                    <a onClick={event => openEditModal(event, result.id)} style={{cursor:"pointer"}}><i className="bx bx-edit-alt me-1"></i>
                                                                </a>
                                                    <a onClick={event => handleDelete(event, result.id)} style={{cursor:"pointer"}}><i className="bx bx-trash me-1"></i>
                                                                </a>
                                                </td>
                                                :
                                                <td></td>
                                                }
                                            </tr>
                                            )})
                                        :
                                        <></>}
                                        </tbody>
                                    </table>
                                    :
                                    <></>
                                    }
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div>
                    {showModal &&
                        <Modal onClose={childCall}/>       
                    }
                    {editModal &&
                        <EditModal lesson_id = {editId} onClose={editchildCall}/>      
                    }
                    {readModal &&
                        <ReadModal lesson_id = {readId} onClose={readchildCall}/>      
                    }
        </div>
                </div>
            </div>
        </div>
        {/*Below we add the modal wrapper*/}
        <div id="modal-root"></div>
    </div>

    
  )
}

export default lessons