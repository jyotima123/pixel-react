import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useFormik } from 'formik';
import Select from "react-select";
import { useSession } from "next-auth/react";



const Modal = ({ onClose, children, title }) => {
    const { data: session } = useSession()
    const [isLoading, setLoading] = useState(false);
    const[gradeData,SetGradeData] = useState([]);
    const[gradeId,setgradeId] = useState();
    const [lesson, setLesson] = useState({
        lessonName: "",
        createdBy:0,
        timing: "",  
        cover:"",
        description: "",
        lessonUrl:"",
        lessonPdf:"",
        resourceUrl:"",
        resourcePdf:"",
        status:1,
        lessonGrade:[]
      });
      const [errors, setError] = useState({
        errors: "",
      });
        

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    useEffect(() => {
        async function fetchData()
        {
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSelectFullGrade", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(gradeList => {
                    
                // console.log(res.gradeList)
                SetGradeData(gradeList)
            });

        }
        fetchData();    
    },[]);

    
    const onInputChange = e => {
        setLesson({ ...lesson, [e.target.name]: e.target.value });
    };

    const changegrade = e => {
        console.log(e)
        setLesson({ ...lesson, 'lessonGrade': e});
        setgradeId(e);
    };

    const onFileChange = async(e,type) => {
        
        if(type == 'cover')
        {
            // setLesson({ ...lesson, 'cover': e.target.files[0]});
            var ext = checkFileExtension(e.target.files[0].name)
            if(ext == 'png' || ext == 'jpeg' || ext == 'jpg')
            {
                setLesson({ ...lesson, 'cover': e.target.files[0]});
            }
            else{
                let errors = {};
                errors["cover"] = "Please upload only PNG,JPEG,JPG Files";
                setError(errors);
            }
        }
        if(type == 'lesson')
        {
            // console.log(e.target.files[0].name)
            var ext = checkFileExtension(e.target.files[0].name)
            console.log(ext)
            if(ext == 'pdf')
            {
                setLesson({ ...lesson, 'lessonPdf': e.target.files[0]});
            }
            else{
                let errors = {};
                errors["lessonPdf"] = "Please upload only PDF File";
                setError(errors);
            }
            
        }
        if(type == 'resource')
        {
            var ext = checkFileExtension(e.target.files[0].name)
            console.log(ext)
            if(ext == 'pdf')
            {
                setLesson({ ...lesson, 'resourcePdf': e.target.files[0]});
            }
            else{
                let errors = {};
                errors["resourcePdf"] = "Please upload only PDF File";
                setError(errors);
            }
        }
         
      }

      const checkFileExtension = (fileName) => {
        // fileName = document.querySelector('#file1').value;
        var extension = fileName.substring(fileName.lastIndexOf('.') + 1);
        return extension
    };

    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        console.log(lesson);
        //lessonname
        if(!lesson["lessonName"]){
        formIsValid = false;
        errors["lessonName"] = "Lesson name Cannot be empty";
        }
        //grades
        if(lesson["lessonGrade"].length == 0){
            formIsValid = false;
            errors["lessonGrade"] = "Please select atleast 1 grade";
        }
        if(!lesson["cover"]){
            formIsValid = false;
            errors["cover"] = "Please upload any cover image";
        }
        if(!lesson["description"]){
            formIsValid = false;
            errors["description"] = "Please fill description";
        }
        if(!lesson["lessonUrl"]){
            formIsValid = false;
            errors["lessonUrl"] = "Please give lesson URL";
        }
        if(!lesson["lessonPdf"]){
            formIsValid = false;
            errors["lessonPdf"] = "Please upload lesson PDF";
        }
        if(!lesson["resourceUrl"]){
            formIsValid = false;
            errors["resourceUrl"] = "Please provide ressource URL";
        }
        if(!lesson["resourcePdf"]){
            formIsValid = false;
            errors["resourcePdf"] = "Please upload resource PDF";
        }
    
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){

            const data = new FormData();
            data.append("lessonName",lesson.lessonName);
            data.append('createdBy',session?.user.id);
            data.append("timing",lesson.timing);  
            data.append("cover",lesson.cover);
            data.append("description",lesson.description);
            data.append("lessonUrl",lesson.lessonUrl);
            data.append("lessonPdf",lesson.lessonPdf);
            data.append("resourceUrl",lesson.resourceUrl);
            data.append("resourcePdf",lesson.resourcePdf);
            data.append("status",lesson.status);
            data.append("lessonGrade",JSON.stringify(lesson.lessonGrade));

            console.log(lesson)

            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/lesson/add", {
            method: "POST",
            body: data,
            // headers: {
            //     "Content-Type": "undefined",
            // }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success");
                }
                else{
                    onClose("error");
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };

      const customStyles = {
        control: base => ({
          ...base,
          height: 33,
          minHeight: 33,
          borderRadius: 5,
        })
      };

    

    const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Add Lesson</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                    <form>
                     <div className="row">
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label htmlFor="defaultFormControlInput" className="form-label">Lesson Name</label>
                                 <input type="text" name="lessonName" className="form-control" id="defaultFormControlInput" placeholder="lesson name" aria-describedby="defaultFormControlHelp" onChange={e => onInputChange(e)}/>
                             </div>
                             <span className="error" style={{ color: "red" }}>{errors['lessonName']}</span>
                         </div>
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label className="form-label" htmlFor="basic-icon-default-email">Assign Grade</label>
                                 <Select 
                                    options={gradeData}
                                    placeholder="Select Grade"
                                    value={gradeId}
                                    onChange={changegrade}
                                    isSearchable={true}
                                    isMulti
                                    className="basic-multi-select"
                                    classNamePrefix="select"
                                />
                             </div>
                             <span className="error" style={{ color: "red" }}>{errors['lessonGrade']}</span>
                         </div>
                     </div>
                     <div className="row">
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label className="form-label" htmlFor="basic-icon-default-email">Timing(in minutes)</label>
                                 <input className="form-control" name="timing" type="text" placeholder="Lesson Timing" onChange={e => onInputChange(e)}/>
                             </div>
                         </div>
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label className="form-label" htmlFor="basic-icon-default-phone">Upload Cover</label>
                                 <div className="input-group">
                                     <input type="file" name="cover" className="form-control" accept="image/png, image/jpeg" id="inputGroupFile02" onChange={e => onFileChange(e,'cover')}/>
                                     {/* <label className="input-group-text" htmlFor="inputGroupFile02">Upload</label> */}
                                 </div>
                             </div>
                             <span className="error" style={{ color: "red" }}>{errors['cover']}</span>
                         </div>
                     </div>
                     <div className="row">
                         <div className="col-md-12">
                             <div className="mb-3">
                                 <label htmlFor="exampleFormControlTextarea1" className="form-label">Description</label>
                                 <textarea className="form-control" id="exampleFormControlTextarea1" name="description" rows="3" spellCheck="false" onChange={e => onInputChange(e)}></textarea>
                             </div>
                             <span className="error" style={{ color: "red" }}>{errors['description']}</span>
                         </div>
                     </div>
                     <div className="row">
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label htmlFor="defaultFormControlInput" className="form-label">Lesson Plan URL</label>
                                 <input type="text" className="form-control" id="defaultFormControlInput" name="lessonUrl" placeholder="Lorem Ipsum" aria-describedby="defaultFormControlHelp" onChange={e => onInputChange(e)}/>
                             </div>
                             <span className="error" style={{ color: "red" }}>{errors['lessonUrl']}</span>
                         </div>
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label className="form-label" htmlFor="basic-icon-default-email">Upload Lesson Plan PDF</label>
                                 <div className="input-group">
                                     <input type="file" className="form-control" id="inputGroupFile02" name="lessonPdf" accept='application/pdf' onChange={e => onFileChange(e,'lesson')}/>
                                     {/* <label className="input-group-text" htmlFor="inputGroupFile02">Upload</label> */}
                                 </div>
                             </div>
                             <span className="error" style={{ color: "red" }}>{errors['lessonPdf']}</span>
                         </div>
                     </div>
                     <div className="row">
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label htmlFor="defaultFormControlInput" className="form-label">Resource Presentation URL</label>
                                 <input type="text" className="form-control" id="defaultFormControlInput" name="resourceUrl" placeholder="Lorem Ipsum" aria-describedby="defaultFormControlHelp" onChange={e => onInputChange(e)}/>
                             </div>
                             <span className="error" style={{ color: "red" }}>{errors['resourceUrl']}</span>
                         </div>
                         <div className="col-md-6">
                             <div className="mb-3">
                                 <label className="form-label" htmlFor="basic-icon-default-email">Resource Presentation PDF</label>
                                 <div className="input-group">
                                     <input type="file" className="form-control" id="inputGroupFile02" accept='application/pdf' name="resourcePdf" onChange={e => onFileChange(e,'resource')}/>
                                     {/* <label className="input-group-text" htmlFor="inputGroupFile02">Upload</label> */}
                                 </div>
                             </div>
                             <span className="error" style={{ color: "red" }}>{errors['resourcePdf']}</span>
                         </div>
                     </div>
                     <div className="row">
                         <div className="col-md-12">
                             <div className="mb-3">
                                 <label className="form-label d-block">Status</label>
                                 <div className="form-check form-check-inline mt-2">
                                     <input className="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" defaultChecked onChange={e => onInputChange(e)}/>
                                     <label className="form-check-label" htmlFor="inlineRadio1">Active</label>
                                 </div>
                                 <div className="form-check form-check-inline">
                                     <input className="form-check-input" type="radio" name="status" id="inlineRadio2" value="0" onChange={e => onInputChange(e)}/>
                                     <label className="form-check-label" htmlFor="inlineRadio2">Inactive</label>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
            {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
};

export default Modal