import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";





const ReadModal = ({ onClose, children, title, lesson_id}) => {
    const[description,SetDescription] = useState();
   

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    useEffect(() => {
        async function fetchData()
        {
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getLessonDescriptionById?id="+lesson_id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(lessonData => {
                    
                // console.log(res.gradeList)
                SetDescription(lessonData.output.description)
            });

        }
        fetchData();    
    },[]);

 

    

    const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-scrollable">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="ReadMore">Description</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                    
                    <p>
                        {description}
                    </p>
                    </div>
                    <div className="modal-footer">
                    <button type="button" className="btn btn-primary" onClick={handleCloseClick}>Close</button>
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
};

export default ReadModal