"use client";

import Image from 'next/image'
import { useRouter } from 'next/navigation'
import { useFormik } from 'formik';
import React, { useRef,useState,useEffect } from "react";
import { signIn, signOut,useSession } from "next-auth/react"
import { alertService } from 'services';
import Alert  from '@components/Alert';

export default function Login() {
    const { data: session } = useSession()
    const [isLoading, setLoading] = React.useState(false);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });
    const router = useRouter()
    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        onSubmit
    })

    useEffect(() => {
        
        if(session?.user.role == 0)
        router.push(process.env.NEXT_PUBLIC_URL+"/adminDashboard")
        if(session?.user.role == 1)
        router.push(process.env.NEXT_PUBLIC_URL+"/schoolDashboard")
        if(session?.user.role == 2)
        router.push(process.env.NEXT_PUBLIC_URL+"/teacherDashboard")
        if(session?.user.role == 3)
        router.push(process.env.NEXT_PUBLIC_URL+"/studentDashboard")
        if(session?.user.role == 5)
        router.push(process.env.NEXT_PUBLIC_URL+"/districtDashboard")
        
      }, [session]);
    
    async function onSubmit(values){
        setLoading(true);
        const status = await signIn('credentials', {
            redirect: false,
            email: values.email,
            password: values.password
        })
    console.log(status)
        if(status.ok){
            if(!status.url)
            { 
                alertService.error('Invalid Credentials', options)
                setLoading(false);
            }
            
        } 
        
    }
    

  return (
    <div className="login-bg">
        <div className="container">
            <div className="authentication-wrapper authentication-basic">
                <div className="row">
                    
                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-mb-none">
                        <div className="login-img">
                            <img src="/assets/img/home-bg.png" className="img-responsive"/>
                        </div>
                    </div>
                    
                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div className="position-container">
                            <div className="authentication-inner">
                            
                                <div className="card">
                                    <div className="card-body">
                                    
                                        <div className="app-brand justify-content-center">
                                            <a href="SuperAdmin-Dashboard.php" className="app-brand-link gap-2">
                                                <img src="/assets/img/pixel-logo.png" alt="" height="55px"/>
                                            </a>
                                        </div>
                                        
                                        <h3 className="mb-2 text-center fw-bold">Hello Again!</h3>
                                        <p className="mb-4 text-center">Enter your credentials to access your account </p>
                                        <Alert />
                                        <form onSubmit={formik.handleSubmit} className="mb-3">
                                            <div className="form-floating mb-3">
                                                <input type="text" name="email" className="form-control" id="floatingInput" placeholder="name@example.com" {...formik.getFieldProps('email')}/>
                                                <label>Email address</label>
                                            </div>
                                            <div className="form-floating mb-3">
                                                <input type="password" name="password" className="form-control" id="floatingPassword" placeholder="Enter at least 8+ characters" {...formik.getFieldProps('password')}/>
                                                <label>Password</label>
                                            </div>
                                            
                                            {!isLoading &&<button className="btn btn-primary d-grid w-100" type="submit">Login</button>}
                            {isLoading &&<button type="submit" className="btn btn-primary d-grid w-100" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                            </form>
                                            <div className="mb-0 d-flex justify-content-between">
                                            <div className="form-check">
                                                {/* <input className="form-check-input" type="checkbox" id="terms-conditions" name="terms"/>
                                                <label className="form-check-label" for="terms-conditions"> Keep me logged in </label> */}
                                            </div>
                                                <div className="form-check">
                                                    <a href="/forgotPassword">Forgot password?</a>
                                                </div>
                                            </div>
                                        
                                            
                                    
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>

  )
}
