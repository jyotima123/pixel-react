"use client"
import { useEffect, useState } from "react";
import { BiPlusMedical } from "react-icons/bi";
import { useRouter } from 'next/navigation'
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';

import { alertService } from 'services';
import Alert  from '@components/Alert';
export const dynamic = 'force-dynamic'
import { useSession } from "next-auth/react";

// Initialize jquery and Datatable
// 

const school = () => {
    const { data: session } = useSession()
    const [showModal, setShowModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const[districtData,SetdistrictData] = useState([]);
    const[schoolList,setSchoolList] = useState([]);
    const[schoolType,setSchoolType] = useState(0);
    const[district,setDistrict] = useState(0);
    const[report,setReport] = useState([]);
    const[editId,setId] = useState(false);
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });
    const router = useRouter()

    useEffect(() => {
        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
            });
        }
    
        callTable();
        
    },[schoolList]);

  useEffect(() => {
    
    async function fetchData()
        {

            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            window.JSZip = jzip;
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSchoolListByDistrictId?id="+session?.user.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(schoolList => {
                        $('#RgTableX').DataTable().destroy();
                        // console.log(schoolList)
                        setSchoolList(schoolList.output)
                        
                });

            
        }
        if(session?.user.role == 5)
        {
            fetchData()
        }
    
  }, [session]);

 


  const childCall = (res,msg) =>{

    setShowModal(false)
    if(res == 'success')
    {   
        location.reload();
        alertService.success(msg, options)
    }
    if(res == 'error')
    alertService.error(msg, options)

  } 



  const openEditModal = (event, param) => {
    setId(param);
    
    setEditModal(true);
  }
  const editchildCall = (res,msg) =>{

    setEditModal(false)
   
    if(res == 'success')
    {   
        location.reload();
        alertService.success(msg, options)
    }
    if(res == 'error')
    {
        location.reload();
        alertService.error(msg, options)
    }

  }

  const changeStatus = async (event, param) => {
   
    var data = {
        'id' : param,
        'status' : event.target.checked
    }
    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateSchoolStatus", {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store'
        }).then(res => res.json())
        .then(res => {
            
            
            if(res.success)
            {
                location.reload()
                alertService.success('Status Updated Successfully', options)
            }
            else{
                alertService.error('Something went wrong', options)
            }
        });
  }

  const handleDelete = async(event, param) => {
    if (confirm("Do you really want to delete this data.")) {

        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/school/delete/"+param, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store'
        }).then(res => res.json())
        .then(res => {
            if(res.success)
            {
                location.reload()
                alertService.success('School deleted successfully', options)
            }
            else{
                alertService.error('Something went wrong', options)
            }
        });
    }
  }

  const getFilter = async(e) => {
    var url = '';

    if(e.target.name == 'schooltype')
    {
        setSchoolType(e.target.value)
        var url = process.env.NEXT_PUBLIC_APP_URL+'/getSchoolByTypeAndDistrict?schooltype='+e.target.value+'&district='+session?.user.id
        
    }
    
    
    if(url != '')
    {
        const res = await fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
            cache: 'no-store'
            }).then(res => res.json())
            .then(res => {
                if(res.success)
                {
                    $('#RgTableX').DataTable().destroy();
                    setSchoolList([])
                    setSchoolList(res.schoolData)
                }
            });
    }
   
  }

  const onInputChange = e => {
    setReport([...report, e.target.value ]);
};

const generateReport = async () => {

    let abc = {
        id : report
    }

    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/getSchoolsExcelReport', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store',
        body: JSON.stringify(abc)
        }).then(res => res.json())
        .then(res => {
            if(res.success)
            {
                let alink = document.createElement('a');
                alink.href = res.fileurl;
                alink.download = 'report.xlsx';
                alink.click();
            }
        });
}

const toggleMenu = () => {

    setLeft('layout-menu-expanded')
    
}
const menuCall = () => {

    setLeft(null)
    
}


  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="school" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">Schools</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                    <Alert />
                        <div className="card">
                            <div className="card-header">
                                <div className="row d-flex align-items-center">
                                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3 mb-Rg-3">
                                        <label for="defaultSelect" className="form-label">Filter By</label>
                                        <select id="defaultSelect" name="schooltype" className="form-select" onChange={e => getFilter(e)} value={schoolType}>
                                            <option value="0">School Type</option>
                                            <option value="1">Private</option>
                                            <option value="2">Public</option>
                                            <option value="3">Charter</option>
                                            <option value="4">Independent</option>
                                        </select>
                                    </div>
                                    {/* <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3 mb-Rg-3">
                                        <label for="defaultSelect" className="form-label">Filter By</label>
                                        <select id="defaultSelect" name="district" className="form-select" value={district} onChange={e => getFilter(e)}>
                                        <option value="0">Select District</option>
                                        {districtData.map((result,key) => {
                                            return (
                                                <option key={key} value={result.id}>{result.fullName}</option>
                                            )})}
                                        </select>
                                    </div> */}
                                    {/* <div className="col-lg-2 col-md-2 col-sm-3 col-xs-3 mt-3 mb-Rg-3">
                                        <a type="button" className="btn btn-theme btn-outline-theme me-2" onClick={generateReport}>
                                            <span className="bx bxs-download"></span>&nbsp; Export
                                        </a>
                                    </div> */}
                                    
                                </div>
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                
                                <table id="RgTableX" className="table RgTable" style={{width: '100%'}}>
                                        <thead className="RgBGTable">
                                            <tr>
                                                {/* <th></th> */}
                                                <th>School Name</th>
                                                
                                                <th>Contact Person</th>
                                                <th>Email</th>
                                                <th>Contact No.</th>
                                                <th>Ext.</th>
                                               
                                                <th>Type</th>
                                                <th>Status</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {schoolList.map((result,key, row) => {
                                            return (
                                                <tr key={key}>
                                                {/* <td><input className="form-check-input" type="checkbox" onChange={e => onInputChange(e)} value={result.schoolId} id="flexCheckDefault"/></td> */}
                                                <td>{result.fullName}</td>
                                                
                                                <td>{result.contactPerson}</td>
                                                <td>{result.email}</td>
                                                <td>{result.phone}</td>
                                                <td>{result.extension}</td>
                                                                                                
                                                <td>{result.schoolType}</td>
                                                
                                                
                                                <td>
                                                
                                                <div className="form-check form-switch mb-2">
                                                        <input className="form-check-input" type="checkbox" checked={result.status} id="flexSwitchCheckDefault"/>
                                                    </div>
                                                </td>
                                               
                                               
                                            </tr>
                                            )})}
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div>
                    {showModal &&
            <Modal onClose={childCall}/>
                
        }
         {editModal &&
            <EditModal school_id = {editId} onClose={editchildCall}/>
                
        }
        </div>
                </div>
            </div>
        </div>
        {/*Below we add the modal wrapper*/}
        <div id="modal-root"></div>
    </div>

    
  )
}

export default school