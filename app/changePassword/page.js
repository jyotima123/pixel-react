"use client"
import { useEffect, useState } from "react";
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { useSession } from "next-auth/react";
import { alertService } from 'services';
import Alert  from '@components/Alert';

const page = () => {
    const { data: session } = useSession()
    const [isLoading, setLoading] = useState(false);
    const[inputType,setInputType] = useState('password');
    const[reinputType,setreInputType] = useState('password');
    const[leftSide,setLeft] = useState(null);
    const [userData, setUserData] = useState({
        userId:"",
        password:""
      });
      const [errors, setError] = useState({
        errors: "",
      });
      const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });
      useEffect(() => {
    
        async function fetchData()
            {
                
                setUserData({...userData,'userId': session?.user.id})
    
                
            }

            if(session)
            {
                fetchData();
            }
            
        
      }, [session]);

    const changeInput = (e,val) =>{
        setInputType(val);
    }

    const changereInput = (e,val) =>{
        setreInputType(val);
    }

    const onInputChange = e => {

        setUserData({ ...userData, [e.target.name]: e.target.value });
        
    };
    const onConInputChange = e => {

        let errors = {};
        let formIsValid = true;
        let pwd = userData.password
        if(pwd !== e.target.value)
        {
            formIsValid = false;
            errors["rePassword"] = "Password does not match";
        }      
        
        setError(errors);
        return formIsValid;
    };

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/user/changePassword", {
            method: "PUT",
            body: JSON.stringify(userData),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    location.reload();
                    alertService.success('Account Updated Successfully.Your new Password will be effective in next login', options)
                }
                else{
                    alertService.error('Something went wrong', options)
                }
            });
  
          
      };

      const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
        <Sidebar  onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Change Password</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>
                  

                    <div className="container-xxl flex-grow-1 container-p-y">
                    <Alert />
                        <div className="row">
                            <div className="col-md-12">
                                <ul className="nav nav-pills flex-column flex-md-row mb-3">
                                    <li className="nav-item">
                                        <a className="nav-link" href="/setting"><i className="bx bx-user me-1"></i> Account</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link active"><i className="bx bx bxs-key me-1"></i> Change Password</a>
                                    </li>
                                </ul>
                                <div className="card mb-4">
                                    <h5 className="card-header">Change password</h5>
                                   
                                    <hr className="my-0" />
                                    <div className="card-body">
                                        <form id="formAccountSettings" method="POST" onsubmit="return false">
                                            <div className="row">
                                                

                                                <div className="mb-3 col-md-6 form-password-toggle">
                                                    <div className="d-flex justify-content-between">
                                                        <label className="form-label" for="password">New Password</label>
                                                    </div>
                                                    <div className="input-group input-group-merge">
                                                        <input type={inputType} id="password" className="form-control" name="password"  aria-describedby="password" onChange={e => onInputChange(e)}/>
                                                        <span className="input-group-text cursor-pointer">
                                                            {(inputType == 'password') ?
                                                            <i className="bx bx-hide" onClick={(e) => changeInput(e,'text')}></i>
                                                            :
                                                            <i className="bx bx-show" onClick={(e) => changeInput(e,'password')}></i>
                                                           }
                                                        </span>
                                                    </div>
                                                </div>
                                                </div>
                                                <div className="row">
                                                <div className="mb-3 col-md-6 form-password-toggle">
                                                    <div className="d-flex justify-content-between">
                                                        <label className="form-label" for="password">Re Enter New Password</label>
                                                    </div>
                                                    <div className="input-group input-group-merge">
                                                        <input type={reinputType} id="password" className="form-control" name="repassword"  aria-describedby="password" onChange={e => onConInputChange(e)}/>
                                                        <span className="input-group-text cursor-pointer">
                                                            {(reinputType == 'password') ?
                                                            <i className="bx bx-hide" onClick={(e) => changereInput(e,'text')}></i>
                                                            :
                                                            <i className="bx bx-show" onClick={(e) => changereInput(e,'password')}></i>
                                                           }</span>
                                                    </div>
                                                    <span className="error" style={{ color: "red" }}>{errors['rePassword']}</span>
                                                </div>
                                            </div>
                                            <div className="mt-2">
                                            {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Save Changes</button>}
            {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                                                
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
  )
}

export default page