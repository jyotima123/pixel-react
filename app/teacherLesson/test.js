"use client"
import { useEffect, useState } from "react";
import { BiPlusMedical } from "react-icons/bi";
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { alertService } from 'services';
import Alert  from '@components/Alert';
export const dynamic = 'force-dynamic'
import { useSession } from "next-auth/react";
import { usePathname } from 'next/navigation'

const page = () => {
    const { data: session } = useSession()
    const[lessonData,SetLessonData] = useState([]);
    const[classData,SetClassData] = useState([]);
    const[classes,setClasses] = useState(0);
    const[tab,setTab] = useState(0)
    const pathname = usePathname()

    const sid = pathname.split('/')

    useEffect(() => {
    
        async function fetchData()
            {
    
                await import('datatables.net-dt/js/dataTables.dataTables')
                await import('datatables.net-dt/css/jquery.dataTables.min.css')
                window.JSZip = jzip;
                const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getLessonByTeacherId?id="+session?.user.id, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    cache: 'no-store'
                    }).then(res => res.json())
                    .then(list => {
                            // console.log(schoolList)
                            SetLessonData(list.output)
                            setTab(sid[2])
                            
                    });

                    const result = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getClassesDropListByTeacherId?id="+session?.user.id, {
                        method: "GET",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        cache: 'no-store'
                        }).then(res => res.json())
                        .then(list => {
                            
                            SetClassData(list.output)
                   
                    });
    
                
            }
            if(session && classes == 0)
            {
                fetchData();
            }
        
      }, [session]);


      const openTab = (e,key) => {

        setTab(key)

      }

      const getFilter = async(e) => {

        var url = process.env.NEXT_PUBLIC_APP_URL+'/getLessonByTeacherIdAndClassId'

        let data = {
            teacherId : session?.user.id,
            classId : e.target.value
        }

        setClasses(e.target.value)

        const res = await fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store',
                body: JSON.stringify(data)
                }).then(res => res.json())
                .then(res => {
                    if(res.success)
                    {
                        SetLessonData([])
                        SetLessonData(res.output)
                    }
                });
       
    }

  return (
    <div className="layout-wrapper layout-content-navbar">
        <div className="layout-container">
        <Sidebar menu="teacherLesson"/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Lessons</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                        
                        <div className="card">
                            <div className="card-header">
                                <div className="row align-items-center justify-content-between border-bottom">
                                    <div className="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-3">
                                        <label for="defaultSelect" className="form-label">Select Class</label>
                                        <select id="defaultSelect" className="form-select"  value={classes} onChange={e => getFilter(e)}>
                                            <option value="0">Select Class</option>
                                            {classData.map((result,key) => {
                                            return (
                                                <option key={key} value={result.id}>{result.className}</option>
                                            )})}
                                        </select>
                                    </div>
                                    
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="nav-align-top mb-4">
                                    <ul className="nav nav-tabs nav-fill" role="tablist">
                                        {(lessonData.map((result,key) => {
                                            return(
                                                <li className="nav-item">
                                                    <button type="button" id={"p_"+key} className={(tab == result.id)? "nav-link active":"nav-link"} role="tab" onClick={event => openTab(event,result.id)}>
                                                        <i className="tf-icons bx bxs-book-open"></i> {result.lessonName}
                                                    </button>
                                                </li>
                                            )
                                        }))}
                                        
                                    </ul>
                                    <div className="tab-content">
                                        {(lessonData.map((result,key) => {
                                            return(
                                                <div className={(tab == result.id) ?"tab-pane fade active show" : "tab-pane fade"} id="navs-justified-Rgtab1" role="tabpanel">
                                                <div className="row">
                                                    <div className="col-md-12 mb-3">
                                                        <div className="Rg-img"><img className="lesson-img" src={result.cover}/></div>
                                                        <div className="text-end mt-2">
                                                            <span className="badge bg-secondary">Timing: {result.timing} minutes</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12 border-bottom mb-3">
                                                        <h3 className="sub-heading clr-theme">Description</h3>
                                                        <p className="mb-2">
                                                            {result.description}
                                                        </p>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="row">
                                                            <div className="col-lg-3 mb-2">
                                                                <a href={result.lessonUrl} className="btn d-block btn-outline-primary" target="_blank">Go to Lesson Plan</a>
                                                            </div>
                                                            <div className="col-lg-3 mb-2">
                                                                <a href={result.lessonPdf} type="button" className="btn d-block rounded-pill btn-primary" download>
                                                                    <span className="tf-icons bx bx-download"></span>&nbsp; Download Lesson Plan
                                                                </a>
                                                            </div>
                                                            <div className="col-lg-3 mb-2">
                                                                <a href={result.resourceUrl} className="btn d-block btn-outline-primary" target="_blank">Go to Resource Presentation</a>
                                                            </div>
                                                            <div className="col-lg-3 mb-2">
                                                                <a href={result.resourcePdf} type="button" className="btn d-block rounded-pill btn-primary" download>
                                                                    <span className="tf-icons bx bx-download"></span>&nbsp; Download Resource
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            )
                                        }))}
                                       
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
    </div>
  )
}

export default page