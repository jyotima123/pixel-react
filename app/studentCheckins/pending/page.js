"use client"
import { useEffect, useState } from "react";
import { BiPlusMedical } from "react-icons/bi";
import { BsFillEyeFill } from "react-icons/bs";
import $ from "jquery";
import jzip from 'jzip';
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { alertService } from 'services';
import Alert  from '@components/Alert';
export const dynamic = 'force-dynamic'
import { useSession } from "next-auth/react";

const page = () => {
    const { data: session } = useSession()
    const[surveyList,setSurveyList] = useState([]);
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });
    
    useEffect(() => {
        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "scrollX": true,
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
            });
        }
    
        callTable();
        
    },[surveyList]);

    useEffect(() => {
    
        async function fetchData()
            {
    
                await import('datatables.net-dt/js/dataTables.dataTables')
                await import('datatables.net-dt/css/jquery.dataTables.min.css')
                window.JSZip = jzip;
                const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getPendingScheduleCheckinsByStudentId?id="+session?.user.id, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    cache: 'no-store'
                    }).then(res => res.json())
                    .then(surveyList => {
                            $('#RgTableX').DataTable().destroy();
                            
                            setSurveyList(surveyList.output)
                            
                    });
    
            }
            if(session?.user.role == 3)
            {
                fetchData();
            }
        
      }, [session]);


      const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }
      
  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="studentCheckins" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">Surveys</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                    <Alert />
                        <div className="card">
                            <div className="card-header align-items-center d-flex justify-content-between">
                                <a type="button" >
                                    {/* <span className="bx bxs-download"></span>&nbsp; Export */}
                                </a>
                                <a href="/studentCheckins"> &nbsp; Show Full List</a>
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                    <table id="RgTableX" className="table RgTable" style={{width:'100%'}}>
                                        <thead className="RgBGTable">
                                            <tr>
                                                {/* <th>Survey Name</th> */}
                                                
                                                <th>Check-In Name</th>
                                        <th>Status</th>
                                                
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {surveyList.map((result,key, row) => {
                                            return (
                                                <tr key={key}>
                                                {/* <td>{result.surveyName}</td> */}
                                                <td>{result.checkinName}</td>
                                                <td>Pending</td>
                                                
                                                
                                                <td>
                                                    <a href={"/survey/fill/"+result.surveyId+'-'+result.studentCheckinSurveyId} style={{cursor:"pointer"}} title="Fill Survey" target="_blank"><BsFillEyeFill />
                                                                </a>
                                                    
                                                </td>
                                                
                                                
                                            </tr>
                                             )})}
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
  )
}

export default page