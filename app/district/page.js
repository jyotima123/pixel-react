"use client"
import { useEffect, useState } from "react";
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import $ from "jquery";
import jzip from 'jzip';

import { BiPlusMedical } from "react-icons/bi";
import Modal from "./addModal";
import EditModal from "./editModal";
import { alertService } from 'services';
import Alert  from '@components/Alert';
import { useSession } from "next-auth/react";

// Initialize jquery and Datatable
// 

const district = () => {
    const { data: session,update } = useSession()
    const [showModal, setShowModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const[districtList,SetDistrictList] = useState([]);
    const[report,setReport] = useState([]);
    const[editId,setId] = useState(false);

    const[districtData,SetdistrictData] = useState([]);
    const[schoolType,setSchoolType] = useState(0);
    const[district,setDistrict] = useState(0);
    
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });

    useEffect(() => {

        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
              
            });
        }
    
        callTable();
    
        
    },[districtList]);

  useEffect(() => {
    
    async function fetchData()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;

            const result = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getDistrictListWithSchool", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(districtList => {

                    if(districtList.output.length > 0)
                    {
                        $('#RgTableX').DataTable().destroy();
                        SetDistrictList(districtList.output)
                    }
                    
            });
        }
        fetchData();
    
  }, []);

  

  const childCall = (res,msg) =>{

    setShowModal(false)
    if(res == 'success')
    {   
        location.reload();
        alertService.success(msg, options)
    }
    if(res == 'error')
    alertService.error(msg, options)

  } 

  

  const openEditModal = (event, param) => {
    setId(param);
    setEditModal(true);
  }
  const editchildCall = (res,msg) =>{

    setEditModal(false)
 
    if(res == 'success')
    {   location.reload();
        alertService.success(msg, options)
    }
    if(res == 'error')
    {
        alertService.error(msg, options)
    }
    
  }

  const changeStatus = async (event, param) => {
   
    var data = {
        'id' : param,
        'status' : event.target.checked
    }
    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateDistrictStatus", {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store'
        }).then(res => res.json())
        .then(res => {
            
            
            if(res.success)
            {
                location.reload()
                alertService.success('Status Updated Successfully', options)
            }
            else{
                alertService.error('Something went wrong', options)
            }
        });
  }

  const handleDelete = async(event, param) => {
    if (confirm("Do you really want to delete this data.")) {

        const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/deleteDistrict/"+param, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store'
        }).then(res => res.json())
        .then(res => {
            if(res.success)
            {
                alertService.success('School District deleted successfully', options)
                location.reload()
            }
            else{
                alertService.error('Something went wrong', options)
            }
        });
    }
  }

  const onInputChange = e => {
    setReport([...report, e.target.value ]);
};

const generateReport = async () => {

    let abc = {
        id : report
    }

    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/getDistrictListInSheet', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store',
        body: JSON.stringify(abc)
        }).then(res => res.json())
        .then(res => {
            if(res.success)
            {
                let alink = document.createElement('a');
                alink.href = res.fileurl;
                alink.download = 'report.xlsx';
                alink.click();
            }
        });
}

const login = async(e,values) => {


    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/getUserById?id='+values, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store',
        }).then(res => res.json())
        .then(async res => {
            if(res.success)
            {
                
                await update({
                    ...session,
                    user:{
                        ...session?.user,
                        name: res.data.fullName,
                        role: res.data.role,
                        email: res.data.email,
                        id: res.data.id
                    },
                })
                
                
                window.location.href = '/districtDashboard'
            }
        });
        
}

const sendEmail = async(e,values) => {


    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/sendLoginEmailById?id='+values, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store',
        }).then(res => res.json())
        .then(async res => {
            if(res.success)
            alertService.success('Login Credential Send Successfully', options)
            else
            alertService.error('There is some issue to send email', options)
        });
        
}

const toggleMenu = () => {

    setLeft('layout-menu-expanded')
    
}
const menuCall = () => {

    setLeft(null)
    
}

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="district"  onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4"  onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">School Districts</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                    <Alert />
                        <div className="card">
                            <div className="card-header">
                                <div className="d-flex align-items-center justify-content-between">
                                    <a type="button" className="btn btn-theme btn-outline-theme" onClick={generateReport}>
                                        <span className="bx bxs-download"></span>&nbsp; Export
                                    </a>
                                    <a type="button" className="btn btn-primary" onClick={() => setShowModal(true)}>
                                        <BiPlusMedical />&nbsp; Add School District
                                    </a>
                                </div>
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                
                                <table id="RgTableX" className="table RgTable">
                                        <thead className="RgBGTable">
                                            <tr>
                                                <th></th>
                                                <th>School District</th>
                                                <th>Email</th>
                                                <th>No. Of Schools</th>
                                                <th>No. of Students</th>
                                                <th>Status</th>
                                                <th>Send Email</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {(districtList.length > 0)?
                                        districtList.map((result,key, row) => {
                                            return (
                                                <tr key={key}>
                                                <td><input className="form-check-input" type="checkbox" onChange={e => onInputChange(e)} value={result.id} id="flexCheckDefault"/></td>
                                                <td>{result.fullName}</td>
                                                <td>{result.email}</td>
                                                <td>{result.schoolCount}</td>
                                                <td>{result.studentCount}</td>
                                                <td>
                                                {(result.status == 1) ?
                                                    <div className="form-check form-switch mb-2">
                                                        <input className="form-check-input" type="checkbox" id="flexSwitchCheckDefault" defaultChecked onChange={event => changeStatus(event, result.id)}/>
                                                    </div>
                                                    :
                                                    <div className="form-check form-switch mb-2">
                                                        <input className="form-check-input" type="checkbox" id="flexSwitchCheckDefault" onChange={event => changeStatus(event, result.id)}/>
                                                    </div>
                                                }
                                                </td>
                                                <td><a className="btn btn-primary" onClick={event => sendEmail(event, result.schoolId)} style={{cursor:"pointer"}}>Send Credential
                                                                </a></td>
                                                <td>
                                                <a onClick={event => login(event, result.id)} style={{cursor:"pointer"}} title="Login As District"><i style={{fontSize:'2.15rem'}} className="bx bx-right-arrow-circle"></i>
                                                                </a>
                                                    <a onClick={event => openEditModal(event, result.id)} style={{cursor:"pointer"}}><i className="bx bx-edit-alt me-1"></i>
                                                                </a>
                                                    <a onClick={event => handleDelete(event, result.id)} style={{cursor:"pointer"}}><i className="bx bx-trash me-1"></i>
                                                                </a>
                                                </td>
                                            </tr>
                                            )})
                                        :
                                        <></>
                                        }
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div>
                    {showModal &&
            <Modal onClose={childCall}/>
                
        }
         {editModal &&
            <EditModal district_id = {editId} onClose={editchildCall}/>
                
        }
        </div>
                </div>
            </div>
        </div>
        {/*Below we add the modal wrapper*/}
        <div id="modal-root"></div>
    </div>

    
  )
}

export default district