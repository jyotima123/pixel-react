import React,{ useEffect,useState } from 'react'
import ReactDOM from "react-dom";
import { useFormik } from 'formik';



const EditModal = ({ onClose, children, title,district_id }) => {
    const [isLoading, setLoading] = useState(false);
    const [district, setDistrict] = useState({
        id:"",
        fullName: "",
        email: "",  
        phone: ""
      });
      const [errors, setError] = useState({
        errors: "",
      });

    const handleCloseClick = (e) => {
        e.preventDefault();
        let res = "close";
        onClose(res);
    };

    useEffect(() => {
        
        async function fetchData()
        {

            const resschool = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getDistrictById?id="+district_id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store' 
                }).then(res => res.json())
                .then(districtDetail => {
                    
                // console.log(res)
                setDistrict(districtDetail.output)
            
            // console.log(countryData)
        });
        }
        fetchData();    
    },[]);

   

    const onInputChange = e => {
        if(e.target.name == 'phone')
        {
            var contact = formatPhoneNumber(e.target.value)
            // console.log(contact)
            setDistrict({ ...district, [e.target.name]: contact });
        }
        else{
            setDistrict({ ...district, [e.target.name]: e.target.value });
        }
    };

    const formatPhoneNumber = (phoneNumberString) => {
        var cleaned = ('' + phoneNumberString).replace(/\D/g, '');
        var match = phoneNumberString.match(/^([0-9]{3})([0-9]{3})([0-9]{4})$/);
        // console.log(match)
        if (match) {
          return  match[1] + ' ' + match[2] + ' ' + match[3];
        }
        return cleaned;
    }

    const handleValidation = (e) => {
        let errors = {};
        let formIsValid = true;
        console.log(district);
        //username
        if(!district["fullName"]){
        formIsValid = false;
        errors["districtName"] = "District name Cannot be empty";
        }  
        
        if(!district["email"]){
            formIsValid = false;
            errors["email"] = "Email cannot be empty";
          }
          
        //Mobile No.
        if(!district["phone"]){
            formIsValid = false;
            errors["phone"] = "Contact cannot be empty";
          }
    
        setError(errors);
        return formIsValid;
    }

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        if(handleValidation()){
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateDistrict", {
            method: "PUT",
            body: JSON.stringify(district),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    onClose("success",res.message);
                }
                else{
                    onClose("error",res.message);
                }
            });
  
          }
          else{
            setLoading(false);
          }
       
      };

    

    const modalContent = (
        <div className="modal">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title Rg-Modal-title" id="exampleModalLabel1">Edit District</h5>
                    <button type="button" className="btn-close" onClick={handleCloseClick}>
                            x
                        </button>
                    </div>
                    {title && <h1>{title}</h1>}
                    <div className="modal-body">
                        <form>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label for="defaultFormControlInput" className="form-label">District Name</label>
                                        <input type="hidden" name="id" value={district.id}/>
                                        <input type="text" placeholder="District" className="form-control" name="fullName" value={district.fullName} onChange={e => onInputChange(e)}/>
                                        <span className="error" style={{ color: "red" }}>{errors['districtName']}</span>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label className="form-label" for="basic-icon-default-phone">Contact No.</label>
                                        <div className="input-group input-group-merge">
                                            <span id="basic-icon-default-phone2" className="input-group-text"><i className="bx bx-phone"></i></span>
                                            <input type="text" id="basic-icon-default-phone" value={district.phone} className="form-control phone-mask" placeholder="658 799 8941" maxLength={12} name="phone" onChange={e => onInputChange(e)}/>
                       
                                        </div>
                                        <span className="error" style={{ color: "red" }}>{errors['phone']}</span>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label className="form-label" for="basic-icon-default-email">Email</label>
                                        <div class="input-group input-group-merge">
                                        <span className="input-group-text"><i className="bx bx-envelope"></i></span>
                                            <input type="text" id="basic-icon-default-email" className="form-control" placeholder="tom@example.com" name="email" value={district.email} onChange={e => onInputChange(e)}/>
                                            </div>
                                            <span className="error" style={{ color: "red" }}>{errors['email']}</span>
                                        <div className="form-text">You can use letters, numbers &amp; periods</div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-outline-secondary" onClick={handleCloseClick}>Cancel</button>
                        
                        {!isLoading && <button type="button" className="btn btn-primary" onClick={onSubmit}>Submit</button>}
            {isLoading && <button type="button" className="btn btn-primary" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                    </div>
                </div>
            </div>
        </div>
    );

    return ReactDOM.createPortal(
        modalContent,
        document.getElementById("modal-root")
    );
};

export default EditModal