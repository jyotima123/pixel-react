"use client"
import { useEffect, useState } from "react";
import Header from '@components/Header'
import Sidebar from '@components/Sidebar'
import Select from "react-select";
import { useSession } from "next-auth/react";
import dynamic from 'next/dynamic';

const page = () => {
    const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });
    const[schoolData,SetSchoolData] = useState([]);
    const[schoolId,setschoolId] = useState();
    const[gradeData,SetGradeData] = useState([]);
    const[gradeId,setgradeId] = useState();
    const[studentData,SetstudentData] = useState([]);
    const[studentId,setstudentId] = useState();
    const[leftSide,setLeft] = useState(null);
    const[filter,setFilter] = useState({
        schools: [],
        grades: [],
        students: [],
        districts: []
    })

    const [radialChart, setRadial] = useState({
        options:{
            series: [72],
            plotOptions: {
                radialBar: {
                    hollow: {
                        margin: 5,
                        size: "25%",
                    },
                    dataLabels: {
                        style: {
                            colors: ['#641CEC'],
                        },
                        name: {
                            show: false,
                        },
                        value: {
                            show: false
                        },
                    }
                }
            },
            fill: {
                type: "solid",
                colors: ['#641CEC']
            },
            // labels: ['Type A', 'Type B', 'Type C', 'Type D', 'Type E']
            grid: {
                padding: {
                    top: -35,
                    bottom: -35
                }
            },
        }
        
    });

    const [columnChart, setColumn] = useState({
        series: [{
            name: 'Check-ins by Grade',
            data: [20, 60, 10, 20]
        }],
        options: {
            colors: ['#641cec'],
            chart: {
                type: 'bar',
                height: 168,
                toolbar: {
                    tools: {
                        download: false,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset: false
                    },
                    autoSelected: "zoom"
                },
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: false,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: ['Grade 1', 'Grade 2', 'Grade 3', 'Grade 4'],
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false,
                },
            },
            fill: {
                opacity: 1
            },
            yaxis: {
                show: false
            },
            grid: {
                show: false
            }
        }
    });

    const [lineChart,setLine] = useState({
        options: {
            series: [{
                name: "Desktops",
                data: [10, 15, 1, 10, 4]
            }],
            chart: {
                height: 167,
                type: 'line',
                toolbar: {
                    tools: {
                        download: false,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset: false
                    },
                    autoSelected: "zoom"
                },
                //   zoom: {
                //     enabled: false
                //   },
                lineThickness: 1,

            },
            plotOptions: {
                bar: {
                    horizontal: true
                }
            },
            dataLabels: {
                enabled: false,
            },
            title: {
                show: false
            },
            xaxis: {
                categories: ['21/01', '05/02', '10/02', '05/02', '10/02'],
                labels: {
                    style: {
                        fontSize: '10px'
                    },

                },
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false,
                },
            },
            yaxis: {
                show: false,
            },
            markers: {
                size: 2,
                shape: "circle",
                radius: 2,
                colors: '#641CEC',
                strokeColors: '#641CEC',
                fillOpacity: 1,
            },
            stroke: {
                show: true,
                lineCap: 'butt',
                colors: '#641CEC',
                width: 2,
                dashArray: 0,
            },
            grid: {
                show: false,
            },
        }
    })


    //effect on updating filter
    useEffect(() => {
    
        async function fetchData()
        {


            const result2 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getReportGradeList", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(filter),
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                console.log(list)
                SetGradeData(list.output)
            });


            const result3 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getFilterStudents", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(filter),
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(list)
                SetstudentData(list.output)
            });

        }
    
            fetchData();
      }, [filter]);


    useEffect(() => {
    
        async function fetchData()
        {

            var data = {
                districts: []
            }

            const result1 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSchoolList", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(list)
                SetSchoolData(list.output)
            });

            var data = {
                schools: []
            }
            const result2 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getReportGradeList", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                console.log(list)
                SetGradeData(list.output)
            });


            var data = {
                schools: [],
                grades: []
            }
            const result3 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getFilterStudents", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(list)
                SetstudentData(list.output)
            });

            

            
        }
    
            fetchData();
      }, []);

    const changeschool = async e => {

        var report = []
        for(var i = 0; i< e.length; i++)
        {
            report.push(e[i].value)
        }
        setFilter({ ...filter, 'schools': report});
        
        setschoolId(e);
    };

    const changegrade = async e => {

        var report = []
        for(var i = 0; i< e.length; i++)
        {
            report.push(e[i].value)
        }
        setFilter({ ...filter, 'grades': report});
        
        setgradeId(e);
    };

    const toggleMenu = () => {

        setLeft('layout-menu-expanded')
        
    }
    const menuCall = () => {

        setLeft(null)
        
    }

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="reports" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title">Detailed Report</h4>
                            </div>
                            <Header/>
                        </div>
                    </nav>
 
                    <div className="container-xxl flex-grow-1 container-p-y">

                        <div className="card">
                            <div className="card-body">
                                <div className="row mb-3">
                                    <div className="col-md-12">
                                        <div className="Rg-card-header bg-themePx">
                                            <div className="row d-flex align-items-center justify-content-between">
                                                <div className="col-lg-6 col-md-3 col-sm-6 col-xs-3 mb-Rg-3">
                                                    <h3 className="sub-heading m-0 p-0">Overall</h3>
                                                </div>
                                                <div className="col-lg-2 col-md-3 col-sm-6 col-xs-3 mb-Rg-3">
                                                    <Select 
                                                        options={schoolData}
                                                        placeholder="Select School"
                                                        value={schoolId}
                                                        onChange={changeschool}
                                                        isSearchable={true}
                                                        isMulti
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                    />
                                                </div>
                                                <div className="col-lg-2 col-md-3 col-sm-6 col-xs-3 mb-Rg-3">
                                                    <Select 
                                                        options={gradeData}
                                                        placeholder="Select Grade"
                                                        value={gradeId}
                                                        onChange={changegrade}
                                                        isSearchable={true}
                                                        isMulti
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                    />
                                                </div>
                                                <div className="col-lg-2 col-md-3 col-sm-6 col-xs-3 mb-Rg-3">
                                                    <Select 
                                                        options={studentData}
                                                        placeholder="Select Student"
                                                        value={studentId}
                                                        isSearchable={true}
                                                        onChange={changeschool}
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="co1-12">
                                        <div className="row">
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                                <div className="border rounded bg-themePx">
                                                    <div className="card-header">
                                                        <h5 className="m-0 p-0">Overall</h5>
                                                    </div>
                                                    <div className="card-body">
                                                        <div className="row d-flex align-items-center">
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <h1 className="Rg-Graph-Title hw-bold text-nowrap clr-theme m-0 p-0">72%</h1>
                                                            </div>
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <div id="RgChartRadical">
                                                                    <Chart
                                                                        options={radialChart.options}
                                                                        series={radialChart.options.series}
                                                                        type="radialBar"
                                                                        height="200"
                                                                    />
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                                <div className="border rounded">
                                                    <div className="">
                                                        <div id="RgChartColumn">
                                                            <Chart
                                                                options={columnChart.options}
                                                                series={columnChart.series}
                                                                type="bar"
                                                                height="168"
                                                            />
                                                        </div>

                                                        

                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                                <div className="border rounded">
                                                    <div className="">
                                                        <div id="RgChartLine">
                                                            <Chart
                                                                options={lineChart.options}
                                                                series={lineChart.options.series}
                                                                type="line"
                                                                height="167"
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="col-md-12">
                                        <div className="Rg-card-header bg-themePx">
                                            <div className="row d-flex align-items-center justify-content-between">
                                                <div className="col-lg-6 col-md-3 col-sm-6 col-xs-3 mb-Rg-3">
                                                    <h3 className="sub-heading m-0 p-0">Wellness</h3>
                                                </div>
                                                {/* <div className="col-lg-2 col-md-3 col-sm-6 col-xs-3 mb-Rg-3">
                                                    <select id="defaultSelect" className="form-select">
                                                        <option value="1">Group 1</option>
                                                        <option value="2">Group 2</option>
                                                        <option value="3">Group 3</option>
                                                        <option value="4">Group 4</option>
                                                        <option value="5">Group 5</option>
                                                    </select>
                                                </div> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="co1-12">
                                        <div className="row">
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                                <div className="border rounded bg-themePx">
                                                    <div className="card-header">
                                                        <h5 className="m-0 p-0">Overall</h5>
                                                    </div>
                                                    <div className="card-body">
                                                        <div className="row d-flex align-items-center">
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <h1 className="Rg-Graph-Title hw-bold text-nowrap clr-theme m-0 p-0">72%</h1>
                                                            </div>
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <div id="RgChartRadical-1"><Chart
                                                                        options={radialChart.options}
                                                                        series={radialChart.options.series}
                                                                        type="radialBar"
                                                                        height="200"
                                                                    /></div>

                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                                <div className="border rounded">
                                                    <div className="">
                                                        <div id="RgChartColumn-1"><Chart
                                                                options={columnChart.options}
                                                                series={columnChart.series}
                                                                type="bar"
                                                                height="168"
                                                            /></div>

                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                                <div className="border rounded">
                                                    <div className="">
                                                        <div id="RgChartLine-1"><Chart
                                                                options={lineChart.options}
                                                                series={lineChart.options.series}
                                                                type="line"
                                                                height="167"
                                                            /></div>

                                                        

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="col-md-12">
                                        <div className="Rg-card-header bg-themePx">
                                            <div className="row d-flex align-items-center justify-content-between">
                                                <div className="col-lg-6 col-md-3 col-sm-6 col-xs-3 mb-Rg-3">
                                                    <h3 className="sub-heading m-0 p-0">Knowledge</h3>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="co1-12">
                                        <div className="row">
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                                <div className="border rounded bg-themePx">
                                                    <div className="card-header">
                                                        <h5 className="m-0 p-0">Overall</h5>
                                                    </div>
                                                    <div className="card-body">
                                                        <div className="row d-flex align-items-center">
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <h1 className="Rg-Graph-Title hw-bold text-nowrap clr-theme m-0 p-0">72%</h1>
                                                            </div>
                                                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <div id="RgChartRadical-2"><Chart
                                                                        options={radialChart.options}
                                                                        series={radialChart.options.series}
                                                                        type="radialBar"
                                                                        height="200"
                                                                    /></div>

                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                                <div className="border rounded">
                                                    <div className="">
                                                        <div id="RgChartColumn-2"><Chart
                                                                options={columnChart.options}
                                                                series={columnChart.series}
                                                                type="bar"
                                                                height="168"
                                                            /></div>

                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                                <div className="border rounded">
                                                    <div className="">
                                                        <div id="RgChartLine-2"><Chart
                                                                options={lineChart.options}
                                                                series={lineChart.options.series}
                                                                type="line"
                                                                height="167"
                                                            /></div>

                                                        

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="col-md-12">
                                        <div className="accordion mt-3" id="accordionExample">
                                            <div className="card accordion-item">
                                                <h2 className="accordion-header" id="headingOne">
                                                    <button type="button" className="accordion-button collapsed bg-themePx" data-bs-toggle="collapse" data-bs-target="#accordionOne" aria-expanded="false" aria-controls="accordionOne">
                                                        Q1. I sleep well
                                                        <span className="Rg-accordion-number">72%</span>
                                                    </button>
                                                </h2>
                                                <div id="accordionOne" className="accordion-collapse collapse" data-bs-parent="#accordionExample">
                                                    <div className="accordion-body">
                                                        <div className="row">
                                                            <div className="col-lg-10 col-md-8 col-sm-8 col-xs-12 mb-Rg-3">
                                                                <div className="table-responsive table-bordered text-nowrap">
                                                                    <table className="table table-hover">
                                                                        
                                                                        <tbody className="table-border-bottom-0">
                                                                            <tr>
                                                                                <td><i className="fab fa-angular fa-lg text-danger me-3"></i> <strong>Lesson 1</strong></td>
                                                                                <td>Always</td>
                                                                                <td><span className="badge bg-label-primary me-1">21/01/2022</span></td>
                                                                                <td className="clr-theme">88</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><i className="fab fa-angular fa-lg text-danger me-3"></i> <strong>Lesson 2</strong></td>
                                                                                <td>Rarely</td>
                                                                                <td><span className="badge bg-label-primary me-1">05/02/2023</span></td>
                                                                                <td className="clr-theme">23</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><i className="fab fa-angular fa-lg text-danger me-3"></i> <strong>3 week check in </strong></td>
                                                                                <td>Sometimes</td>
                                                                                <td><span className="badge bg-label-primary me-1">10/02/2020</span></td>
                                                                                <td className="clr-theme">18</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-end">
                                                                <div id="RgChartRadical-accordion"></div>

                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card accordion-item">
                                                <h2 className="accordion-header" id="headingTwo">
                                                    <button type="button" className="accordion-button collapsed bg-themePx" data-bs-toggle="collapse" data-bs-target="#accordionTwo" aria-expanded="false" aria-controls="accordionOne">
                                                        Q2. I eat well
                                                        <span className="Rg-accordion-number">65%</span>
                                                    </button>
                                                </h2>
                                                <div id="accordionTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample" >
                                                    <div className="accordion-body">
                                                        <div className="row">
                                                            <div className="col-lg-10 col-md-8 col-sm-8 col-xs-12 mb-Rg-3">
                                                                <div className="table-responsive table-bordered text-nowrap">
                                                                    <table className="table table-hover">
                                                                       
                                                                        <tbody className="table-border-bottom-0">
                                                                            <tr>
                                                                                <td><i className="fab fa-angular fa-lg text-danger me-3"></i> <strong>Lesson 1</strong></td>
                                                                                <td>Always</td>
                                                                                <td><span className="badge bg-label-primary me-1">21/01/2022</span></td>
                                                                                <td className="clr-theme">88</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><i className="fab fa-angular fa-lg text-danger me-3"></i> <strong>Lesson 2</strong></td>
                                                                                <td>Rarely</td>
                                                                                <td><span className="badge bg-label-primary me-1">05/02/2023</span></td>
                                                                                <td className="clr-theme">23</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><i className="fab fa-angular fa-lg text-danger me-3"></i> <strong>3 week check in </strong></td>
                                                                                <td>Sometimes</td>
                                                                                <td><span className="badge bg-label-primary me-1">10/02/2020</span></td>
                                                                                <td className="clr-theme">18</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                                                                <div id="RgChartRadical-accordion-1"></div>

                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card accordion-item">
                                                <h2 className="accordion-header" id="headingThree">
                                                    <button type="button" className="accordion-button collapsed bg-themePx" data-bs-toggle="collapse" data-bs-target="#accordionThree" aria-expanded="false" aria-controls="accordionOne">
                                                        Q3. I am rested
                                                        <span className="Rg-accordion-number">84%</span>
                                                    </button>
                                                </h2>
                                                <div id="accordionThree" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                    <div className="accordion-body">
                                                        <div className="row">
                                                            <div className="col-lg-10 col-md-8 col-sm-8 col-xs-12 mb-Rg-3">
                                                                <div className="table-responsive table-bordered text-nowrap">
                                                                    <table className="table table-hover">
                                                                        
                                                                        <tbody className="table-border-bottom-0">
                                                                            <tr>
                                                                                <td><i className="fab fa-angular fa-lg text-danger me-3"></i> <strong>Lesson 1</strong></td>
                                                                                <td>Always</td>
                                                                                <td><span className="badge bg-label-primary me-1">21/01/2022</span></td>
                                                                                <td className="clr-theme">88</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><i className="fab fa-angular fa-lg text-danger me-3"></i> <strong>Lesson 2</strong></td>
                                                                                <td>Rarely</td>
                                                                                <td><span className="badge bg-label-primary me-1">05/02/2023</span></td>
                                                                                <td className="clr-theme">23</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><i className="fab fa-angular fa-lg text-danger me-3"></i> <strong>3 week check in </strong></td>
                                                                                <td>Sometimes</td>
                                                                                <td><span className="badge bg-label-primary me-1">10/02/2020</span></td>
                                                                                <td className="clr-theme">18</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                                                                <div id="RgChartRadical-accordion-2"></div>

                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default page