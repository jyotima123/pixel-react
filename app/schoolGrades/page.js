"use client"
import { useEffect, useState } from "react";

import $ from "jquery";
import jzip from 'jzip';
import { BiPlusMedical } from "react-icons/bi";
import Sidebar from "@components/Sidebar";
import Header from '@components/Header';
import { alertService } from 'services';
import Alert  from '@components/Alert';
import { useSession } from "next-auth/react";

// Initialize jquery and Datatable
// 

const grade = () => {
    const { data: session } = useSession()
    const[gradeList,SetGradeList] = useState([]);
    const[report,setReport] = useState([]);
    const[leftSide,setLeft] = useState(null);
    const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });

    useEffect(() => {

        async function callTable()
        {
            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
    
            $('#RgTableX').DataTable({
                "searching": true,   // Search Box will Be Disabled
                "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
                "info": true,         // Will show "1 to n of n entries" Text at bottom
                "lengthChange": true, // Will Disabled Record number per page
                "language": {
                "paginate": {
                  "previous": "<",
                  "next": ">"
                }
              } 
              
            });
        }
    
        callTable();
    
        
    },[gradeList]);

  useEffect(() => {
    
    async function fetchData()
        {

            await import('datatables.net-dt/js/dataTables.dataTables')
            await import('datatables.net-dt/css/jquery.dataTables.min.css')
            
            window.JSZip = jzip;
            
            const result = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getGradeListBySchoolId?id="+session?.user.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(res => {
                    
                // console.log(res)
                if(res.output.length>0)
                {
                    $('#RgTableX').DataTable().destroy();
                    SetGradeList(res.output)
                }
               
            });
        }

        if(session)
        {
            fetchData();
        }
    
  }, [session]);



  const onInputChange = e => {
    setReport([...report, e.target.value ]);
};

const generateReport = async () => {

    let abc = {
        schoolId: session?.user.id,
        gradeIds : report
    }

    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/getGradeListInSheetBySchoolId', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store',
        body: JSON.stringify(abc)
        }).then(res => res.json())
        .then(res => {
            if(res.success)
            {
                let alink = document.createElement('a');
                alink.href = res.fileurl;
                alink.download = 'report.xlsx';
                alink.click();
            }
        });
}

const changeStatus = async (event, param) => {

    var data = {
        'id' : param,
        'status' : event.target.checked
    }
    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/updateSchoolGradeStatus", {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store'
        }).then(res => res.json())
        .then(res => {
        
        
        if(res.success)
        {
            location.reload()
            alertService.success('Status Updated Successfully', options)
        }
        else{
            alertService.error('Something went wrong', options)
        }
    });
}

const toggleMenu = () => {

    setLeft('layout-menu-expanded')
    
}
const menuCall = () => {

    setLeft(null)
    
}

  return (
    <div className={"layout-wrapper layout-content-navbar "+leftSide}>
        <div className="layout-container">
            <Sidebar menu="grades" onClick={menuCall}/>
            <div className="layout-page">
                <div className="content-wrapper">
                    <nav className="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                        <div className="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                            <a className="nav-item nav-link px-0 me-xl-4" onClick={toggleMenu} style={{cursor:'pointer'}}>
                                <i className="bx bx-menu bx-sm"></i>
                            </a>
                        </div>
                        <div className="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                            <div className="row d-flex align-items-center">
                                <h4 className="main-title ">Grades</h4>
                            </div>
                            <Header />
                        </div>
                    </nav>

                    <div className="container-xxl flex-grow-1 container-p-y">

                    <Alert />
                        <div className="card">
                            <div className="card-header">
                                <div className="d-flex align-items-center justify-content-between">
                                    <a type="button" className="btn btn-theme btn-outline-theme" onClick={generateReport}>
                                        <span className="bx bxs-download"></span>&nbsp; Export
                                    </a>
                                    
                                </div>
                            </div>
                            <div className="table-responsive text-nowrap">
                                <div className="container-fluid">
                                
                                <table id="RgTableX" className="table RgTable" style={{width: '100%'}}>
                                        <thead className="RgBGTable">
                                        <tr>
                                            <th></th>
                                            <th>Grade Name</th>
                                            <th>Total Check-ins</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {(gradeList.length > 0) ?
                                        gradeList.map((result,key, row) => {
                                            return (
                                                <tr key={key}>
                                                <td><input className="form-check-input" type="checkbox" onChange={e => onInputChange(e)} value={result.id} id="flexCheckDefault"/></td>
                                                <td>{result.gradename}</td>
                                                <td>{result.totalCheckins}</td>
                                               
                                                <td>
                                                <div className="form-check form-switch mb-2">
                                                        <input className="form-check-input" type="checkbox" id="flexSwitchCheckDefault" checked={result.status} onChange={event => changeStatus(event, result.id)}/>
                                                    </div>
                                                </td>
                                            </tr>
                                            )}):
                                            <></>}
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                  
                </div>
            </div>
        </div>
        
    </div>

    
  )
}

export default grade