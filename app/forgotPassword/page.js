"use client"
import { useEffect, useState } from "react";
import { alertService } from 'services';
import Alert  from '@components/Alert';

const page = () => {
    const [isLoading, setLoading] = useState(false);
    const [userData, setUserData] = useState({
        email: ""
      });
      const [options, setOptions] = useState({
        autoClose: true,
        keepAfterRouteChange: false
    });
    const onInputChange = e => {

        setUserData({ ...userData, [e.target.name]: e.target.value });
        
    };

    const onSubmit = async e => {
        e.preventDefault();
        
        setLoading(true);
        
           
            const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/user/forgotPassword", {
            method: "PUT",
            body: JSON.stringify(userData),
            headers: {
                "Content-Type": "application/json",
            }
            }).then(res => res.json())
            .then(res => {
                setLoading(false);
                
                if(res.success)
                {
                    
                    alertService.success(res.message, options)
                }
                else{
                    alertService.error(res.message, options)
                }
            });
  
          
      };

  return (
    <div className="login-bg">
        <div className="container">
            <div className="authentication-wrapper authentication-basic">
                <div className="row">
                   
                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-mb-none">
                        <div className="login-img">
                            <img src="/assets/img/home-bg.png" className="img-responsive"/>
                        </div>
                    </div>
                   
                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div className="position-container">
                            <div className="authentication-inner">
                               
                                <div className="card">
                                    <div className="card-body">
                                        
                                        <div className="app-brand justify-content-center">
                                            <a href="SuperAdmin-Dashboard.php" className="app-brand-link gap-2">
                                                <img src="/assets/img/pixel-logo.png" alt="" height="55px"/>
                                            </a>
                                        </div>
                                        
                                        <h3 className="mb-2 text-center fw-bold">Forgot Password? 🔒</h3>
                                        <p className="mb-4 text-center">Enter your email and we'll send you instructions <br />to reset your password</p>
                                        <Alert />
                                        <form id="formAuthentication" className="mb-3" action="SuperAdmin-Dashboard.php" method="POST">
                                            <div className="form-floating mb-3">
                                                <input type="email" className="form-control" id="floatingInput" name="email" placeholder="name@example.com" onChange={e => onInputChange(e)}/>
                                                <label for="floatingInput">Email address</label>
                                            </div>
                                            {!isLoading && <button type="button" className="btn btn-primary d-grid w-100" onClick={onSubmit}>Submit</button>}
            {isLoading && <button type="button" className="btn btn-primary d-grid w-100" disabled>Loading...<i className="fas fa-spinner fa-spin"></i></button>}
                                            
                                        </form>
                                        <div className="text-center">
                                            <a href="/login" className="d-flex align-items-center justify-content-center">
                                                <i className="bx bx-chevron-left scaleX-n1-rtl bx-sm"></i>
                                                Back to login
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
  )
}

export default page