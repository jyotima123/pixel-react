"use client"
import { useEffect, useState } from "react";
import Header from '@components/Header'
import Sidebar from '@components/Sidebar'
import Select from "react-select";
import { useSession } from "next-auth/react";
import dynamic from 'next/dynamic';
import Bar from "@components/Maps/Bar";
import Line from "@components/Maps/Line";
import Radial from "@components/Maps/Radial";
import Pie from "@components/Maps/Pie";
import '@styles/chart.css'

const DetailedPage = () => {
    const { data: session,update } = useSession()
    const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });
    const[reportData,SetReportData] = useState([]);
    // const[schoolId,setschoolId] = useState();
    const[surveyData,SetSurveyData] = useState([]);
    const[surveyId,setsurveyId] = useState();
    // const[studentData,SetstudentData] = useState([]);
    // const[studentId,setstudentId] = useState();
    const[filter,setFilter] = useState({
        studentId : 0,
        surveyId: 0
    })

    const [overradialChart, setOverRadial] = useState({
        // options:{
        //     series: [],
        //     colors: ['#641CEC'],
        //     plotOptions: {
        //         radialBar: {
        //             hollow: {
        //                 margin: 5,
        //                 size: "25%",
        //             },
        //             dataLabels: {
        //                 style: {
        //                     colors: ['#641CEC'],
        //                 },
        //                 name: {
        //                     show: false,
        //                 },
        //                 value: {
        //                     show: false
        //                 },
        //             }
        //         }
        //     },
        //     fill: {
        //         type: "solid",
        //         colors: ['#641CEC']
        //     },
        //     // labels: ['Type A', 'Type B', 'Type C', 'Type D', 'Type E']
        //     grid: {
        //         padding: {
        //             top: -35,
        //             bottom: -35
        //         }
        //     },
        // }
        options: {
            series: [],
            colors: ['#641CEC'],
            labels: [],
            dataLabels: {
                enabled: false
            },
            chart: {
                height: 300,
                type: 'pie',
                toolbar: {
                    show: false
                }
            },
            legend: {
                show: false
            }
        }
    });

    const [groupChart, setGroup] = useState({
        series: [{
            name: 'Score by Group',
            data: []
        }],
        options: {
            colors: ['#641cec'],
            chart: {
                type: 'bar',
                height: 168,
                toolbar: {
                    tools: {
                        download: false,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset: false
                    },
                    autoSelected: "zoom"
                },
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: false,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: [],
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false,
                },
            },
            fill: {
                opacity: 1
            },
            yaxis: {
                show: false
            },
            grid: {
                show: false
            }
        }
    });

    const [columnChart, setColumn] = useState({
        series: [{
            name: 'Check-ins by Grade',
            data: [20, 60, 10, 20]
        }],
        options: {
            colors: ['#641cec'],
            chart: {
                type: 'bar',
                height: 168,
                toolbar: {
                    tools: {
                        download: false,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset: false
                    },
                    autoSelected: "zoom"
                },
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: false,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: ['Grade 1', 'Grade 2', 'Grade 3', 'Grade 4'],
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false,
                },
            },
            fill: {
                opacity: 1
            },
            yaxis: {
                show: false
            },
            grid: {
                show: false
            }
        }
    });

    const [lineChart,setLine] = useState({
        options: {
            series: [{
                name: "Scores By Date",
                data: []
            }],
            chart: {
                height: 167,
                type: 'line',
                toolbar: {
                    tools: {
                        download: false,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset: false
                    },
                    autoSelected: "zoom"
                },
                //   zoom: {
                //     enabled: false
                //   },
                lineThickness: 1,

            },
            plotOptions: {
                bar: {
                    horizontal: true
                }
            },
            dataLabels: {
                enabled: false,
            },
            title: {
                show: false
            },
            xaxis: {
                categories: [],
                labels: {
                    style: {
                        fontSize: '10px'
                    },

                },
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false,
                },
            },
            yaxis: {
                show: false,
            },
            markers: {
                size: 2,
                shape: "circle",
                radius: 2,
                colors: '#641CEC',
                strokeColors: '#641CEC',
                fillOpacity: 1,
            },
            stroke: {
                show: true,
                lineCap: 'butt',
                colors: '#641CEC',
                width: 2,
                dashArray: 0,
            },
            grid: {
                show: false,
            },
        }
    })


    //effect on updating filter
    useEffect(() => {
    
        async function fetchData()
        {


            const result2 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getStudentDetailedReport", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(filter),
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    SetReportData([])
                    SetReportData(list)
            });

        }
    
            fetchData();
      }, [filter]);


    useEffect(() => {
    
        async function fetchData()
        {

            const result1 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getSurveyListByStudentId?id="+session?.user.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                // console.log(list)
                SetSurveyData(list.output)
            });

            var data = {
                studentId : session?.user.id
            }

            const result2 = await fetch(process.env.NEXT_PUBLIC_APP_URL+"/getStudentDetailedReport", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
                cache: 'no-store'
                }).then(res => res.json())
                .then(list => {
                    
                SetReportData(list)
                setsurveyId(list.dropSurvey)

            });
       
            
        }
        if(session?.user.role == 3)
        {
            fetchData();

            setFilter({ ...filter, 'studentId': session?.user.id});
        }
            

      }, [session]);

    const changesurvey = async e => {

        setFilter({ ...filter, 'surveyId': e.value});

        // console.log(filter)
        
        setsurveyId(e)
    };


  return (
    <div className="card">
        <div className="card-body">
            <div className="row mb-3">
                <div className="col-md-12">
                    <div className="Rg-card-header bg-themePx">
                        <div className="row d-flex align-items-center justify-content-between">
                            <div className="col-lg-6 col-md-3 col-sm-6 col-xs-3 mb-Rg-3">
                            {(reportData.surveyId) ? 
                                <h3 className="sub-heading m-0 p-0">Overall</h3>
                                :
                                <></>
                            }
                            </div>
                            <div className="col-lg-2 col-md-3 col-sm-6 col-xs-3 mb-Rg-3">
                            
                            </div>
                            <div className="col-lg-2 col-md-3 col-sm-6 col-xs-3 mb-Rg-3">
                                
                            </div>
                            <div className="col-lg-2 col-md-3 col-sm-6 col-xs-3 mb-Rg-3">
                                <Select 
                                    options={surveyData}
                                    placeholder="Select Survey"
                                    value={surveyId}
                                    onChange={changesurvey}
                                    className="basic-multi-select"
                                    classNamePrefix="select"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {(reportData.surveyId) ? 
            <>
                <div className="row mb-3">
                    <div className="co1-12">
                        <div className="row">
                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                <div className="border rounded bg-themePx">
                                    <div className="card-header">
                                        <h5 className="m-0 p-0">Overall</h5>
                                    </div>
                                    <div className="card-body">
                                        <div className="row d-flex align-items-center">
                                            {/* <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <h1 className="Rg-Graph-Title hw-bold text-nowrap clr-theme m-0 p-0">{reportData.overallPercentage}</h1>
                                            </div> */}
                                            <div className="col-lg-12 col-md-6 col-sm-6 col-xs-6">
                                                {/* <div id="RgChartRadical">
                                                    <Chart
                                                        options={overradialChart.options}
                                                        series={overradialChart.options.series}
                                                        type="radialBar"
                                                        height="200"
                                                    />
                                                </div> */}
                                                <Pie list={reportData.overallChartPercentage}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div className="col-lg-8 col-md-6 col-sm-6 col-xs-6 mb-3">
                                <div className="border rounded">
                                    <div className="">
                                        {/* <div id="RgChartLine">
                                            <Chart
                                                options={lineChart.options}
                                                series={lineChart.options.series}
                                                type="line"
                                                height="167"
                                            />
                                        </div> */}
                                        <Line list={reportData.OverallDateChart}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* for wellness */}
                <div className="row mb-3">
                    <div className="co1-12">
                        <div className="row">
                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                <div className="border rounded bg-themePx">
                                    <div className="card-header">
                                        <h5 className="m-0 p-0">Wellness</h5>
                                    </div>
                                    <div className="card-body">
                                        <div className="row d-flex align-items-center">
                                            {/* <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <h1 className="Rg-Graph-Title hw-bold text-nowrap clr-theme m-0 p-0">{reportData.overallPercentage}</h1>
                                            </div> */}
                                            <div className="col-lg-12 col-md-6 col-sm-6 col-xs-6">
                                                {/* <div id="RgChartRadical">
                                                    <Chart
                                                        options={overradialChart.options}
                                                        series={reportData.wellnessChartPercentage}
                                                        type="radialBar"
                                                        height="200"
                                                    />
                                                </div> */}
                                            <Pie list={reportData.wellnessChartPercentage}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div className="col-lg-8 col-md-6 col-sm-6 col-xs-6 mb-3">
                                <div className="border rounded">
                                    <div className="">
                                        {/* <div id="RgChartLine">
                                            <Chart
                                                options={lineChart.options}
                                                series={lineChart.options.series}
                                                type="line"
                                                height="167"
                                            />
                                        </div> */}
                                        <Line list={reportData.WellnessDateChart}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* for knowledge */}
                <div className="row mb-3">
                    <div className="co1-12">
                        <div className="row">
                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 mb-3">
                                <div className="border rounded bg-themePx">
                                    <div className="card-header">
                                        <h5 className="m-0 p-0">Knowledge</h5>
                                    </div>
                                    <div className="card-body">
                                        <div className="row d-flex align-items-center">
                                            {/* <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <h1 className="Rg-Graph-Title hw-bold text-nowrap clr-theme m-0 p-0">{reportData.overallPercentage}</h1>
                                            </div> */}
                                            <div className="col-lg-12 col-md-6 col-sm-6 col-xs-6">
                                                {/* <div id="RgChartRadical">
                                                    <Chart
                                                        options={overradialChart.options}
                                                        series={reportData.knowledgeChartPercentage}
                                                        type="radialBar"
                                                        height="200"
                                                    />
                                                </div> */}
                                            <Pie list={reportData.knowledgeChartPercentage}/>              
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div className="col-lg-8 col-md-6 col-sm-6 col-xs-6 mb-3">
                                <div className="border rounded">
                                    <div className="">
                                        {/* <div id="RgChartLine">
                                            <Chart
                                                options={lineChart.options}
                                                series={lineChart.options.series}
                                                type="line"
                                                height="167"
                                            />
                                        </div> */}
                                        <Line list={reportData.KnowledgeDateChart}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div className="row mb-3">
                    <div className="col-md-12">
                        <div className="accordion mt-3" id="accordionExample">
                {(reportData.studentOutput.map((res,key) => {
                    return(
                            <div className="card accordion-item">
                                <h2 className="accordion-header" id="headingOne">
                                    <button type="button" className="accordion-button collapsed bg-themePx" data-bs-toggle="collapse" data-bs-target={"#accordionOne"+key} aria-expanded="false" aria-controls="accordionOne">
                                        Q{key+1}. {res.question}
                                        <span className="Rg-accordion-number"></span>
                                    </button>
                                </h2>
                                <div id={"accordionOne"+key} className="accordion-collapse collapse" data-bs-parent="#accordionExample">
                                    <div className="accordion-body">
                                        <div className="row">
                                            <div className="col-lg-10 col-md-8 col-sm-8 col-xs-12 mb-Rg-3">
                                                <div className="table-responsive table-bordered text-nowrap">
                                                    <table className="table table-hover">
                                                        
                                                        <tbody className="table-border-bottom-0">
                                                            {(res.result.map((rest,key) => {
                                                                return(
                                                                    <tr>
                                                                        <td><i className="fab fa-angular fa-lg text-danger me-3"></i> <strong>{rest.checkin}</strong></td>
                                                                        <td>{rest.answer}</td>
                                                                        <td><span className="badge bg-label-primary me-1">{rest.createdAt}</span></td>
                                                                        {/* <td className="clr-theme">{rest.score}</td> */}
                                                                    </tr>
                                                                )
                                                                
                                                            }))}
                                                            
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-end">
                                                <div id="RgChartRadical-accordion"></div>

                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    )
                }))}
                
                            
                            
                        </div>
                    </div>
                </div>
            </>
            :
            <>
            <div className="row mb-3">
                    <div className="co1-12">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3">
                                <div className="border rounded bg-themePx">
                                    {/* <div className="card-header">
                                        <h5 className="m-0 p-0">Overall</h5>
                                    </div> */}
                                    <div className="card-body text-center">
                                        <h5 className="m-0 p-0">Nothing To Display</h5>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </>
            }
        </div>
    </div>
  )
}

export default DetailedPage