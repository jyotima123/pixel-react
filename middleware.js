import { withAuth } from "next-auth/middleware"

// middleware is applied to all routes, use conditionals to select

export default withAuth(
  function middleware (req) {
    console.log('--------------hello--------------------')
  },
  {
    callbacks: {
      authorized: ({ req, token }) => {
        if (
          (req.nextUrl.pathname.startsWith('/adminDashboard') || req.nextUrl.pathname.startsWith('/school') || req.nextUrl.pathname.startsWith('/district') || req.nextUrl.pathname.startsWith('/grades') || req.nextUrl.pathname.startsWith('/lessons') || req.nextUrl.pathname.startsWith('/resources') || req.nextUrl.pathname.startsWith('/survey') || req.nextUrl.pathname.startsWith('/addSurvey') || req.nextUrl.pathname.startsWith('/setting') || req.nextUrl.pathname.startsWith('/changePassword') || req.nextUrl.pathname.startsWith('/checkin') || req.nextUrl.pathname.startsWith('/schoolDashboard') || req.nextUrl.pathname.startsWith('/teachers') || req.nextUrl.pathname.startsWith('/schoolGrades') || req.nextUrl.pathname.startsWith('/students') || req.nextUrl.pathname.startsWith('/teacherDashboard') || req.nextUrl.pathname.startsWith('/classes') || req.nextUrl.pathname.startsWith('/teacherClasses') || req.nextUrl.pathname.startsWith('/teacherLesson/') || req.nextUrl.pathname.startsWith('/teacherStudents') || req.nextUrl.pathname.startsWith('/teacherSurvey') || req.nextUrl.pathname.startsWith('/districtDashboard') || req.nextUrl.pathname.startsWith('/districtSchool') || req.nextUrl.pathname.startsWith('/studentDashboard') || req.nextUrl.pathname.startsWith('/studentCheckins') || req.nextUrl.pathname.startsWith('/studentLessons') || req.nextUrl.pathname.startsWith('/scheduleCheckin') || req.nextUrl.pathname.startsWith('/studentDetailedReports') || req.nextUrl.pathname.startsWith('/reports/schoolReports') || req.nextUrl.pathname.startsWith('/reports/gradeReports') || req.nextUrl.pathname.startsWith('/reports/studentReports') || req.nextUrl.pathname.startsWith('/reports/districtReports') || req.nextUrl.pathname.startsWith('/schoolCheckin') || req.nextUrl.pathname.startsWith('/checkin/') || req.nextUrl.pathname.startsWith('/schoolLesson/') || req.nextUrl.pathname.startsWith('/completeReports') || req.nextUrl.pathname.startsWith('/schoolCompleteReport') || req.nextUrl.pathname.startsWith('/teacherCompleteReport')) &&
          token === null
        ) {
          return false
        }
        return true
      }
    },
    pages: {
        signIn: "/login",
      }
  }
)

export const config = { matcher: ['/adminDashboard','/school','/district','/grades','/lessons','/resources','/survey','/addSurvey','/setting','/changePassword','/checkin','/schoolDashboard','/teachers','/schoolGrades','/students','/teacherDashboard','/classes','/teacherClasses','/teacherLesson/:path*','/teacherStudents','/teacherSurvey','/districtDashboard','/districtSchool','/studentDashboard','/studentCheckins','/studentLessons','/scheduleCheckin','/studentDetailedReports','/reports/schoolReports','/reports/gradeReports','/reports/studentReports','/reports/districtReports','/schoolCheckin','/checkin/:path*','/schoolLesson/:path*','/completeReports','/schoolCompleteReport','/teacherCompleteReport'] };