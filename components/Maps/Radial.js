"use client"
import { useEffect, useState } from "react";
import dynamic from 'next/dynamic';

const Radial = ({list}) => {
    const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });

    const [radialChart, setRadial] = useState({
        options:{
            series: [],
            colors: ['#641CEC'],
            plotOptions: {
                radialBar: {
                    hollow: {
                        margin: 5,
                        size: "25%",
                    },
                    dataLabels: {
                        style: {
                            colors: ['#641CEC'],
                        },
                        name: {
                            show: false,
                        },
                        value: {
                            show: false
                        },
                    }
                }
            },
            fill: {
                type: "solid",
                colors: ['#641CEC']
            },
            // labels: ['Type A', 'Type B', 'Type C', 'Type D', 'Type E']
            grid: {
                padding: {
                    top: -35,
                    bottom: -35
                }
            },
        }
        
    });

    useEffect(()=>{
        let chartArray = {...radialChart,'options': {
            series: list,
            colors: ['#641CEC','green'],
            plotOptions: {
                radialBar: {
                    hollow: {
                        margin: 5,
                        size: "25%",
                    },
                    dataLabels: {
                        style: {
                            colors: ['#641cec'],
                        },
                        name: {
                            show: false,
                        },
                        value: {
                            show: false
                        },
                    }
                }
            },grid: {
                padding: {
                    top: -35,
                    bottom: -35
                }
            },
            tooltip:{
                enabled: true,
                theme: false,
                fillSeriesColor: true,
                custom: function({ series }) {
                    return (
                      `<div style={{padding:10,backgroundColor:"#555"}}>
                      <span><strong>&nbsp 
                      ${series[0]}
                      %</strong></span>
                      </div>`
                    );
                  }
            }
        }}
        
        setRadial(chartArray)
    }, [list])

  return (
    <div id="RgChartRadical-1">
        <Chart
            options={radialChart.options}
            series={radialChart.options.series}
            type="radialBar"
            height="200"
        />
    </div>
  )
}

export default Radial