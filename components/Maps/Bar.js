"use client"
import { useEffect, useState } from "react";
import dynamic from 'next/dynamic';

const Bar = ({list}) => {
    const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });

    const [columnChart, setColumn] = useState({
        series: [{
            name: 'Score By Question',
            data: []
        }],
        options: {
            colors: ['#641cec'],
            chart: {
                type: 'bar',
                height: 168,
                toolbar: {
                    tools: {
                        download: false,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset: false
                    },
                    autoSelected: "zoom"
                },
            },
            tooltip: {
                enabled: false,
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: false,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: [],
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false,
                },
            },
            fill: {
                opacity: 1
            },
            yaxis: {
                show: false
            },
            grid: {
                show: false
            }
        }
    });

    useEffect(()=>{
        let bchartArray = {...columnChart,'series': [{
            name: 'Scores By Question',
            data: list.values
        }]}
        bchartArray = {...bchartArray,'options': {
                chart: {
                    toolbar: {
                        tools: {
                            download: false,
                            selection: false,
                            zoom: false,
                            zoomin: false,
                            zoomout: false,
                            pan: false,
                            reset: false
                        },
                        autoSelected: "zoom"
                    },
                },
                xaxis: {
                    categories: list.labels,
                    labels: {
                        show: true,
                        trim: true,
                        formatter: function(val) {
                            return val.substr(0,30)
                        } 
                    }
                },
                colors: ['#641cec'],
                fill: {
                    opacity: 1
                },
                yaxis: {
                    show: false
                },
                grid: {
                    show: false
                },
                stroke: {
                    show: false,
                    width: 2,
                    colors: ['transparent']
                },
                dataLabels: {
                    enabled: false
                },
                tooltip:{
                    custom: function({ series, seriesIndex, dataPointIndex, w }) {
                        return (
                          `<div style={{padding:10,backgroundColor:"#555"}}>
                          <span>&nbsp
                          ${w.globals.labels[dataPointIndex]} 
                          : &nbsp<br/><strong>&nbsp 
                          ${series[seriesIndex][dataPointIndex]}
                          </strong></span>
                          </div>`
                        );
                      }
                }
            }}

            setColumn(bchartArray)
    }, [list])


  return (
         <div id="RgChartColumn-1">
            <Chart
                options={columnChart.options}
                series={columnChart.series}
                type="bar"
                height="168"
            />
        </div>
  )
}

export default Bar