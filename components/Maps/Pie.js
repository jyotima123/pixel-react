"use client"
import { useEffect, useState } from "react";
import dynamic from 'next/dynamic';

const Pie = ({list}) => {

    const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });

    const [piChart, setPi] = useState({
        options: {
            series: [],
            colors: ['#641CEC'],
            labels: [],
            dataLabels: {
                enabled: false
            },
            chart: {
                height: 200,
                type: 'pie',
                toolbar: {
                    show: false
                }
            },
            legend: {
                show: false
            }
        }
      });


      useEffect(()=>{
        let chartArray = {...piChart,'options': {
            series: list,
            colors: ['#641CEC','#F5F2FDFF'],
            labels: [],
            dataLabels: { enabled: false },
            legend: { show: false },
            tooltip:{
                enabled: false
            }
            
        }}
        
        setPi(chartArray)

    }, [list])


  return (
    <div id="RgChartRadical-1">
        <Chart
            options={piChart.options}
            series={piChart.options.series}
            type="pie"
            height="200"
        />
    </div>
  )
}

export default Pie