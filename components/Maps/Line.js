"use client"
import { useEffect, useState } from "react";
import dynamic from 'next/dynamic';

const Line = ({list}) => {
    const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });

    const [lineChart,setLine] = useState({
        options: {
            series: [{
                name: "Scores By Date",
                data: []
            }],
            chart: {
                height: 167,
                type: 'line',
                toolbar: {
                    tools: {
                        download: false,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset: false
                    },
                    autoSelected: "zoom"
                },
                //   zoom: {
                //     enabled: false
                //   },
                lineThickness: 1,

            },
            plotOptions: {
                bar: {
                    horizontal: true
                }
            },
            dataLabels: {
                enabled: false,
            },
            title: {
                show: false
            },
            xaxis: {
                categories: [],
                labels: {
                    style: {
                        fontSize: '10px'
                    },

                },
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false,
                },
            },
            yaxis: {
                show: false,
            },
            markers: {
                size: 2,
                shape: "circle",
                radius: 2,
                colors: '#641CEC',
                strokeColors: '#641CEC',
                fillOpacity: 1,
            },
            stroke: {
                show: true,
                lineCap: 'butt',
                colors: '#641CEC',
                width: 2,
                dashArray: 0,
            },
            grid: {
                show: false,
            },
        }
    })

    useEffect(()=>{
        let lchartArray = {...lineChart,'options': {
            series: [{
                name: "Scores By Date",
                data: list.values
            }],
            chart: {
                toolbar: {
                    tools: {
                        download: false,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset: false
                    },
                    autoSelected: "zoom"
                },
                
                lineThickness: 1,

            },
            plotOptions: {
                bar: {
                    horizontal: true
                }
            },
            dataLabels: {
                enabled: false,
            },
            title: {
                show: false
            },
            xaxis: {
                categories: list.labels,
                tickPlacement: 'between'
            },
            yaxis: {
                show: false,
            },
            markers: {
                size: 2,
                shape: "circle",
                radius: 2,
                colors: '#641CEC',
                strokeColors: '#641CEC',
                fillOpacity: 1,
            },
            grid: {
                show: false,
            },
            }}

            setLine(lchartArray)
    }, [list])


  return (
    <div id="RgChartLine-1">
        <Chart
        options={lineChart.options}
        series={lineChart.options.series}
        type="line"
        height="167"
        />
    </div>
  )
}

export default Line