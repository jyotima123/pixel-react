"use client";

import { useEffect, useState } from "react";

export default function MultipleImageChoice({quesIndex, questions, setQuestions, ...rest}) {
    const currQuestion = questions[quesIndex]
    const[answers, setAnswers] = useState([])
    
    useEffect(()=>{
        if(currQuestion && currQuestion.answer){
            setAnswers(currQuestion.answer)
        }
    }, [])

    useEffect(()=>{
        updateAnswers(answers)
    }, [answers])

    const handleChange = (e) => {
        let value = parseInt(e.target.value)
        if(e.target.checked){
            setAnswers([...answers, value])            
        }
        else{
            setAnswers(current =>
                current.filter(currentValue => {
                  return currentValue !== value
                }),
            );
        }
    }

    const updateAnswers = () => {
        console.log("answers",answers)
        questions[quesIndex].answer = answers
        setQuestions(questions)
    }

    return (
        <>
        <div className="mb-5 text-left">
            <h2>{currQuestion.question}</h2>
        </div>
        <div className="w-100 mb-3 pt-5">
            <div className="Rg-container">
                <div className="radio-buttons">
                    {
                        currQuestion.options.map(option => {
                            return (
                                <label className="custom-radio">
                                    <input type="checkbox" id={option.optionImage} autoComplete="off" defaultValue={option.id} checked={answers.includes(option.id) ? true: false} onChange={handleChange}/>
                                    <span className="radio-btn"><i className="bx bx-check"></i>
                                        <div className="hobbies-icon Rg-img">
                                            <img src={option.optionImage} alt="image" height="120px" width="120px"/>
                                        </div>
                                    </span>
                                </label>
                            )
                        })
                    }
                </div>
            </div>
        </div>
        </>
    )
} 