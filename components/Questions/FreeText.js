"use client";

import { useEffect, useState } from "react";

export default function FreeText({quesIndex, questions, setQuestions, ...rest}) {
    const currQuestion = questions[quesIndex]
    const[answer, setAnswer] = useState('')

    useEffect(()=>{
        if(currQuestion && currQuestion.answer){
            setAnswer(currQuestion.answer)
        }
    }, [])

    const handleChange = (e) => {
        let value = e.target.value
        setAnswer(value)
        questions[quesIndex].answer = value
        setQuestions(questions)
    }
    return (
        <>
        <div className="mb-5 text-left">
            <h2>{currQuestion.question}</h2>
        </div>
        <div className="btn-group w-100 mb-3 pt-5">
            <textarea className="form-control" cols="30" rows="5" placeholder="Please type your text here.." defaultValue={answer} onChange={handleChange}></textarea>
        </div>
        </>
    )
} 