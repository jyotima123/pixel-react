"use client";

import { useEffect, useState } from "react";

export default function SingleImageChoice({quesIndex, questions, setQuestions, ...rest}) {
    const currQuestion = questions[quesIndex]
    const[answer, setAnswer] = useState('')

    useEffect(()=>{
        if(currQuestion && currQuestion.answer){
            setAnswer(currQuestion.answer)
        }
    }, [])

    useEffect(()=>{
        updateAnswer()
    }, [answer])

    const handleChange = (e) => {
        let value = parseInt(e.target.value)
        console.log("value", value)
        setAnswer(value)
    }

    const updateAnswer = () => {
        questions[quesIndex].answer = answer
        setQuestions(questions)
    }
    
    return (
        <>
        <div className="mb-5 text-left">
            <h2>{currQuestion.question}</h2>
        </div>
        <div className="w-100 mb-3 pt-5">
            <div className="Rg-container">
                <div className="radio-buttons">
                    {
                        currQuestion.options.map(option => {
                            return (
                                <label className="custom-radio">
                                    <input type="radio" name="radio" id={option.optionImage} autoComplete="off" value={option.id} checked={answer===option.id ? true: false} onClick={handleChange} />
                                    <span className="radio-btn"><i className="bx bx-check"></i>
                                        <div className="hobbies-icon Rg-img">
                                            <img src={option.optionImage} alt="image" height="120px" width="120px"/>
                                        </div>
                                    </span>
                                </label>
                            )
                        })
                    }
                </div>
            </div>
        </div>
        </>
    )
}