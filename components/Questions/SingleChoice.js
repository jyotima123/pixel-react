"use client";

import { useEffect, useState } from "react";

export default function SingleChoice({quesIndex, questions, setQuestions, ...rest}) {
    const currQuestion = questions[quesIndex]
    const[answer, setAnswer] = useState('')

    useEffect(()=>{
        if(currQuestion && currQuestion.answer){
            setAnswer(currQuestion.answer)
        }
    }, [])

    useEffect(()=>{
        updateAnswer()
    }, [answer])

    const handleChange = (e) => {
        let value = parseInt(e.target.value)
        console.log("value", value)
        setAnswer(value)
    }

    const updateAnswer = () => {
        questions[quesIndex].answer = answer
        setQuestions(questions)
    }

    return (
        <>
        <div className="mb-5 text-left">
            <h2>{currQuestion.question}</h2>
        </div>
        <div className="mb-5 pt-5">
            <div className="row">
            {
                currQuestion.options.map(option => {
                    return (
                        <div className="col-md-6 mb-3">
                        <input type="radio" className="btn-check" name="btnradio" id={option.optionName} autoComplete="off" value={option.id} checked={answer===option.id ? true: false} onClick={handleChange}/>
                        <label className="btn btn-outline-primary w-100" htmlFor={option.optionName}>{option.optionName}</label>
                        </div>
                    )
                })
            }
            </div>
        </div>
        </>
    )
}