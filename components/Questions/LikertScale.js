"use client";

import { useEffect, useState } from "react";

export default function LikertScale({quesIndex, questions, setQuestions, ...rest}) {
    const currQuestion = questions[quesIndex]
    const[range, setRange] = useState(currQuestion.scaleMin)

    useEffect(()=>{
        if(currQuestion && currQuestion.answer){
           
            setRange(currQuestion.answer)
        }
        else{
            questions[quesIndex].answer = currQuestion.scaleMin
           
            setQuestions(questions)
            setRange(currQuestion.scaleMin)
        }
        // console.log(currQuestion)
    }, [quesIndex,range])

    const handleChange = (e) => {
        let value = e.target.value
        setRange(value)
        questions[quesIndex].answer = value
        setQuestions(questions)
    }
    
    return (
        <>
        <div className="mb-3 text-left">
            <h2>{currQuestion.question}</h2>
        </div>
        <div className="w-100 mb-3 pt-5">
            <div className="containerRg">
                <div className="range-slider">
                    {(range < currQuestion.scaleMax) ?
                    <div id="slider_thumb" className="range-slider_thumb" style={{left: `${((range/currQuestion.scaleMax)*100)-((currQuestion.scaleMin/currQuestion.scaleMax)*100)}%`}}>{range}</div>
                    :
                    <div id="slider_thumb" className="range-slider_thumb" style={{left: `${((range/currQuestion.scaleMax)*100)-((currQuestion.scaleMin/currQuestion.scaleMax)*100)-10}%`}}>{range}</div>
                    }
                    <div className="d-flex justify-content-between">
                        <span className="fw-bold">{currQuestion.scaleMinPointer}</span>
                        <span className="fw-bold">{currQuestion.scaleMaxPointer}</span>
                    </div>
                    <div className="range-slider_line">
                        <div id="slider_line" className="range-slider_line-fill" style={{width: `${(range/currQuestion.scaleMax)*100}%`}}></div>
                    </div>
                    <input id="slider_input" className="range-slider_input" type="range" onChange={handleChange} min={currQuestion.scaleMin} max={currQuestion.scaleMax} />
                </div>
            </div>
        </div>
        </>
    )
} 