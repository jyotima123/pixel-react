"use client";

import { useEffect, useState } from "react";

export default function MultipleChoice({quesIndex, questions, setQuestions, ...rest}) {
    const currQuestion = questions[quesIndex]
    const[answers, setAnswers] = useState([])
    
    useEffect(()=>{
        if(currQuestion && currQuestion.answer){
            setAnswers(currQuestion.answer)
        }
    }, [])

    useEffect(()=>{
        updateAnswers(answers)
    }, [answers])

    const handleChange = (e) => {
        let value = parseInt(e.target.value)
        if(e.target.checked){
            setAnswers([...answers, value])            
        }
        else{
            setAnswers(current =>
                current.filter(currentValue => {
                  return currentValue !== value
                }),
            );
        }
        
    }

    const updateAnswers = () => {
        questions[quesIndex].answer = answers
        setQuestions(questions)
    }

    return (
        <>
        <div className="mb-5 text-left">
            <h2>{currQuestion.question}</h2>
        </div>
        <div className="mb-5 pt-5">
            <div className="row">
                {
                    currQuestion.options.map(option => {
                        return (
                            <div className="col-md-6 mb-3">
                                <input type="checkbox" className="btn-check" name={"opt"+option.id} id={option.id} autoComplete="off" defaultValue={option.id} checked={answers.includes(option.id) ? true: false} onChange={handleChange}/>
                                <label className="btn btn-outline-primary w-100" htmlFor={option.id}>{option.optionName}</label>
                            </div>
                        )
                    })
                }
            </div>
        </div>
        </>
    )
} 