"use client";

import React, { useEffect,useState } from 'react';
import { useRouter } from 'next/navigation'
import { BiSolidBell } from "react-icons/bi";
import { signIn, signOut, useSession, getProviders } from "next-auth/react";

const Header = () => {
    const[leftSide,setLeft] = useState(false);
    const[rightSide,setRight] = useState(false);
    const router = useRouter()
 
  const { data: session } = useSession()

  const [providers, setProviders] = useState(null);

  useEffect(() => {
    (async () => {
      const res = await getProviders();
      setProviders(res);
    })();

    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);

  function handleSignOut(){
    signOut()
  }
    

  return (
    <>
    {/* {session?.user ? ( */}
    
    <ul className="navbar-nav flex-row align-items-center ms-auto">
        <div className="d-none d-lg-block me-4 mt-2">
            <h6>{session?.user.district}</h6>
        </div>
        {/* <div className="btn-group d-none d-lg-block me-4"> */}
            
            {/* <button type="button" className="btn btn-primary btn-icon rounded-pill dropdown-toggle hide-arrow" onClick={() => setRight((prev) => !prev)}>
                <div className="position-relative">
                    <i className="bx bxs-bell"></i>
                    <span className="Rg-Notification flex-shrink-0 badge badge-center rounded-pill bg-danger w-px-20 h-px-20">4</span>
                </div>
            </button> */}
            {/* {(rightSide) ?
            <ul className="dropdown-menu dropdown-menu-end show" data-bs-popper="none">
                <li className="dropdown-header">
                    You have 4 new notifications
                    <a href="#"><span className="badge rounded-pill bg-primary p-2 ms-2">View all</span></a>
                </li>
                <li>
                    <hr className="dropdown-divider"/>
                </li>
                <li className="Rg-Notification-item">
                    <i className="bi bi-exclamation-circle text-warning"></i>
                    <div>
                        <h4>Lorem Ipsum</h4>
                        <p>Quae dolorem earum veritatis oditseno</p>
                        <p>30 min. ago</p>
                    </div>
                </li>
                <li>
                    <hr className="dropdown-divider"/>
                </li>
                <li className="Rg-Notification-item">
                    <i className="bi bi-x-circle text-danger"></i>
                    <div>
                        <h4>Atque rerum nesciunt</h4>
                        <p>Quae dolorem earum veritatis oditseno</p>
                        <p>1 hr. ago</p>
                    </div>
                </li>
                <li>
                    <hr className="dropdown-divider"/>
                </li>
                <li className="Rg-Notification-item">
                    <i className="bi bi-check-circle text-success"></i>
                    <div>
                        <h4>Sit rerum fuga</h4>
                        <p>Quae dolorem earum veritatis oditseno</p>
                        <p>2 hrs. ago</p>
                    </div>
                </li>
                <li>
                    <hr className="dropdown-divider"/>
                </li>
                <li className="Rg-Notification-item">
                    <i className="bi bi-info-circle text-primary"></i>
                    <div>
                        <h4>Dicta reprehenderit</h4>
                        <p>Quae dolorem earum veritatis oditseno</p>
                        <p>4 hrs. ago</p>
                    </div>
                </li>
                <li>
                    <hr className="dropdown-divider"/>
                </li>
                <li className="dropdown-footer">
                    <a href="#">Show all notifications</a>
                </li>
            </ul>
            :
            <></>
            } */}
        {/* </div> */}
        
        <li className="fw-bold" style={{cursor:'pointer'}} onClick={() => setLeft((prev) => !prev)}>&nbsp;&nbsp;{session?.user.name}</li>
        <li className="nav-item navbar-dropdown dropdown-user dropdown">
            <a style={{cursor:'pointer'}} className="nav-link dropdown-toggle hide-arrow  show" onClick={() => setLeft((prev) => !prev)}>
                <i className='bx bx-chevron-down'></i>
            </a>
            {(leftSide) ?
            <ul className="dropdown-menu dropdown-menu-end show" data-bs-popper="none">
                <li>
                    <div className="dropdown-divider"></div>
                </li>
                <li>
                    <a className="dropdown-item" href="/setting">
                        <i className="bx bx-cog me-2"></i>
                        <span className="align-middle fw-bold me-2">Settings</span>
                    </a>
                </li>
                <li>
                    <div className="dropdown-divider"></div>
                </li>
                <li>
                    <a className="dropdown-item" onClick={handleSignOut} style={{cursor:'pointer'}}>
                        <i className="bx bx-power-off me-2"></i>
                        <span className="align-middle fw-bold">Log Out</span>
                    </a>
                </li>
            </ul>
            :
            <></>
            }
        </li>
    </ul>
    {/* ):
    <>
    {router.push('/login')}
    </>
    } */}
    </>
  )
}

export default Header
