"use client";

import React, { useEffect,useState } from 'react';
import { useRouter } from 'next/navigation'
import { useSession, getProviders } from "next-auth/react";

const Nav = () => {
  const [providers, setProviders] = useState(null);
  const router = useRouter()
  const { data: session } = useSession()



useEffect(() => {
  (async () => {
    const res = await getProviders();
    setProviders(res);
  })();
}, []);


  return (
    <>
    {session?.user ? (
      <>
      </>
    ):(
      <>
       {router.push('/login')}
      </>
    )}
    </>
  )
}

export default Nav
