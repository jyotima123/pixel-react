"use client";

import Link from "next/link"
import { useEffect, useState } from "react";
import { useRouter } from 'next/navigation'
import { BiMap,BiLaptop, BiFontSize,BiCalendarCheck } from "react-icons/bi";
import { BsGraphUpArrow } from "react-icons/bs";
import { signIn, signOut, useSession, getProviders } from "next-auth/react";

const Sidebar = ({onClick, menu},props) => {

    const router = useRouter()
 
  const { data: session,update } = useSession()
  const [providers, setProviders] = useState(null);
  const[url,setUrl] = useState('')

  useEffect(() => {
    (async () => {
      const res = await getProviders();
      setProviders(res);
    })();

    if(session?.user.role == 0)
    setUrl('/adminDashboard')
    else if(session?.user.role == 1)
    setUrl('/schoolDashboard')
    else if(session?.user.role == 2)
    setUrl('/teacherDashboard')
    else if(session?.user.role == 3)
    setUrl('/studentDashboard')
    else if(session?.user.role == 5)
    setUrl('/districtDashboard')

  }, [session]);

  const login = async() => {

    if(session?.user.loginrole == 0 && (session?.user.role == 2 || session?.user.role == 3))
    {
        if (typeof window !== 'undefined' && window.localStorage) {
            var sid = localStorage.getItem('schoolAdmin');
            var tid = localStorage.getItem('schoolTeacher');
        }
        var id = (tid) ? tid : sid
    }
    else{
        var id = session?.user.loginId
    }

    const res = await fetch(process.env.NEXT_PUBLIC_APP_URL+'/getUserById?id='+id, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
        cache: 'no-store',
        }).then(res => res.json())
        .then(async res => {
            if(res.success)
            {
               
                if(tid)
                {
                    localStorage.removeItem('schoolTeacher');
                }
                else
                localStorage.clear();
                
                await update({
                    ...session,
                    user:{
                        ...session?.user,
                        name: res.data.fullName,
                        role: res.data.role,
                        email: res.data.email,
                        id: res.data.id,
                        district: res.data.district
                    },
                })
                
                if(res.data.role == 0)
                {
                    window.location.href = '/adminDashboard'
                }
                if(res.data.role == 1)
                {
                    window.location.href = '/schoolDashboard'
                }
                if(res.data.role == 2)
                {
                    window.location.href = '/teacherDashboard'
                }
                
            }
        });
        
}

const handleClick = (e) => {
    
    onClick(e);
};

  return (
<>
    {/* {session?.user ? ( */}

    <aside id="layout-menu" className="layout-menu menu-vertical menu bg-menu-theme">
        <div className="app-brand demo">
            <a href={url} className="app-brand-link">
                <span className="app-brand-logo demo">
                    <img src="/assets/img/pixel-logo.png" alt="" height="50px"/>
                </span>
            </a>

            <a className="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none" style={{cursor:'pointer'}}>
                <i className="bx bx-chevron-left bx-sm align-middle" onClick={handleClick}></i>
            </a>
        </div>
        
        <div className="menu-inner-shadow"></div>

        <ul className="menu-inner py-1">

          
            <li className="menu-header small text-capitalize fw-bold"></li>

{(session?.user.role == 0) ?
        <>
            {(menu=='dashboard') ?
                <li className="menu-item active">
                    <Link href="/adminDashboard" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-user-check"></i>
                        <div>Dashboard</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                <Link href="/adminDashboard" className="menu-link">
                <i className="menu-icon tf-icons bx bx-user-check"></i>
                    <div>Dashboard</div>
                </Link>
                </li>
            }
            {(menu=='district') ?
                <li className="menu-item active">
                    <Link href="/district" className="menu-link">
                        <BiMap className="menu-icon" style={{fontSize:'large'}}/>
                        <div>School Districts</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/district" className="menu-link">
                    <BiMap className="menu-icon" style={{fontSize:'large'}}/>
                        <div>School Districts</div>
                    </Link>
                </li>
            }
            {(menu=='grades') ?
                <li className="menu-item active">
                    <Link href="/grades" className="menu-link">
                        <i className="menu-icon tf-icons bx bx-chalkboard"></i>
                        <div>Grades</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/grades" className="menu-link">
                        <i className="menu-icon tf-icons bx bx-chalkboard"></i>
                        <div>Grades</div>
                    </Link>
                </li>
            }
            {(menu=='school') ?
                <li className="menu-item active">
                    <Link href="/school" className="menu-link">
                        <i className="menu-icon tf-icons bx bx-user-circle"></i>
                        <div>Schools</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/school" className="menu-link">
                        <i className="menu-icon tf-icons bx bx-user-circle"></i>
                        <div>Schools</div>
                    </Link>
                </li>
            }
        </>
        :
        (session?.user.role == 1) ?
        <>
        {(menu=='dashboard') ?
                <li className="menu-item active">
                    <Link href="/schoolDashboard" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-user-check"></i>
                        <div>Dashboard</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/schoolDashboard" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-user-check"></i>
                        <div>Dashboard</div>
                    </Link>
                </li>
            }
            {(menu=='grades') ?
                <li className="menu-item active">
                    <Link href="/schoolGrades" className="menu-link">
                        <i className="menu-icon tf-icons bx bx-chalkboard"></i>
                        <div>Grades</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/schoolGrades" className="menu-link">
                        <i className="menu-icon tf-icons bx bx-chalkboard"></i>
                        <div>Grades</div>
                    </Link>
                </li>
            }
            {(menu=='classes') ?
                <li className="menu-item active">
                    <Link href="/classes" className="menu-link">
                        <i className="menu-icon tf-icons bx bx-chalkboard"></i>
                        <div>Classes</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/classes" className="menu-link">
                        <i className="menu-icon tf-icons bx bx-chalkboard"></i>
                        <div>Classes</div>
                    </Link>
                </li>
            }
            {(menu=='teachers') ?
                <li className="menu-item active">
                    <Link href="/teachers" className="menu-link">
                        <i className="menu-icon tf-icons bx bxs-user"></i>
                        <div>Teachers</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/teachers" className="menu-link">
                        <i className="menu-icon tf-icons bx bxs-user"></i>
                        <div>Teachers</div>
                    </Link>
                </li>
            }
            {(menu=='students') ?
                    <li className="menu-item active">
                        <Link href="/students" className="menu-link">
                            <i className="menu-icon tf-icons bx bxs-user"></i>
                            <div>Students</div>
                        </Link>
                    </li>
                :
                    <li className="menu-item">
                        <Link href="/students" className="menu-link">
                            <i className="menu-icon tf-icons bx bxs-user"></i>
                            <div>Students</div>
                        </Link>
                    </li>
                }
            
        </>
        :
        (session?.user.role == 2) ?
        <>
            {(menu=='dashboard') ?
                <li className="menu-item active">
                    <Link href="/teacherDashboard" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-user-check"></i>
                        <div>Dashboard</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/teacherDashboard" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-user-check"></i>
                        <div>Dashboard</div>
                    </Link>
                </li>
            }
            {(menu=='teacherClass') ?
                <li className="menu-item active">
                    <Link href="/teacherClasses" className="menu-link">
                        <i className="menu-icon tf-icons bx bxs-user"></i>
                        <div>Classes</div>
                    </Link>
                </li>
            :
                <li className="menu-item">
                    <Link href="/teacherClasses" className="menu-link">
                        <i className="menu-icon tf-icons bx bxs-user"></i>
                        <div>Classes</div>
                    </Link>
                </li>
            }
            {(menu=='students') ?
                    <li className="menu-item active">
                        <Link href="/teacherStudents" className="menu-link">
                            <i className="menu-icon tf-icons bx bxs-user"></i>
                            <div>Students</div>
                        </Link>
                    </li>
                :
                    <li className="menu-item">
                        <Link href="/teacherStudents" className="menu-link">
                            <i className="menu-icon tf-icons bx bxs-user"></i>
                            <div>Students</div>
                        </Link>
                    </li>
                }
        </>
        :
        (session?.user.role == 5) ?
        <>
        {(menu=='dashboard') ?
                <li className="menu-item active">
                    <Link href="/districtDashboard" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-user-check"></i>
                        <div>Dashboard</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/districtDashboard" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-user-check"></i>
                        <div>Dashboard</div>
                    </Link>
                </li>
            }
            {(menu=='school') ?
                <li className="menu-item active">
                    <Link href="/districtSchool" className="menu-link">
                        <i className="menu-icon tf-icons bx bxs-user"></i>
                        <div>Schools</div>
                    </Link>
                </li>
            :
                <li className="menu-item">
                    <Link href="/districtSchool" className="menu-link">
                        <i className="menu-icon tf-icons bx bxs-user"></i>
                        <div>Schools</div>
                    </Link>
                </li>
            }
        </>
        :
        <>
        {(menu=='dashboard') ?
            <li className="menu-item active">
                <Link href="/studentDashboard" className="menu-link">
                <i className="menu-icon tf-icons bx bx-user-check"></i>
                    <div>Dashboard</div>
                </Link>
            </li>
            :
            <li className="menu-item">
            <Link href="/studentDashboard" className="menu-link">
            <i className="menu-icon tf-icons bx bx-user-check"></i>
                <div>Dashboard</div>
            </Link>
            </li>
        }
         {(menu=='studentCheckins') ?
            <li className="menu-item active">
                <Link href="/studentCheckins" className="menu-link">
                <i className="menu-icon tf-icons bx bx-user-check"></i>
                    <div>My Check-ins</div>
                </Link>
            </li>
            :
            <li className="menu-item">
                <Link href="/studentCheckins" className="menu-link">
                <i className="menu-icon tf-icons bx bx-user-check"></i>
                    <div>My Check-ins</div>
                </Link>
            </li>
        }
        {(menu=='studentLesson') ?
            <li className="menu-item active">
            <Link href="/studentLessons" className="menu-link">
            <i className="menu-icon tf-icons bx bx-user-check"></i>
                <div>Lessons</div>
            </Link>
        </li>
        :
        <li className="menu-item">
            <Link href="/studentLessons" className="menu-link">
            <i className="menu-icon tf-icons bx bx-user-check"></i>
                <div>Lessons</div>
            </Link>
        </li>
        }
        
        </>
}

        

        
        {(session?.user.role == 2) ?
            <>
                {(menu=='teacherSurvey') ?
                    <li className="menu-item active">
                        <Link href="/teacherSurvey" className="menu-link">
                            <i className="menu-icon tf-icons bx bxs-user"></i>
                            <div>Survey</div>
                        </Link>
                    </li>
                :
                    <li className="menu-item">
                        <Link href="/teacherSurvey" className="menu-link">
                            <i className="menu-icon tf-icons bx bxs-user"></i>
                            <div>Survey</div>
                        </Link>
                    </li>
                }
                {(menu=='scheduleCheckin') ?
                    <li className="menu-item active">
                        <Link href="/scheduleCheckin" className="menu-link">
                            <i className="menu-icon tf-icons bx bxs-user"></i>
                            <div>Schedule</div>
                        </Link>
                    </li>
                :
                    <li className="menu-item">
                        <Link href="/scheduleCheckin" className="menu-link">
                            <i className="menu-icon tf-icons bx bxs-user"></i>
                            <div>Schedule</div>
                        </Link>
                    </li>
                }
                {(menu=='teacherLesson') ?
                    <li className="menu-item active">
                        <Link href="/teacherLesson" className="menu-link">
                            <i className="menu-icon tf-icons bx bxs-user"></i>
                            <div>Lessons</div>
                        </Link>
                    </li>
                :
                    <li className="menu-item">
                        <Link href="/teacherLesson" className="menu-link">
                            <i className="menu-icon tf-icons bx bxs-user"></i>
                            <div>Lessons</div>
                        </Link>
                    </li>
                }
            </>
            :
            <></>
        }

        {(session?.user.role == 0 || session?.user.role == 1) ?
            <>
            {(menu=='survey') ?
                <li className="menu-item active">
                    <Link href="/survey" className="menu-link">
                        <i className="menu-icon tf-icons bx bx-detail"></i>
                        <div>Surveys</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/survey" className="menu-link">
                        <i className="menu-icon tf-icons bx bx-detail"></i>
                        <div>Surveys</div>
                    </Link>
                </li>
            }
            {(menu=='lessons') ?
                <li className="menu-item active">
                    <Link href="/lessons" className="menu-link">
                        <BiLaptop className="menu-icon" style={{fontSize:'large'}}/>
                        <div>Lessons</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/lessons" className="menu-link">
                        <BiLaptop className="menu-icon tf-icons" style={{fontSize:'large'}}/>
                        <div>Lessons</div>
                    </Link>
                </li>
            }
            
            </>
            :
            <></>
        }
        {(session?.user.role == 0)?
        <>
        {(menu=='checkin') ?
                <li className="menu-item active">
                    <Link href="/checkin" className="menu-link">
                        <BiCalendarCheck className="menu-icon" style={{fontSize:'large'}}/>
                        <div>Schedule</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/checkin" className="menu-link">
                        <BiCalendarCheck className="menu-icon tf-icons" style={{fontSize:'large'}}/>
                        <div>Schedule</div>
                    </Link>
                </li>
            }
        </>
        :
        <></>}
        {(session?.user.role == 1) ?
        <>
        {(menu=='checkin') ?
                <li className="menu-item active">
                    <Link href="/schoolCheckin" className="menu-link">
                        <BiCalendarCheck className="menu-icon" style={{fontSize:'large'}}/>
                        <div>Schedule</div>
                    </Link>
                </li>
                :
                <li className="menu-item">
                    <Link href="/schoolCheckin" className="menu-link">
                        <BiCalendarCheck className="menu-icon tf-icons" style={{fontSize:'large'}}/>
                        <div>Schedule</div>
                    </Link>
                </li>
            }
        </>
        :
        <></>}

        {(session?.user.role == 0 || session?.user.role == 1 || session?.user.role == 2 || session?.user.role == 3 ) ?
            <>
                {(menu=='resources') ?
                    <li className="menu-item active">
                        <Link href="/resources" className="menu-link">
                            <i className="menu-icon tf-icons bx bx-detail"></i>
                            <div>Resources</div>
                        </Link>
                    </li>
                    :
                    <li className="menu-item">
                        <Link href="/resources" className="menu-link">
                            <i className="menu-icon tf-icons bx bx-detail"></i>
                            <div>Resources</div>
                        </Link>
                    </li>
                }
            </>
            :
            <></>
        }

{(session?.user.role == 0 ) ?
            (menu=='reports') ?
     
            <li className="menu-item active">
                <Link href="/reports/schoolReports" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-detail"></i>
                    <div>Reports</div>
                </Link>
            </li>
        
            :
            <li className="menu-item">
                <Link href="/reports/schoolReports" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-detail"></i>
                    <div>Reports</div>
                </Link>
            </li>
            :
            <></>

}

{/* {(session?.user.role == 3 ) ?
            (props.menu=='reports') ?
       
            <li className="menu-item active">
                <Link href="/studentDetailedReports" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-detail"></i>
                    <div>Reports</div>
                </Link>
            </li>
        
            :
            <li className="menu-item">
                <Link href="/studentDetailedReports" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-detail"></i>
                    <div>Reports</div>
                </Link>
            </li>
            :
            <></>

} */}

{(session?.user.role == 0) ?
            (menu=='surveyreports') ?
     
            <li className="menu-item active">
                <Link href="/completeReports" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-detail"></i>
                    <div>Completion Report</div>
                </Link>
            </li>
        
            :
            <li className="menu-item">
                <Link href="/completeReports" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-detail"></i>
                    <div>Completion Report</div>
                </Link>
            </li>
            :
            <></>

}

{(session?.user.role == 1) ?
        (menu=='surveyreports') ?
            
        <li className="menu-item active">
            <Link href="/schoolCompleteReport" className="menu-link">
                <i className="menu-icon tf-icons bx bx-detail"></i>
                <div>Completion Report</div>
            </Link>
        </li>

        :
        <li className="menu-item">
            <Link href="/schoolCompleteReport" className="menu-link">
                <i className="menu-icon tf-icons bx bx-detail"></i>
                <div>Completion Report</div>
            </Link>
        </li>
        :
        <></>
}

{(session?.user.role == 2) ?
        (menu=='surveyreports') ?
            
        <li className="menu-item active">
            <Link href="/teacherCompleteReport" className="menu-link">
                <i className="menu-icon tf-icons bx bx-detail"></i>
                <div>Completion Report</div>
            </Link>
        </li>

        :
        <li className="menu-item">
            <Link href="/teacherCompleteReport" className="menu-link">
                <i className="menu-icon tf-icons bx bx-detail"></i>
                <div>Completion Report</div>
            </Link>
        </li>
        :
        <></>
}
        
            
     {(session?.user.type == 'admin' && session?.user.role != 0) ?
        <li className="menu-item">
            <a onClick={login} style={{cursor:"pointer"}} className="menu-link">
                <i className="menu-icon tf-icons bx bx-left-arrow-circle"></i>
                <div>Go Back</div>
            </a>
        </li>
        :
        <></>
     }              
            
        </ul>
    </aside>
    {/* ):
    <>
    {router.push('/login')}
    </>
    } */}
</>
  )
}

export default Sidebar